package mx.com.allianz.solicitante.portlet;

import mx.com.allianz.solicitante.constants.DatosSolicitantePortletKeys;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import java.util.Map;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

/**
 * @author sfrancof
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=Formularios",
		"com.liferay.portlet.instanceable=true",
		"com.liferay.portlet.requires-namespaced-parameters=false",
		"javax.portlet.display-name=datos-solicitante Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + DatosSolicitantePortletKeys.DatosSolicitante,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,guest"
	},
	service = Portlet.class
)
public class DatosSolicitantePortlet extends MVCPortlet {
	
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		if (_log.isInfoEnabled()) {
			_log.info("datos.solicitante -  v1.0.0), Cargado");
		}
	}
	
	private static Log _log = LogFactoryUtil.getLog(DatosSolicitantePortlet.class);
}