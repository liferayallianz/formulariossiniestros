<%@ include file="/init.jsp" %>

<div id="<portlet:namespace/>div-datos-solicitante" style="display:none">
	<aui:form action="#" name="fm_datos_solicitante">
		<aui:input name="version-datos-solicitud" type="hidden" value="1.0.0"  />
		<aui:input cssClass="formularioCampoTexto" id="datos_solicitante_nombre" name="nombre" type="text" value="" label="" placeholder="datos.personales.nombre.obligatorio" required="true" >
			<aui:validator name="required" errorMessage="El nombre es requerido"/>
			<aui:validator name="maxLength" errorMessage="La longitud del nombre debe ser menor a 75 caracteres">75</aui:validator>
			<aui:validator name="custom"  errorMessage="Por favor ingrese s&oacute;lo letras, guiones o espacios">
		        function (val, fieldNode, ruleValue) {
		                var returnValue = true;
		                var iChars = "~!@#$%&*()+=[]\\;,./{}|\":<>?0123456789";
		                for (var i = 0; i < val.length; i++) {
		                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
		                     returnValue = false;
		                    }
		                }
		                return returnValue;
		        }
		    </aui:validator>
		</aui:input>
		<aui:input cssClass="formularioCampoTexto" id="datos_solicitante_apellido_paterno" name="apellidoPaterno" type="text" value="" label="" placeholder="datos.personales.apellido.paterno.obligatorio" required="true">
			<aui:validator name="required" errorMessage="El apellido paterno es requerido"/>
			<aui:validator name="maxLength" errorMessage="La longitud del apellido paterno debe ser menor a 50 caracteres">50</aui:validator>
			<aui:validator name="custom"  errorMessage="Por favor ingrese s&oacute;lo letras, guiones o espacios">
		        function (val, fieldNode, ruleValue) {
		                var returnValue = true;
		                var iChars = "~!@#$%&*()+=[]\\;,./{}|\":<>?0123456789";
		                for (var i = 0; i < val.length; i++) {
		                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
		                     returnValue = false;
		                    }
		                }
		                return returnValue;
		        }
		    </aui:validator>		
		    </aui:input>
		<aui:input cssClass="formularioCampoTexto" id="datos_solicitante_apellido_materno" name="apellidoMaterno" type="text" value="" label="" placeholder="datos.personales.apellido.materno" >
			<aui:validator name="maxLength" errorMessage="La longitud del apellido materno debe ser menor a 50 caracteres">50</aui:validator>
			<aui:validator name="custom"  errorMessage="Por favor ingrese s&oacute;lo letras, guiones o espacios">
		        function (val, fieldNode, ruleValue) {
		                var returnValue = true;
		                var iChars = "~!@#$%&*()+=[]\\;,./{}|\":<>?0123456789";
		                for (var i = 0; i < val.length; i++) {
		                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
		                     returnValue = false;
		                    }
		                }
		                return returnValue;
		        }
		    </aui:validator>				
		</aui:input>
		    
		<aui:input cssClass="formularioCampoTexto" id="datos_solicitante_telefono_particular" name="telefonoParticular" type="text" value="" label="" placeholder="datos_solicitante_web_DatosContactoPersona.telefonoParticular" >
			<aui:validator name="required" errorMessage="El tel&eacute;fono es requerido"/>
			<aui:validator name="rangeLength" errorMessage="La longitud del tel&eacute;fono debe ser mayor a 8 y menor a 25 caracteres">[8,25]</aui:validator>	
			<aui:validator name="custom"  errorMessage="Por favor ingrese s&oacute;lo n&uacute;meros, guiones, parentesis o espacio">
			        function (val, fieldNode, ruleValue) {
			                var returnValue = true;
			                var iChars = "~`!@#$%^&*_=[]\\\';,./{}|\":<>?abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
			                for (var i = 0; i < val.length; i++) {
			                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
			                     returnValue = false;
			                    }
			                }
			                return returnValue;
			        }
			    </aui:validator>
		</aui:input>
		<aui:input cssClass="formularioCampoTexto" id="datos_solicitante_telefono_celular" name="telefonoCelular" type="text" value="" label="" placeholder="datos_solicitante_web_DatosContactoPersona.telefonoCelular" >
		<aui:validator name="rangeLength" errorMessage="La longitud del tel&eacute;fono celular debe ser mayor a 8 y menor a 25 caracteres">[8,25]</aui:validator>	
			<aui:validator name="custom"  errorMessage="Por favor ingrese s&oacute;lo n&uacute;meros, guiones, parentesis o espacio">
			        function (val, fieldNode, ruleValue) {
			                var returnValue = true;
			                var iChars = "~`!@#$%^&*_=[]\\\';,./{}|\":<>?abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
			                for (var i = 0; i < val.length; i++) {
			                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
			                     returnValue = false;
			                    }
			                }
			                return returnValue;
			        }
			    </aui:validator>
		</aui:input>
		<aui:input cssClass="formularioCampoTexto" id="datos_solicitante_email" name="email" type="text" value="" label="" placeholder="datos_solicitante_web_DatosContactoPersona.email" >
				<aui:validator name="required" errorMessage="El correo electr&oacute;nico es requerido"/>
				<aui:validator name="email" errorMessage="Por favor ingrese un correo electr&oacute;nico v&aacute;lido, ej. nombre@mail.com"/>
		</aui:input>
		<aui:input cssClass="formularioCampoTexto" id="datos_solicitante_email_confirmacion" name="emailConfirmacion" type="text" value="" label="" placeholder="datos_solicitante_web_DatosContactoPersona.email.confirmacion" >
				<aui:validator name="required" errorMessage="La confirmaci&oacute;n de correo electr&oacute;nico es requerida"/>
				<aui:validator name="email" errorMessage="Por favor ingrese la confirmaci&oacute;n de correo electr&oacute;nico con formato de correo electr&oacute;nico v&aacute;lido, ej. nombre@mail.com"/>
				<aui:validator name="equalTo" errorMessage="La confirmaci&oacute;n de correo electr&oacute;nico debe ser igual al correo electr&oacute;nico">'#<portlet:namespace />datos_solicitante_email'</aui:validator>
		</aui:input>
	
	</aui:form>
</div>

<script type="text/javascript">
console.log("datos.solicitante v1.0.0");
</script>

<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">

Liferay.provide(window, 'mostrarSeccionSolicitante', function(seccionPoliza,mostrado) {
	A.one('#<portlet:namespace/>' + seccionPoliza)._node.style.display = mostrado;
});

Liferay.on("mostrarDatosSolicitante",function(event){
	mostrarSeccionSolicitante('div-datos-solicitante','block');
});

Liferay.on("ocultarDatosSolicitante",function(event){
	mostrarSeccionSolicitante('div-datos-solicitante','none');
});

	
Liferay.on('validaDatosSolicitante', function(event) {
	var formValidator = Liferay.Form.get('<portlet:namespace />fm_datos_solicitante').formValidator;
	console.log('Entrando a validaDatosSolicitante');
	formValidator.validate();
	var eventoRespuesta = {};
	eventoRespuesta.valido = !formValidator.hasErrors();
	console.log('ValidaDatosPersonales eventoRespuesta.valido = ' + eventoRespuesta.valido);
	if (eventoRespuesta.valido) {
		eventoRespuesta.nombreSolicitante = A.one('#<portlet:namespace />datos_solicitante_nombre').get('value');
		eventoRespuesta.apellidoPaternoSolicitante = A.one('#<portlet:namespace />datos_solicitante_apellido_paterno').get('value');
		eventoRespuesta.apellidoMaternoSolicitante = A.one('#<portlet:namespace />datos_solicitante_apellido_materno').get('value');
		eventoRespuesta.telefonoParticular = A.one('#<portlet:namespace />datos_solicitante_telefono_particular').get('value');
		eventoRespuesta.telefonoCelular = A.one('#<portlet:namespace />datos_solicitante_telefono_celular').get('value');
		eventoRespuesta.email = A.one('#<portlet:namespace />datos_solicitante_email').get('value');
		
	}	
	eventoRespuesta.nombreSeccion = "DatosSolicitante";
	Liferay.fire('validaDatosSolicitanteRespuesta', eventoRespuesta );
});
	
Liferay.on('cargaDatosPersonalesDeCliente', function(cliente){
	if (cliente){
		A.one('#<portlet:namespace />datos_solicitante_nombre').set('value', cliente.nombre ? cliente.nombre : "");
		A.one('#<portlet:namespace />datos_solicitante_apellido_paterno').set('value', cliente.apellidoPaterno? cliente.apellidoPaterno : "");
		A.one('#<portlet:namespace />datos_solicitante_apellido_materno').set('value', cliente.apellidoMaterno? cliente.apellidoMaterno : "");
		A.one('#<portlet:namespace />datos_contacto_persona_email').set('value', cliente.email);
		A.one('#<portlet:namespace />datos_contacto_persona_email_confirmacion').set('value', cliente.email);
		A.one('#<portlet:namespace />datos_contacto_persona_telefono_particular').set('value', cliente.telefonoParticular?cliente.telefonoParticular:"");
		A.one('#<portlet:namespace />datos_contacto_persona_telefono_celular').set('value', cliente.telefonoCelular?cliente.telefonoCelular:"");
	} 
});
</aui:script>