package mx.com.allianz.client.puc.tramites.api.exception;

public class PucTramitesServiceClientException extends Exception {

	private static final long serialVersionUID = 3355065473264274389L;

	public PucTramitesServiceClientException() {
		super();
	}

	public PucTramitesServiceClientException(String message) {
		super(message);
	}

	public PucTramitesServiceClientException(Throwable cause) {
        super(cause);
    }
}
