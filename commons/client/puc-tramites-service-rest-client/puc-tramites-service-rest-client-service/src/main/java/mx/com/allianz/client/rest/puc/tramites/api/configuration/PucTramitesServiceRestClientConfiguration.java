package mx.com.allianz.client.rest.puc.tramites.api.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;


/**
 * @author amarquez
 *
 */
@ExtendedObjectClassDefinition(category = "WS Configuration")
@Meta.OCD(
		id = "mx.com.allianz.client.rest.puc.tramites.api.configuration.PucTramitesServiceRestClientConfiguration",
		localization = "content/Language",
		name = "PUC Tramites REST Web Service Configuration"
	)
public interface PucTramitesServiceRestClientConfiguration {
	
	@Meta.AD(
		deflt = "http://apps.allianz.com.mx/az-servicio-rest-eegmm/rest/envio/posts/generarOtTramites/", 
		description = "PUC Tramites REST Web Service EndPoint",
		required = false
	)
	public String pucTramitesServiceRESTEndPoint();
	
	@Meta.AD(
		deflt = "allianz:allianz123456", 
		description = "PUC Tramites REST Web Service Credentials",
		required = false
	)
	public String pucTramitesServiceRESTCredentials();
	
	@Meta.AD(
		deflt = "600000", 
		description = "PUC Tramites REST Web Service Timeout",
		required = false
	)
	public long pucServiceRESTTimeout();

	/**
	 * validContentTypes: The list of content types separated by commas.
	 * 
	 * @see <a href="http://www.freeformatter.com/mime-types-list.html">Mime Types List</a>
	 * 
	 */
	@Meta.AD(deflt = "doc:application/msword," + 
					 "docx:application/vnd.openxmlformats-officedocument.wordprocessingml.document," + 
					 "xls:application/vnd.ms-excel," + 
					 "xlsx:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet," + 
					 "ppt:application/vnd.ms-powerpoint," + 
					 "pptx:application/vnd.openxmlformats-officedocument.presentationml.presentation," + 
					 "pdf:application/pdf," + 
					 "jpg:image/jpeg," +
					 "jpeg:image/jpeg," +
					 "png:image/png," + 
					 "tiff:image/tiff," + 
					 "gif:image/gif", required = false)
	public String[] validContenTypesExtentions();

}