package mx.com.allianz.client.rest.puc.tramites.api;

import feign.Headers;
import feign.Param;
import feign.RequestLine;
import mx.com.allianz.commons.dto.puc.PucTramiteRespuestaRestDTO;

//@FeignClient("client")
public interface PucTramitesFeignRestClient {
	/**
	 * Metodo abstracto que registra una orden de trabajo de Tramites con los
	 * campos separados por ||.
	 * 
	 * @return PucTramiteRespuestaRestDTO
	 */
	/*
	 * @RequestLine anotacion de feign para ligar el llamado a la operacion del
	 * servicio a la implementacion de este metodo
	 */
//	@RequestMapping(method = RequestMethod.GET, value = "/path-to-endpoint")
	@RequestLine("POST /posts/generarOtTramites/{campos}")
	@Headers({ "Authorization: Basic YWxsaWFuejoxMjM0NTY=", "Content-Type: multipart/form-data" })
	PucTramiteRespuestaRestDTO generarOtTramites(@Param("campos") String campos);
}
