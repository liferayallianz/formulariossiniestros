/**
 * 
 */
package it.dontesta.labs.liferay.lrbo16.webservice.crm.service.configuration;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

/**
 * @author amusarra
 *
 */
@ExtendedObjectClassDefinition(category = "WS Configuration")
@Meta.OCD(
		id = "it.dontesta.labs.liferay.lrbo16.webservice.crm.service.configuration.CRMSOAPServiceConfiguration",
		localization = "content/Language",
		name = "crm.services.configuration.name"
	)
public interface CRMSOAPServiceConfiguration {
	@Meta.AD(
		deflt = "http://portalb.allianz.com.mx/ws-puc-quejas/service/PucService?wsdl", 
		description = "PUC Quejas SOAP Web Service EndPoint",
		required = false
	)
	public String crmServiceSOAPEndPoint();

	@Meta.AD(
		deflt = "600000", 
		description = "PUC Quejas SOAP Web Service Timeout",
		required = false
	)
	public long crmServiceSOAPTimeout();
	

	/**
	 * validContentTypes: The list of content types separated by commas.
	 * 
	 * @see <a href="http://www.freeformatter.com/mime-types-list.html">Mime Types List</a>
	 * 
	 */
	@Meta.AD(deflt = "doc:application/msword," + 
					 "docx:application/vnd.openxmlformats-officedocument.wordprocessingml.document," + 
					 "xls:application/vnd.ms-excel," + 
					 "xlsx:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet," + 
					 "ppt:application/vnd.ms-powerpoint," + 
					 "pptx:application/vnd.openxmlformats-officedocument.presentationml.presentation," + 
					 "pdf:application/pdf," + 
					 "jpg:image/jpeg," +
					 "jpeg:image/jpeg," +
					 "png:image/png," + 
					 "tiff:image/tiff," + 
					 "gif:image/gif", required = false)
	public String[] validContenTypesExtentions();
}