package it.dontesta.labs.liferay.lrbo16.webservice.crm.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.xml.namespace.QName;
import javax.xml.ws.WebServiceException;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.Validator;

import aQute.bnd.annotation.ProviderType;
import aQute.bnd.annotation.metatype.Configurable;
import it.dontesta.labs.liferay.lrbo16.webservice.crm.api.CRMService;
import it.dontesta.labs.liferay.lrbo16.webservice.crm.exception.CRMServiceException;
import it.dontesta.labs.liferay.lrbo16.webservice.crm.model.Customer;
import it.dontesta.labs.liferay.lrbo16.webservice.crm.service.configuration.CRMSOAPServiceConfiguration;
import service.ws.puc.habilgroup.com.ArchivoPuc;
import service.ws.puc.habilgroup.com.GenerarOTQuejas;
import service.ws.puc.habilgroup.com.PucWSException_Exception;
import service.ws.puc.habilgroup.com.PucWebService;

/**
 * 
 * @author amusarra
 *
 */
@Component(
		configurationPid="it.dontesta.labs.liferay.lrbo16.webservice.crm.service.configuration.CRMSOAPServiceConfiguration",
		immediate = true,
		property = {},
		service = CRMService.class
		)
@ProviderType
public class CRMSOAPService implements CRMService {

	@Override
	public Integer generarOTQuejas(String username, String cveFlujo, String nombreClasificador, List<String> campos,
			String observaciones, List<File> archivos) throws CRMServiceException {

		if (_log.isInfoEnabled()) {
			_log.info("CRMSOAPService -> generarOTQuejas ");
			_log.info("username = " + username + ", cveFlujo = " + cveFlujo + ", nombreClasificador = " + nombreClasificador
					+ ", campos = [" + Optional.ofNullable(campos).orElseGet(ArrayList<String>::new).stream().collect(Collectors.joining(",")) + "]" + ", observaciones = " + observaciones 
					+ ", archivos = [" + Optional.ofNullable(archivos).orElseGet(ArrayList<File>::new).stream().map(File::getName)
					.collect(Collectors.joining(",")) + "]");			
		}
		
		GenerarOTQuejas queja = new GenerarOTQuejas();
		int idPuc = 0;
		List<ArchivoPuc> archivosPuc = new ArrayList<>();
		Map<String,String> fileExtentions = getCRMServiceSOAPFileExtentions();
		
		queja.setUsername(username);
		queja.setCveFlujo(cveFlujo);
		queja.setNombreClasificador(nombreClasificador);
		queja.setObservaciones(observaciones);
		
		archivosPuc = Optional.ofNullable(archivos).orElseGet(ArrayList<File>::new).
				stream().filter(File::exists).filter(File::isFile).map(file -> {
				ArchivoPuc archivoPuc = new ArchivoPuc();
				String fileName = file.getName();
				String extention = FileUtil.getExtension(fileName);
				String contentType = fileExtentions.get(extention);
				if (_log.isDebugEnabled()) {
					_log.debug("fileName = " + fileName);
					_log.debug("extention = " + extention);
					_log.debug("contentType = " + contentType);
				}
				// TODO quitar println
				System.out.println("fileName = " + fileName);
				System.out.println("extention = " + extention);
				System.out.println("contentType = " + contentType);
				try {
					archivoPuc.setBytes(Files.readAllBytes(file.toPath()));
				} catch (IOException e) {
					if(_log.isErrorEnabled()) _log.error(e);
				}
				archivoPuc.setContentType(contentType);
				archivoPuc.setNombre(fileName);
				return archivoPuc;
		}).collect(Collectors.toList());

		if (_log.isDebugEnabled()) {
			_log.debug("queja (GenerarOTQueja) = " + queja);
			_log.debug("archivosPuc = " + archivosPuc);
		}

		if (_log.isInfoEnabled()) {
			_log.info("Iniciando envio para consumo de servicio Puc Quejas SOAP");
		}
		try {
			idPuc = crmService.generarOTQuejas(username, cveFlujo,
					nombreClasificador, campos, observaciones, archivosPuc.isEmpty()?null:archivosPuc);
		} catch (Exception e) {
			if (_log.isErrorEnabled()) {
				_log.error("Error al consumir servicio Puc Quejas SOAP se escala error como CRMServiceException: \n" + e.getMessage());
			}
			// TODO quitar printStackTrace
			e.printStackTrace();
			throw new CRMServiceException(e);
		}
		if (_log.isInfoEnabled()) {
			_log.info("El id devuelto del servicio Puc Quejas SOAP es :"+ idPuc);			
		}
	
		return idPuc;
	}
	
	@Override
	public Integer createOTDefault() throws CRMServiceException {
		
		if(_log.isInfoEnabled()){
			_log.info("Invocacion por defecto para pruebas de servicio ");
		}
		
		int idPuc = 0;
		GenerarOTQuejas queja = new GenerarOTQuejas();
		List<String> campos = new ArrayList<>();
		
		if (Validator.isNotNull(crmService)) {
			try {
				campos = queja.getCampos();
				campos.add("");
				campos.add("Queja");
				campos.add("2016");
				campos.add("");
				campos.add("Cliente");
				campos.add(" ");
				campos.add("GMMC");
				campos.add("");
				campos.add("756");
				campos.add("prueba");
				campos.add("prueba");
				campos.add(" ");
				campos.add("1324567890");
				campos.add(" ");
				campos.add("aldo.lobato@allianz.com.mx");
				campos.add("Distrito Federal");
				campos.add("Probelmas en la atención de un siniestro");
				campos.add("No estoy de acuerdo");
				campos.add("1");
				campos.add("1");
				campos.add("publico");
				campos.add(" ");
				campos.add("KIJYMC");
				campos.add("-1138425460");
				
				queja.setUsername("admin.quejas");
				queja.setCveFlujo("Siniestros");
				queja.setNombreClasificador("Queja-1-GMMC");
				queja.setObservaciones("Prueba queja");
				
				if(_log.isInfoEnabled()){
					_log.info("campos = " + campos.stream().collect(Collectors.joining(", ")));
					_log.info("queja = " + queja);
				}
				
				idPuc = crmService.generarOTQuejas(queja.getUsername(), queja.getCveFlujo(),
						queja.getNombreClasificador(), campos, queja.getObservaciones(), null);
			} catch (WebServiceException | PucWSException_Exception e) {
				if (_log.isErrorEnabled()) {
					_log.error(e);
				}
				throw new CRMServiceException(e);
			}
		} else {
			if(_log.isErrorEnabled()){
				_log.error("The CRMService is null! " + crmService);				
			}
			throw new CRMServiceException("The CRMService is null!");
		}
		if(_log.isInfoEnabled()){
			_log.info("El id devuelto es :"+ idPuc);					
		}
		return idPuc;
	}

	
	/**
	 * 
	 * @return
	 */
	public String getCRMServiceSOAPEndPoint() {
		return _configuration.crmServiceSOAPEndPoint();
	}

	public long getCRMServiceSOAPTimeout() {
		return _configuration.crmServiceSOAPTimeout();
	}
	
	public Map<String,String> getCRMServiceSOAPFileExtentions() {
		return Arrays.stream(_configuration.validContenTypesExtentions()).map(row -> row.split(":"))
				.collect(Collectors.toMap(row -> row[0], row -> row[1]));
	}
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		_configuration = Configurable.createConfigurable(
				CRMSOAPServiceConfiguration.class, properties);
		
		if (_log.isInfoEnabled()) {
			_log.info("Configured PUC Quejas SOAP : { " +
					"Endpoint : " + getCRMServiceSOAPEndPoint() + ", " +
					"Timeout : " + getCRMServiceSOAPTimeout() + ", " +
					"ContentTypes : " + getCRMServiceSOAPFileExtentions().entrySet().stream()
		            .map(entry -> entry.getKey() + " - " + entry.getValue())
		            .collect(Collectors.joining(", "))
					+ " }");
		}
		setCRMService();
	}
	
	/**
	 * 
	 * @return
	 */
	private void setCRMService() {
        long timeout = getCRMServiceSOAPTimeout();
		try {
			JaxWsProxyFactoryBean proxyFactory = new JaxWsProxyFactoryBean();
			proxyFactory.setServiceClass(PucWebService.class);
			proxyFactory.setServiceName(new QName("http://com.habilgroup.puc.ws.service"));
			proxyFactory.setAddress(getCRMServiceSOAPEndPoint());
			PucWebService crmServicePT = (PucWebService)proxyFactory.create();
			crmService = crmServicePT;
			
			if(_log.isDebugEnabled()){
				_log.debug("crmService = " + crmService);
			}
			
			Client client = ClientProxy.getClient(crmServicePT);
	        if (client != null) {
	            HTTPConduit conduit = (HTTPConduit) client.getConduit();
	            HTTPClientPolicy policy = new HTTPClientPolicy();
	            policy.setConnectionTimeout(timeout);
	            policy.setReceiveTimeout(timeout);
	            conduit.setClient(policy);
	        }
			
		} catch (Exception e) {
			if (_log.isErrorEnabled()) {
				_log.error(e);
			}
		}
		
	}

	private PucWebService crmService;
	private volatile CRMSOAPServiceConfiguration _configuration;
	private static Log _log = LogFactoryUtil.getLog(CRMSOAPService.class);
	
}
