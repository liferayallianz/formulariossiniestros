/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import mx.com.allianz.service.catalogos.exception.NoSuchProductoACXException;
import mx.com.allianz.service.catalogos.model.ProductoACX;
import mx.com.allianz.service.catalogos.model.impl.ProductoACXImpl;
import mx.com.allianz.service.catalogos.model.impl.ProductoACXModelImpl;
import mx.com.allianz.service.catalogos.service.persistence.ProductoACXPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the producto a c x service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ProductoACXPersistence
 * @see mx.com.allianz.service.catalogos.service.persistence.ProductoACXUtil
 * @generated
 */
@ProviderType
public class ProductoACXPersistenceImpl extends BasePersistenceImpl<ProductoACX>
	implements ProductoACXPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ProductoACXUtil} to access the producto a c x persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ProductoACXImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ProductoACXModelImpl.ENTITY_CACHE_ENABLED,
			ProductoACXModelImpl.FINDER_CACHE_ENABLED, ProductoACXImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ProductoACXModelImpl.ENTITY_CACHE_ENABLED,
			ProductoACXModelImpl.FINDER_CACHE_ENABLED, ProductoACXImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ProductoACXModelImpl.ENTITY_CACHE_ENABLED,
			ProductoACXModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBUDESCRIPCION =
		new FinderPath(ProductoACXModelImpl.ENTITY_CACHE_ENABLED,
			ProductoACXModelImpl.FINDER_CACHE_ENABLED, ProductoACXImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByfindBuDescripcion",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBUDESCRIPCION =
		new FinderPath(ProductoACXModelImpl.ENTITY_CACHE_ENABLED,
			ProductoACXModelImpl.FINDER_CACHE_ENABLED, ProductoACXImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByfindBuDescripcion", new String[] { String.class.getName() },
			ProductoACXModelImpl.DESCRIPCIONPRODUCTO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_FINDBUDESCRIPCION = new FinderPath(ProductoACXModelImpl.ENTITY_CACHE_ENABLED,
			ProductoACXModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByfindBuDescripcion", new String[] { String.class.getName() });

	/**
	 * Returns all the producto a c xs where descripcionProducto = &#63;.
	 *
	 * @param descripcionProducto the descripcion producto
	 * @return the matching producto a c xs
	 */
	@Override
	public List<ProductoACX> findByfindBuDescripcion(String descripcionProducto) {
		return findByfindBuDescripcion(descripcionProducto, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the producto a c xs where descripcionProducto = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProductoACXModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcionProducto the descripcion producto
	 * @param start the lower bound of the range of producto a c xs
	 * @param end the upper bound of the range of producto a c xs (not inclusive)
	 * @return the range of matching producto a c xs
	 */
	@Override
	public List<ProductoACX> findByfindBuDescripcion(
		String descripcionProducto, int start, int end) {
		return findByfindBuDescripcion(descripcionProducto, start, end, null);
	}

	/**
	 * Returns an ordered range of all the producto a c xs where descripcionProducto = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProductoACXModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcionProducto the descripcion producto
	 * @param start the lower bound of the range of producto a c xs
	 * @param end the upper bound of the range of producto a c xs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching producto a c xs
	 */
	@Override
	public List<ProductoACX> findByfindBuDescripcion(
		String descripcionProducto, int start, int end,
		OrderByComparator<ProductoACX> orderByComparator) {
		return findByfindBuDescripcion(descripcionProducto, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the producto a c xs where descripcionProducto = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProductoACXModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param descripcionProducto the descripcion producto
	 * @param start the lower bound of the range of producto a c xs
	 * @param end the upper bound of the range of producto a c xs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching producto a c xs
	 */
	@Override
	public List<ProductoACX> findByfindBuDescripcion(
		String descripcionProducto, int start, int end,
		OrderByComparator<ProductoACX> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBUDESCRIPCION;
			finderArgs = new Object[] { descripcionProducto };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBUDESCRIPCION;
			finderArgs = new Object[] {
					descripcionProducto,
					
					start, end, orderByComparator
				};
		}

		List<ProductoACX> list = null;

		if (retrieveFromCache) {
			list = (List<ProductoACX>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProductoACX productoACX : list) {
					if (!Objects.equals(descripcionProducto,
								productoACX.getDescripcionProducto())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PRODUCTOACX_WHERE);

			boolean bindDescripcionProducto = false;

			if (descripcionProducto == null) {
				query.append(_FINDER_COLUMN_FINDBUDESCRIPCION_DESCRIPCIONPRODUCTO_1);
			}
			else if (descripcionProducto.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_FINDBUDESCRIPCION_DESCRIPCIONPRODUCTO_3);
			}
			else {
				bindDescripcionProducto = true;

				query.append(_FINDER_COLUMN_FINDBUDESCRIPCION_DESCRIPCIONPRODUCTO_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProductoACXModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDescripcionProducto) {
					qPos.add(descripcionProducto);
				}

				if (!pagination) {
					list = (List<ProductoACX>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProductoACX>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first producto a c x in the ordered set where descripcionProducto = &#63;.
	 *
	 * @param descripcionProducto the descripcion producto
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching producto a c x
	 * @throws NoSuchProductoACXException if a matching producto a c x could not be found
	 */
	@Override
	public ProductoACX findByfindBuDescripcion_First(
		String descripcionProducto,
		OrderByComparator<ProductoACX> orderByComparator)
		throws NoSuchProductoACXException {
		ProductoACX productoACX = fetchByfindBuDescripcion_First(descripcionProducto,
				orderByComparator);

		if (productoACX != null) {
			return productoACX;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("descripcionProducto=");
		msg.append(descripcionProducto);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProductoACXException(msg.toString());
	}

	/**
	 * Returns the first producto a c x in the ordered set where descripcionProducto = &#63;.
	 *
	 * @param descripcionProducto the descripcion producto
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching producto a c x, or <code>null</code> if a matching producto a c x could not be found
	 */
	@Override
	public ProductoACX fetchByfindBuDescripcion_First(
		String descripcionProducto,
		OrderByComparator<ProductoACX> orderByComparator) {
		List<ProductoACX> list = findByfindBuDescripcion(descripcionProducto,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last producto a c x in the ordered set where descripcionProducto = &#63;.
	 *
	 * @param descripcionProducto the descripcion producto
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching producto a c x
	 * @throws NoSuchProductoACXException if a matching producto a c x could not be found
	 */
	@Override
	public ProductoACX findByfindBuDescripcion_Last(
		String descripcionProducto,
		OrderByComparator<ProductoACX> orderByComparator)
		throws NoSuchProductoACXException {
		ProductoACX productoACX = fetchByfindBuDescripcion_Last(descripcionProducto,
				orderByComparator);

		if (productoACX != null) {
			return productoACX;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("descripcionProducto=");
		msg.append(descripcionProducto);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProductoACXException(msg.toString());
	}

	/**
	 * Returns the last producto a c x in the ordered set where descripcionProducto = &#63;.
	 *
	 * @param descripcionProducto the descripcion producto
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching producto a c x, or <code>null</code> if a matching producto a c x could not be found
	 */
	@Override
	public ProductoACX fetchByfindBuDescripcion_Last(
		String descripcionProducto,
		OrderByComparator<ProductoACX> orderByComparator) {
		int count = countByfindBuDescripcion(descripcionProducto);

		if (count == 0) {
			return null;
		}

		List<ProductoACX> list = findByfindBuDescripcion(descripcionProducto,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the producto a c xs before and after the current producto a c x in the ordered set where descripcionProducto = &#63;.
	 *
	 * @param codigoProducto the primary key of the current producto a c x
	 * @param descripcionProducto the descripcion producto
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next producto a c x
	 * @throws NoSuchProductoACXException if a producto a c x with the primary key could not be found
	 */
	@Override
	public ProductoACX[] findByfindBuDescripcion_PrevAndNext(
		String codigoProducto, String descripcionProducto,
		OrderByComparator<ProductoACX> orderByComparator)
		throws NoSuchProductoACXException {
		ProductoACX productoACX = findByPrimaryKey(codigoProducto);

		Session session = null;

		try {
			session = openSession();

			ProductoACX[] array = new ProductoACXImpl[3];

			array[0] = getByfindBuDescripcion_PrevAndNext(session, productoACX,
					descripcionProducto, orderByComparator, true);

			array[1] = productoACX;

			array[2] = getByfindBuDescripcion_PrevAndNext(session, productoACX,
					descripcionProducto, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProductoACX getByfindBuDescripcion_PrevAndNext(Session session,
		ProductoACX productoACX, String descripcionProducto,
		OrderByComparator<ProductoACX> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PRODUCTOACX_WHERE);

		boolean bindDescripcionProducto = false;

		if (descripcionProducto == null) {
			query.append(_FINDER_COLUMN_FINDBUDESCRIPCION_DESCRIPCIONPRODUCTO_1);
		}
		else if (descripcionProducto.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_FINDBUDESCRIPCION_DESCRIPCIONPRODUCTO_3);
		}
		else {
			bindDescripcionProducto = true;

			query.append(_FINDER_COLUMN_FINDBUDESCRIPCION_DESCRIPCIONPRODUCTO_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProductoACXModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindDescripcionProducto) {
			qPos.add(descripcionProducto);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(productoACX);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProductoACX> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the producto a c xs where descripcionProducto = &#63; from the database.
	 *
	 * @param descripcionProducto the descripcion producto
	 */
	@Override
	public void removeByfindBuDescripcion(String descripcionProducto) {
		for (ProductoACX productoACX : findByfindBuDescripcion(
				descripcionProducto, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(productoACX);
		}
	}

	/**
	 * Returns the number of producto a c xs where descripcionProducto = &#63;.
	 *
	 * @param descripcionProducto the descripcion producto
	 * @return the number of matching producto a c xs
	 */
	@Override
	public int countByfindBuDescripcion(String descripcionProducto) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_FINDBUDESCRIPCION;

		Object[] finderArgs = new Object[] { descripcionProducto };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PRODUCTOACX_WHERE);

			boolean bindDescripcionProducto = false;

			if (descripcionProducto == null) {
				query.append(_FINDER_COLUMN_FINDBUDESCRIPCION_DESCRIPCIONPRODUCTO_1);
			}
			else if (descripcionProducto.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_FINDBUDESCRIPCION_DESCRIPCIONPRODUCTO_3);
			}
			else {
				bindDescripcionProducto = true;

				query.append(_FINDER_COLUMN_FINDBUDESCRIPCION_DESCRIPCIONPRODUCTO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindDescripcionProducto) {
					qPos.add(descripcionProducto);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_FINDBUDESCRIPCION_DESCRIPCIONPRODUCTO_1 =
		"productoACX.descripcionProducto IS NULL";
	private static final String _FINDER_COLUMN_FINDBUDESCRIPCION_DESCRIPCIONPRODUCTO_2 =
		"productoACX.descripcionProducto = ?";
	private static final String _FINDER_COLUMN_FINDBUDESCRIPCION_DESCRIPCIONPRODUCTO_3 =
		"(productoACX.descripcionProducto IS NULL OR productoACX.descripcionProducto = '')";

	public ProductoACXPersistenceImpl() {
		setModelClass(ProductoACX.class);
	}

	/**
	 * Caches the producto a c x in the entity cache if it is enabled.
	 *
	 * @param productoACX the producto a c x
	 */
	@Override
	public void cacheResult(ProductoACX productoACX) {
		entityCache.putResult(ProductoACXModelImpl.ENTITY_CACHE_ENABLED,
			ProductoACXImpl.class, productoACX.getPrimaryKey(), productoACX);

		productoACX.resetOriginalValues();
	}

	/**
	 * Caches the producto a c xs in the entity cache if it is enabled.
	 *
	 * @param productoACXs the producto a c xs
	 */
	@Override
	public void cacheResult(List<ProductoACX> productoACXs) {
		for (ProductoACX productoACX : productoACXs) {
			if (entityCache.getResult(
						ProductoACXModelImpl.ENTITY_CACHE_ENABLED,
						ProductoACXImpl.class, productoACX.getPrimaryKey()) == null) {
				cacheResult(productoACX);
			}
			else {
				productoACX.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all producto a c xs.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ProductoACXImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the producto a c x.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ProductoACX productoACX) {
		entityCache.removeResult(ProductoACXModelImpl.ENTITY_CACHE_ENABLED,
			ProductoACXImpl.class, productoACX.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<ProductoACX> productoACXs) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ProductoACX productoACX : productoACXs) {
			entityCache.removeResult(ProductoACXModelImpl.ENTITY_CACHE_ENABLED,
				ProductoACXImpl.class, productoACX.getPrimaryKey());
		}
	}

	/**
	 * Creates a new producto a c x with the primary key. Does not add the producto a c x to the database.
	 *
	 * @param codigoProducto the primary key for the new producto a c x
	 * @return the new producto a c x
	 */
	@Override
	public ProductoACX create(String codigoProducto) {
		ProductoACX productoACX = new ProductoACXImpl();

		productoACX.setNew(true);
		productoACX.setPrimaryKey(codigoProducto);

		return productoACX;
	}

	/**
	 * Removes the producto a c x with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param codigoProducto the primary key of the producto a c x
	 * @return the producto a c x that was removed
	 * @throws NoSuchProductoACXException if a producto a c x with the primary key could not be found
	 */
	@Override
	public ProductoACX remove(String codigoProducto)
		throws NoSuchProductoACXException {
		return remove((Serializable)codigoProducto);
	}

	/**
	 * Removes the producto a c x with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the producto a c x
	 * @return the producto a c x that was removed
	 * @throws NoSuchProductoACXException if a producto a c x with the primary key could not be found
	 */
	@Override
	public ProductoACX remove(Serializable primaryKey)
		throws NoSuchProductoACXException {
		Session session = null;

		try {
			session = openSession();

			ProductoACX productoACX = (ProductoACX)session.get(ProductoACXImpl.class,
					primaryKey);

			if (productoACX == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchProductoACXException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(productoACX);
		}
		catch (NoSuchProductoACXException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ProductoACX removeImpl(ProductoACX productoACX) {
		productoACX = toUnwrappedModel(productoACX);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(productoACX)) {
				productoACX = (ProductoACX)session.get(ProductoACXImpl.class,
						productoACX.getPrimaryKeyObj());
			}

			if (productoACX != null) {
				session.delete(productoACX);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (productoACX != null) {
			clearCache(productoACX);
		}

		return productoACX;
	}

	@Override
	public ProductoACX updateImpl(ProductoACX productoACX) {
		productoACX = toUnwrappedModel(productoACX);

		boolean isNew = productoACX.isNew();

		ProductoACXModelImpl productoACXModelImpl = (ProductoACXModelImpl)productoACX;

		Session session = null;

		try {
			session = openSession();

			if (productoACX.isNew()) {
				session.save(productoACX);

				productoACX.setNew(false);
			}
			else {
				productoACX = (ProductoACX)session.merge(productoACX);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ProductoACXModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((productoACXModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBUDESCRIPCION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						productoACXModelImpl.getOriginalDescripcionProducto()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBUDESCRIPCION,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBUDESCRIPCION,
					args);

				args = new Object[] {
						productoACXModelImpl.getDescripcionProducto()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBUDESCRIPCION,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBUDESCRIPCION,
					args);
			}
		}

		entityCache.putResult(ProductoACXModelImpl.ENTITY_CACHE_ENABLED,
			ProductoACXImpl.class, productoACX.getPrimaryKey(), productoACX,
			false);

		productoACX.resetOriginalValues();

		return productoACX;
	}

	protected ProductoACX toUnwrappedModel(ProductoACX productoACX) {
		if (productoACX instanceof ProductoACXImpl) {
			return productoACX;
		}

		ProductoACXImpl productoACXImpl = new ProductoACXImpl();

		productoACXImpl.setNew(productoACX.isNew());
		productoACXImpl.setPrimaryKey(productoACX.getPrimaryKey());

		productoACXImpl.setCodigoProducto(productoACX.getCodigoProducto());
		productoACXImpl.setDescripcionProducto(productoACX.getDescripcionProducto());
		productoACXImpl.setTipoProducto(productoACX.getTipoProducto());

		return productoACXImpl;
	}

	/**
	 * Returns the producto a c x with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the producto a c x
	 * @return the producto a c x
	 * @throws NoSuchProductoACXException if a producto a c x with the primary key could not be found
	 */
	@Override
	public ProductoACX findByPrimaryKey(Serializable primaryKey)
		throws NoSuchProductoACXException {
		ProductoACX productoACX = fetchByPrimaryKey(primaryKey);

		if (productoACX == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchProductoACXException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return productoACX;
	}

	/**
	 * Returns the producto a c x with the primary key or throws a {@link NoSuchProductoACXException} if it could not be found.
	 *
	 * @param codigoProducto the primary key of the producto a c x
	 * @return the producto a c x
	 * @throws NoSuchProductoACXException if a producto a c x with the primary key could not be found
	 */
	@Override
	public ProductoACX findByPrimaryKey(String codigoProducto)
		throws NoSuchProductoACXException {
		return findByPrimaryKey((Serializable)codigoProducto);
	}

	/**
	 * Returns the producto a c x with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the producto a c x
	 * @return the producto a c x, or <code>null</code> if a producto a c x with the primary key could not be found
	 */
	@Override
	public ProductoACX fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(ProductoACXModelImpl.ENTITY_CACHE_ENABLED,
				ProductoACXImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		ProductoACX productoACX = (ProductoACX)serializable;

		if (productoACX == null) {
			Session session = null;

			try {
				session = openSession();

				productoACX = (ProductoACX)session.get(ProductoACXImpl.class,
						primaryKey);

				if (productoACX != null) {
					cacheResult(productoACX);
				}
				else {
					entityCache.putResult(ProductoACXModelImpl.ENTITY_CACHE_ENABLED,
						ProductoACXImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(ProductoACXModelImpl.ENTITY_CACHE_ENABLED,
					ProductoACXImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return productoACX;
	}

	/**
	 * Returns the producto a c x with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param codigoProducto the primary key of the producto a c x
	 * @return the producto a c x, or <code>null</code> if a producto a c x with the primary key could not be found
	 */
	@Override
	public ProductoACX fetchByPrimaryKey(String codigoProducto) {
		return fetchByPrimaryKey((Serializable)codigoProducto);
	}

	@Override
	public Map<Serializable, ProductoACX> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, ProductoACX> map = new HashMap<Serializable, ProductoACX>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			ProductoACX productoACX = fetchByPrimaryKey(primaryKey);

			if (productoACX != null) {
				map.put(primaryKey, productoACX);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(ProductoACXModelImpl.ENTITY_CACHE_ENABLED,
					ProductoACXImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (ProductoACX)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 4) +
				1);

		query.append(_SQL_SELECT_PRODUCTOACX_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(StringPool.APOSTROPHE);
			query.append((String)primaryKey);
			query.append(StringPool.APOSTROPHE);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (ProductoACX productoACX : (List<ProductoACX>)q.list()) {
				map.put(productoACX.getPrimaryKeyObj(), productoACX);

				cacheResult(productoACX);

				uncachedPrimaryKeys.remove(productoACX.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(ProductoACXModelImpl.ENTITY_CACHE_ENABLED,
					ProductoACXImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the producto a c xs.
	 *
	 * @return the producto a c xs
	 */
	@Override
	public List<ProductoACX> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the producto a c xs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProductoACXModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of producto a c xs
	 * @param end the upper bound of the range of producto a c xs (not inclusive)
	 * @return the range of producto a c xs
	 */
	@Override
	public List<ProductoACX> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the producto a c xs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProductoACXModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of producto a c xs
	 * @param end the upper bound of the range of producto a c xs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of producto a c xs
	 */
	@Override
	public List<ProductoACX> findAll(int start, int end,
		OrderByComparator<ProductoACX> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the producto a c xs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProductoACXModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of producto a c xs
	 * @param end the upper bound of the range of producto a c xs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of producto a c xs
	 */
	@Override
	public List<ProductoACX> findAll(int start, int end,
		OrderByComparator<ProductoACX> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ProductoACX> list = null;

		if (retrieveFromCache) {
			list = (List<ProductoACX>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_PRODUCTOACX);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PRODUCTOACX;

				if (pagination) {
					sql = sql.concat(ProductoACXModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ProductoACX>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProductoACX>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the producto a c xs from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (ProductoACX productoACX : findAll()) {
			remove(productoACX);
		}
	}

	/**
	 * Returns the number of producto a c xs.
	 *
	 * @return the number of producto a c xs
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PRODUCTOACX);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ProductoACXModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the producto a c x persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(ProductoACXImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_PRODUCTOACX = "SELECT productoACX FROM ProductoACX productoACX";
	private static final String _SQL_SELECT_PRODUCTOACX_WHERE_PKS_IN = "SELECT productoACX FROM ProductoACX productoACX WHERE codigoProducto IN (";
	private static final String _SQL_SELECT_PRODUCTOACX_WHERE = "SELECT productoACX FROM ProductoACX productoACX WHERE ";
	private static final String _SQL_COUNT_PRODUCTOACX = "SELECT COUNT(productoACX) FROM ProductoACX productoACX";
	private static final String _SQL_COUNT_PRODUCTOACX_WHERE = "SELECT COUNT(productoACX) FROM ProductoACX productoACX WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "productoACX.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ProductoACX exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ProductoACX exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(ProductoACXPersistenceImpl.class);
}