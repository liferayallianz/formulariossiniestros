/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import mx.com.allianz.service.catalogos.model.Moneda;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Moneda in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Moneda
 * @generated
 */
@ProviderType
public class MonedaCacheModel implements CacheModel<Moneda>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof MonedaCacheModel)) {
			return false;
		}

		MonedaCacheModel monedaCacheModel = (MonedaCacheModel)obj;

		if (idMoneda == monedaCacheModel.idMoneda) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, idMoneda);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{idMoneda=");
		sb.append(idMoneda);
		sb.append(", codigoMoneda=");
		sb.append(codigoMoneda);
		sb.append(", descripcion=");
		sb.append(descripcion);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Moneda toEntityModel() {
		MonedaImpl monedaImpl = new MonedaImpl();

		monedaImpl.setIdMoneda(idMoneda);

		if (codigoMoneda == null) {
			monedaImpl.setCodigoMoneda(StringPool.BLANK);
		}
		else {
			monedaImpl.setCodigoMoneda(codigoMoneda);
		}

		if (descripcion == null) {
			monedaImpl.setDescripcion(StringPool.BLANK);
		}
		else {
			monedaImpl.setDescripcion(descripcion);
		}

		monedaImpl.resetOriginalValues();

		return monedaImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idMoneda = objectInput.readInt();
		codigoMoneda = objectInput.readUTF();
		descripcion = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeInt(idMoneda);

		if (codigoMoneda == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoMoneda);
		}

		if (descripcion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descripcion);
		}
	}

	public int idMoneda;
	public String codigoMoneda;
	public String descripcion;
}