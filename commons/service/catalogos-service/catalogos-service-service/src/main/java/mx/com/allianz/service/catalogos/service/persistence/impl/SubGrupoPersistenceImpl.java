/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import mx.com.allianz.service.catalogos.exception.NoSuchSubGrupoException;
import mx.com.allianz.service.catalogos.model.SubGrupo;
import mx.com.allianz.service.catalogos.model.impl.SubGrupoImpl;
import mx.com.allianz.service.catalogos.model.impl.SubGrupoModelImpl;
import mx.com.allianz.service.catalogos.service.persistence.SubGrupoPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the sub grupo service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SubGrupoPersistence
 * @see mx.com.allianz.service.catalogos.service.persistence.SubGrupoUtil
 * @generated
 */
@ProviderType
public class SubGrupoPersistenceImpl extends BasePersistenceImpl<SubGrupo>
	implements SubGrupoPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link SubGrupoUtil} to access the sub grupo persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = SubGrupoImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(SubGrupoModelImpl.ENTITY_CACHE_ENABLED,
			SubGrupoModelImpl.FINDER_CACHE_ENABLED, SubGrupoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(SubGrupoModelImpl.ENTITY_CACHE_ENABLED,
			SubGrupoModelImpl.FINDER_CACHE_ENABLED, SubGrupoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(SubGrupoModelImpl.ENTITY_CACHE_ENABLED,
			SubGrupoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYNOMBRECONTACTO =
		new FinderPath(SubGrupoModelImpl.ENTITY_CACHE_ENABLED,
			SubGrupoModelImpl.FINDER_CACHE_ENABLED, SubGrupoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByfindByNombreContacto",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYNOMBRECONTACTO =
		new FinderPath(SubGrupoModelImpl.ENTITY_CACHE_ENABLED,
			SubGrupoModelImpl.FINDER_CACHE_ENABLED, SubGrupoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByfindByNombreContacto",
			new String[] { String.class.getName() },
			SubGrupoModelImpl.NOMBRECONTACTO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_FINDBYNOMBRECONTACTO = new FinderPath(SubGrupoModelImpl.ENTITY_CACHE_ENABLED,
			SubGrupoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByfindByNombreContacto",
			new String[] { String.class.getName() });

	/**
	 * Returns all the sub grupos where nombreContacto = &#63;.
	 *
	 * @param nombreContacto the nombre contacto
	 * @return the matching sub grupos
	 */
	@Override
	public List<SubGrupo> findByfindByNombreContacto(String nombreContacto) {
		return findByfindByNombreContacto(nombreContacto, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the sub grupos where nombreContacto = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubGrupoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombreContacto the nombre contacto
	 * @param start the lower bound of the range of sub grupos
	 * @param end the upper bound of the range of sub grupos (not inclusive)
	 * @return the range of matching sub grupos
	 */
	@Override
	public List<SubGrupo> findByfindByNombreContacto(String nombreContacto,
		int start, int end) {
		return findByfindByNombreContacto(nombreContacto, start, end, null);
	}

	/**
	 * Returns an ordered range of all the sub grupos where nombreContacto = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubGrupoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombreContacto the nombre contacto
	 * @param start the lower bound of the range of sub grupos
	 * @param end the upper bound of the range of sub grupos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching sub grupos
	 */
	@Override
	public List<SubGrupo> findByfindByNombreContacto(String nombreContacto,
		int start, int end, OrderByComparator<SubGrupo> orderByComparator) {
		return findByfindByNombreContacto(nombreContacto, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the sub grupos where nombreContacto = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubGrupoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombreContacto the nombre contacto
	 * @param start the lower bound of the range of sub grupos
	 * @param end the upper bound of the range of sub grupos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching sub grupos
	 */
	@Override
	public List<SubGrupo> findByfindByNombreContacto(String nombreContacto,
		int start, int end, OrderByComparator<SubGrupo> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYNOMBRECONTACTO;
			finderArgs = new Object[] { nombreContacto };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FINDBYNOMBRECONTACTO;
			finderArgs = new Object[] {
					nombreContacto,
					
					start, end, orderByComparator
				};
		}

		List<SubGrupo> list = null;

		if (retrieveFromCache) {
			list = (List<SubGrupo>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (SubGrupo subGrupo : list) {
					if (!Objects.equals(nombreContacto,
								subGrupo.getNombreContacto())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_SUBGRUPO_WHERE);

			boolean bindNombreContacto = false;

			if (nombreContacto == null) {
				query.append(_FINDER_COLUMN_FINDBYNOMBRECONTACTO_NOMBRECONTACTO_1);
			}
			else if (nombreContacto.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_FINDBYNOMBRECONTACTO_NOMBRECONTACTO_3);
			}
			else {
				bindNombreContacto = true;

				query.append(_FINDER_COLUMN_FINDBYNOMBRECONTACTO_NOMBRECONTACTO_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(SubGrupoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNombreContacto) {
					qPos.add(nombreContacto);
				}

				if (!pagination) {
					list = (List<SubGrupo>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<SubGrupo>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first sub grupo in the ordered set where nombreContacto = &#63;.
	 *
	 * @param nombreContacto the nombre contacto
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching sub grupo
	 * @throws NoSuchSubGrupoException if a matching sub grupo could not be found
	 */
	@Override
	public SubGrupo findByfindByNombreContacto_First(String nombreContacto,
		OrderByComparator<SubGrupo> orderByComparator)
		throws NoSuchSubGrupoException {
		SubGrupo subGrupo = fetchByfindByNombreContacto_First(nombreContacto,
				orderByComparator);

		if (subGrupo != null) {
			return subGrupo;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("nombreContacto=");
		msg.append(nombreContacto);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchSubGrupoException(msg.toString());
	}

	/**
	 * Returns the first sub grupo in the ordered set where nombreContacto = &#63;.
	 *
	 * @param nombreContacto the nombre contacto
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching sub grupo, or <code>null</code> if a matching sub grupo could not be found
	 */
	@Override
	public SubGrupo fetchByfindByNombreContacto_First(String nombreContacto,
		OrderByComparator<SubGrupo> orderByComparator) {
		List<SubGrupo> list = findByfindByNombreContacto(nombreContacto, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last sub grupo in the ordered set where nombreContacto = &#63;.
	 *
	 * @param nombreContacto the nombre contacto
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching sub grupo
	 * @throws NoSuchSubGrupoException if a matching sub grupo could not be found
	 */
	@Override
	public SubGrupo findByfindByNombreContacto_Last(String nombreContacto,
		OrderByComparator<SubGrupo> orderByComparator)
		throws NoSuchSubGrupoException {
		SubGrupo subGrupo = fetchByfindByNombreContacto_Last(nombreContacto,
				orderByComparator);

		if (subGrupo != null) {
			return subGrupo;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("nombreContacto=");
		msg.append(nombreContacto);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchSubGrupoException(msg.toString());
	}

	/**
	 * Returns the last sub grupo in the ordered set where nombreContacto = &#63;.
	 *
	 * @param nombreContacto the nombre contacto
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching sub grupo, or <code>null</code> if a matching sub grupo could not be found
	 */
	@Override
	public SubGrupo fetchByfindByNombreContacto_Last(String nombreContacto,
		OrderByComparator<SubGrupo> orderByComparator) {
		int count = countByfindByNombreContacto(nombreContacto);

		if (count == 0) {
			return null;
		}

		List<SubGrupo> list = findByfindByNombreContacto(nombreContacto,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the sub grupos before and after the current sub grupo in the ordered set where nombreContacto = &#63;.
	 *
	 * @param idSubGrupo the primary key of the current sub grupo
	 * @param nombreContacto the nombre contacto
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next sub grupo
	 * @throws NoSuchSubGrupoException if a sub grupo with the primary key could not be found
	 */
	@Override
	public SubGrupo[] findByfindByNombreContacto_PrevAndNext(int idSubGrupo,
		String nombreContacto, OrderByComparator<SubGrupo> orderByComparator)
		throws NoSuchSubGrupoException {
		SubGrupo subGrupo = findByPrimaryKey(idSubGrupo);

		Session session = null;

		try {
			session = openSession();

			SubGrupo[] array = new SubGrupoImpl[3];

			array[0] = getByfindByNombreContacto_PrevAndNext(session, subGrupo,
					nombreContacto, orderByComparator, true);

			array[1] = subGrupo;

			array[2] = getByfindByNombreContacto_PrevAndNext(session, subGrupo,
					nombreContacto, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected SubGrupo getByfindByNombreContacto_PrevAndNext(Session session,
		SubGrupo subGrupo, String nombreContacto,
		OrderByComparator<SubGrupo> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_SUBGRUPO_WHERE);

		boolean bindNombreContacto = false;

		if (nombreContacto == null) {
			query.append(_FINDER_COLUMN_FINDBYNOMBRECONTACTO_NOMBRECONTACTO_1);
		}
		else if (nombreContacto.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_FINDBYNOMBRECONTACTO_NOMBRECONTACTO_3);
		}
		else {
			bindNombreContacto = true;

			query.append(_FINDER_COLUMN_FINDBYNOMBRECONTACTO_NOMBRECONTACTO_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(SubGrupoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindNombreContacto) {
			qPos.add(nombreContacto);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(subGrupo);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<SubGrupo> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the sub grupos where nombreContacto = &#63; from the database.
	 *
	 * @param nombreContacto the nombre contacto
	 */
	@Override
	public void removeByfindByNombreContacto(String nombreContacto) {
		for (SubGrupo subGrupo : findByfindByNombreContacto(nombreContacto,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(subGrupo);
		}
	}

	/**
	 * Returns the number of sub grupos where nombreContacto = &#63;.
	 *
	 * @param nombreContacto the nombre contacto
	 * @return the number of matching sub grupos
	 */
	@Override
	public int countByfindByNombreContacto(String nombreContacto) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_FINDBYNOMBRECONTACTO;

		Object[] finderArgs = new Object[] { nombreContacto };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_SUBGRUPO_WHERE);

			boolean bindNombreContacto = false;

			if (nombreContacto == null) {
				query.append(_FINDER_COLUMN_FINDBYNOMBRECONTACTO_NOMBRECONTACTO_1);
			}
			else if (nombreContacto.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_FINDBYNOMBRECONTACTO_NOMBRECONTACTO_3);
			}
			else {
				bindNombreContacto = true;

				query.append(_FINDER_COLUMN_FINDBYNOMBRECONTACTO_NOMBRECONTACTO_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNombreContacto) {
					qPos.add(nombreContacto);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_FINDBYNOMBRECONTACTO_NOMBRECONTACTO_1 =
		"subGrupo.nombreContacto IS NULL";
	private static final String _FINDER_COLUMN_FINDBYNOMBRECONTACTO_NOMBRECONTACTO_2 =
		"subGrupo.nombreContacto = ?";
	private static final String _FINDER_COLUMN_FINDBYNOMBRECONTACTO_NOMBRECONTACTO_3 =
		"(subGrupo.nombreContacto IS NULL OR subGrupo.nombreContacto = '')";

	public SubGrupoPersistenceImpl() {
		setModelClass(SubGrupo.class);
	}

	/**
	 * Caches the sub grupo in the entity cache if it is enabled.
	 *
	 * @param subGrupo the sub grupo
	 */
	@Override
	public void cacheResult(SubGrupo subGrupo) {
		entityCache.putResult(SubGrupoModelImpl.ENTITY_CACHE_ENABLED,
			SubGrupoImpl.class, subGrupo.getPrimaryKey(), subGrupo);

		subGrupo.resetOriginalValues();
	}

	/**
	 * Caches the sub grupos in the entity cache if it is enabled.
	 *
	 * @param subGrupos the sub grupos
	 */
	@Override
	public void cacheResult(List<SubGrupo> subGrupos) {
		for (SubGrupo subGrupo : subGrupos) {
			if (entityCache.getResult(SubGrupoModelImpl.ENTITY_CACHE_ENABLED,
						SubGrupoImpl.class, subGrupo.getPrimaryKey()) == null) {
				cacheResult(subGrupo);
			}
			else {
				subGrupo.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all sub grupos.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(SubGrupoImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the sub grupo.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(SubGrupo subGrupo) {
		entityCache.removeResult(SubGrupoModelImpl.ENTITY_CACHE_ENABLED,
			SubGrupoImpl.class, subGrupo.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<SubGrupo> subGrupos) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (SubGrupo subGrupo : subGrupos) {
			entityCache.removeResult(SubGrupoModelImpl.ENTITY_CACHE_ENABLED,
				SubGrupoImpl.class, subGrupo.getPrimaryKey());
		}
	}

	/**
	 * Creates a new sub grupo with the primary key. Does not add the sub grupo to the database.
	 *
	 * @param idSubGrupo the primary key for the new sub grupo
	 * @return the new sub grupo
	 */
	@Override
	public SubGrupo create(int idSubGrupo) {
		SubGrupo subGrupo = new SubGrupoImpl();

		subGrupo.setNew(true);
		subGrupo.setPrimaryKey(idSubGrupo);

		return subGrupo;
	}

	/**
	 * Removes the sub grupo with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param idSubGrupo the primary key of the sub grupo
	 * @return the sub grupo that was removed
	 * @throws NoSuchSubGrupoException if a sub grupo with the primary key could not be found
	 */
	@Override
	public SubGrupo remove(int idSubGrupo) throws NoSuchSubGrupoException {
		return remove((Serializable)idSubGrupo);
	}

	/**
	 * Removes the sub grupo with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the sub grupo
	 * @return the sub grupo that was removed
	 * @throws NoSuchSubGrupoException if a sub grupo with the primary key could not be found
	 */
	@Override
	public SubGrupo remove(Serializable primaryKey)
		throws NoSuchSubGrupoException {
		Session session = null;

		try {
			session = openSession();

			SubGrupo subGrupo = (SubGrupo)session.get(SubGrupoImpl.class,
					primaryKey);

			if (subGrupo == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchSubGrupoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(subGrupo);
		}
		catch (NoSuchSubGrupoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected SubGrupo removeImpl(SubGrupo subGrupo) {
		subGrupo = toUnwrappedModel(subGrupo);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(subGrupo)) {
				subGrupo = (SubGrupo)session.get(SubGrupoImpl.class,
						subGrupo.getPrimaryKeyObj());
			}

			if (subGrupo != null) {
				session.delete(subGrupo);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (subGrupo != null) {
			clearCache(subGrupo);
		}

		return subGrupo;
	}

	@Override
	public SubGrupo updateImpl(SubGrupo subGrupo) {
		subGrupo = toUnwrappedModel(subGrupo);

		boolean isNew = subGrupo.isNew();

		SubGrupoModelImpl subGrupoModelImpl = (SubGrupoModelImpl)subGrupo;

		Session session = null;

		try {
			session = openSession();

			if (subGrupo.isNew()) {
				session.save(subGrupo);

				subGrupo.setNew(false);
			}
			else {
				subGrupo = (SubGrupo)session.merge(subGrupo);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !SubGrupoModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((subGrupoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYNOMBRECONTACTO.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						subGrupoModelImpl.getOriginalNombreContacto()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBYNOMBRECONTACTO,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYNOMBRECONTACTO,
					args);

				args = new Object[] { subGrupoModelImpl.getNombreContacto() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FINDBYNOMBRECONTACTO,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FINDBYNOMBRECONTACTO,
					args);
			}
		}

		entityCache.putResult(SubGrupoModelImpl.ENTITY_CACHE_ENABLED,
			SubGrupoImpl.class, subGrupo.getPrimaryKey(), subGrupo, false);

		subGrupo.resetOriginalValues();

		return subGrupo;
	}

	protected SubGrupo toUnwrappedModel(SubGrupo subGrupo) {
		if (subGrupo instanceof SubGrupoImpl) {
			return subGrupo;
		}

		SubGrupoImpl subGrupoImpl = new SubGrupoImpl();

		subGrupoImpl.setNew(subGrupo.isNew());
		subGrupoImpl.setPrimaryKey(subGrupo.getPrimaryKey());

		subGrupoImpl.setIdSubGrupo(subGrupo.getIdSubGrupo());
		subGrupoImpl.setCodigoSubGrupo(subGrupo.getCodigoSubGrupo());
		subGrupoImpl.setCodigoGrupoPago(subGrupo.getCodigoGrupoPago());
		subGrupoImpl.setDescripcionSubGrupo(subGrupo.getDescripcionSubGrupo());
		subGrupoImpl.setCodigoPais(subGrupo.getCodigoPais());
		subGrupoImpl.setCodigoEstado(subGrupo.getCodigoEstado());
		subGrupoImpl.setCodigoCiudad(subGrupo.getCodigoCiudad());
		subGrupoImpl.setCodigoMunicipio(subGrupo.getCodigoMunicipio());
		subGrupoImpl.setDireccion(subGrupo.getDireccion());
		subGrupoImpl.setNombreContacto(subGrupo.getNombreContacto());
		subGrupoImpl.setTelefono(subGrupo.getTelefono());
		subGrupoImpl.setZip(subGrupo.getZip());
		subGrupoImpl.setColonia(subGrupo.getColonia());
		subGrupoImpl.setStsCodigoSubGrupo(subGrupo.getStsCodigoSubGrupo());
		subGrupoImpl.setTipoId(subGrupo.getTipoId());
		subGrupoImpl.setNumeroId(subGrupo.getNumeroId());
		subGrupoImpl.setDvId(subGrupo.getDvId());

		return subGrupoImpl;
	}

	/**
	 * Returns the sub grupo with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the sub grupo
	 * @return the sub grupo
	 * @throws NoSuchSubGrupoException if a sub grupo with the primary key could not be found
	 */
	@Override
	public SubGrupo findByPrimaryKey(Serializable primaryKey)
		throws NoSuchSubGrupoException {
		SubGrupo subGrupo = fetchByPrimaryKey(primaryKey);

		if (subGrupo == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchSubGrupoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return subGrupo;
	}

	/**
	 * Returns the sub grupo with the primary key or throws a {@link NoSuchSubGrupoException} if it could not be found.
	 *
	 * @param idSubGrupo the primary key of the sub grupo
	 * @return the sub grupo
	 * @throws NoSuchSubGrupoException if a sub grupo with the primary key could not be found
	 */
	@Override
	public SubGrupo findByPrimaryKey(int idSubGrupo)
		throws NoSuchSubGrupoException {
		return findByPrimaryKey((Serializable)idSubGrupo);
	}

	/**
	 * Returns the sub grupo with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the sub grupo
	 * @return the sub grupo, or <code>null</code> if a sub grupo with the primary key could not be found
	 */
	@Override
	public SubGrupo fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(SubGrupoModelImpl.ENTITY_CACHE_ENABLED,
				SubGrupoImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		SubGrupo subGrupo = (SubGrupo)serializable;

		if (subGrupo == null) {
			Session session = null;

			try {
				session = openSession();

				subGrupo = (SubGrupo)session.get(SubGrupoImpl.class, primaryKey);

				if (subGrupo != null) {
					cacheResult(subGrupo);
				}
				else {
					entityCache.putResult(SubGrupoModelImpl.ENTITY_CACHE_ENABLED,
						SubGrupoImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(SubGrupoModelImpl.ENTITY_CACHE_ENABLED,
					SubGrupoImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return subGrupo;
	}

	/**
	 * Returns the sub grupo with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param idSubGrupo the primary key of the sub grupo
	 * @return the sub grupo, or <code>null</code> if a sub grupo with the primary key could not be found
	 */
	@Override
	public SubGrupo fetchByPrimaryKey(int idSubGrupo) {
		return fetchByPrimaryKey((Serializable)idSubGrupo);
	}

	@Override
	public Map<Serializable, SubGrupo> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, SubGrupo> map = new HashMap<Serializable, SubGrupo>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			SubGrupo subGrupo = fetchByPrimaryKey(primaryKey);

			if (subGrupo != null) {
				map.put(primaryKey, subGrupo);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(SubGrupoModelImpl.ENTITY_CACHE_ENABLED,
					SubGrupoImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (SubGrupo)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_SUBGRUPO_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (SubGrupo subGrupo : (List<SubGrupo>)q.list()) {
				map.put(subGrupo.getPrimaryKeyObj(), subGrupo);

				cacheResult(subGrupo);

				uncachedPrimaryKeys.remove(subGrupo.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(SubGrupoModelImpl.ENTITY_CACHE_ENABLED,
					SubGrupoImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the sub grupos.
	 *
	 * @return the sub grupos
	 */
	@Override
	public List<SubGrupo> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the sub grupos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubGrupoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of sub grupos
	 * @param end the upper bound of the range of sub grupos (not inclusive)
	 * @return the range of sub grupos
	 */
	@Override
	public List<SubGrupo> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the sub grupos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubGrupoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of sub grupos
	 * @param end the upper bound of the range of sub grupos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of sub grupos
	 */
	@Override
	public List<SubGrupo> findAll(int start, int end,
		OrderByComparator<SubGrupo> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the sub grupos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubGrupoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of sub grupos
	 * @param end the upper bound of the range of sub grupos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of sub grupos
	 */
	@Override
	public List<SubGrupo> findAll(int start, int end,
		OrderByComparator<SubGrupo> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<SubGrupo> list = null;

		if (retrieveFromCache) {
			list = (List<SubGrupo>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_SUBGRUPO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_SUBGRUPO;

				if (pagination) {
					sql = sql.concat(SubGrupoModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<SubGrupo>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<SubGrupo>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the sub grupos from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (SubGrupo subGrupo : findAll()) {
			remove(subGrupo);
		}
	}

	/**
	 * Returns the number of sub grupos.
	 *
	 * @return the number of sub grupos
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_SUBGRUPO);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return SubGrupoModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the sub grupo persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(SubGrupoImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_SUBGRUPO = "SELECT subGrupo FROM SubGrupo subGrupo";
	private static final String _SQL_SELECT_SUBGRUPO_WHERE_PKS_IN = "SELECT subGrupo FROM SubGrupo subGrupo WHERE idSubGrupo IN (";
	private static final String _SQL_SELECT_SUBGRUPO_WHERE = "SELECT subGrupo FROM SubGrupo subGrupo WHERE ";
	private static final String _SQL_COUNT_SUBGRUPO = "SELECT COUNT(subGrupo) FROM SubGrupo subGrupo";
	private static final String _SQL_COUNT_SUBGRUPO_WHERE = "SELECT COUNT(subGrupo) FROM SubGrupo subGrupo WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "subGrupo.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No SubGrupo exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No SubGrupo exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(SubGrupoPersistenceImpl.class);
}