/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import mx.com.allianz.service.catalogos.model.SubGrupo;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing SubGrupo in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see SubGrupo
 * @generated
 */
@ProviderType
public class SubGrupoCacheModel implements CacheModel<SubGrupo>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SubGrupoCacheModel)) {
			return false;
		}

		SubGrupoCacheModel subGrupoCacheModel = (SubGrupoCacheModel)obj;

		if (idSubGrupo == subGrupoCacheModel.idSubGrupo) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, idSubGrupo);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(35);

		sb.append("{idSubGrupo=");
		sb.append(idSubGrupo);
		sb.append(", codigoSubGrupo=");
		sb.append(codigoSubGrupo);
		sb.append(", codigoGrupoPago=");
		sb.append(codigoGrupoPago);
		sb.append(", descripcionSubGrupo=");
		sb.append(descripcionSubGrupo);
		sb.append(", codigoPais=");
		sb.append(codigoPais);
		sb.append(", codigoEstado=");
		sb.append(codigoEstado);
		sb.append(", codigoCiudad=");
		sb.append(codigoCiudad);
		sb.append(", codigoMunicipio=");
		sb.append(codigoMunicipio);
		sb.append(", direccion=");
		sb.append(direccion);
		sb.append(", nombreContacto=");
		sb.append(nombreContacto);
		sb.append(", telefono=");
		sb.append(telefono);
		sb.append(", zip=");
		sb.append(zip);
		sb.append(", colonia=");
		sb.append(colonia);
		sb.append(", stsCodigoSubGrupo=");
		sb.append(stsCodigoSubGrupo);
		sb.append(", tipoId=");
		sb.append(tipoId);
		sb.append(", numeroId=");
		sb.append(numeroId);
		sb.append(", dvId=");
		sb.append(dvId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public SubGrupo toEntityModel() {
		SubGrupoImpl subGrupoImpl = new SubGrupoImpl();

		subGrupoImpl.setIdSubGrupo(idSubGrupo);

		if (codigoSubGrupo == null) {
			subGrupoImpl.setCodigoSubGrupo(StringPool.BLANK);
		}
		else {
			subGrupoImpl.setCodigoSubGrupo(codigoSubGrupo);
		}

		if (codigoGrupoPago == null) {
			subGrupoImpl.setCodigoGrupoPago(StringPool.BLANK);
		}
		else {
			subGrupoImpl.setCodigoGrupoPago(codigoGrupoPago);
		}

		if (descripcionSubGrupo == null) {
			subGrupoImpl.setDescripcionSubGrupo(StringPool.BLANK);
		}
		else {
			subGrupoImpl.setDescripcionSubGrupo(descripcionSubGrupo);
		}

		if (codigoPais == null) {
			subGrupoImpl.setCodigoPais(StringPool.BLANK);
		}
		else {
			subGrupoImpl.setCodigoPais(codigoPais);
		}

		if (codigoEstado == null) {
			subGrupoImpl.setCodigoEstado(StringPool.BLANK);
		}
		else {
			subGrupoImpl.setCodigoEstado(codigoEstado);
		}

		if (codigoCiudad == null) {
			subGrupoImpl.setCodigoCiudad(StringPool.BLANK);
		}
		else {
			subGrupoImpl.setCodigoCiudad(codigoCiudad);
		}

		if (codigoMunicipio == null) {
			subGrupoImpl.setCodigoMunicipio(StringPool.BLANK);
		}
		else {
			subGrupoImpl.setCodigoMunicipio(codigoMunicipio);
		}

		if (direccion == null) {
			subGrupoImpl.setDireccion(StringPool.BLANK);
		}
		else {
			subGrupoImpl.setDireccion(direccion);
		}

		if (nombreContacto == null) {
			subGrupoImpl.setNombreContacto(StringPool.BLANK);
		}
		else {
			subGrupoImpl.setNombreContacto(nombreContacto);
		}

		if (telefono == null) {
			subGrupoImpl.setTelefono(StringPool.BLANK);
		}
		else {
			subGrupoImpl.setTelefono(telefono);
		}

		if (zip == null) {
			subGrupoImpl.setZip(StringPool.BLANK);
		}
		else {
			subGrupoImpl.setZip(zip);
		}

		if (colonia == null) {
			subGrupoImpl.setColonia(StringPool.BLANK);
		}
		else {
			subGrupoImpl.setColonia(colonia);
		}

		if (stsCodigoSubGrupo == null) {
			subGrupoImpl.setStsCodigoSubGrupo(StringPool.BLANK);
		}
		else {
			subGrupoImpl.setStsCodigoSubGrupo(stsCodigoSubGrupo);
		}

		if (tipoId == null) {
			subGrupoImpl.setTipoId(StringPool.BLANK);
		}
		else {
			subGrupoImpl.setTipoId(tipoId);
		}

		if (numeroId == null) {
			subGrupoImpl.setNumeroId(StringPool.BLANK);
		}
		else {
			subGrupoImpl.setNumeroId(numeroId);
		}

		if (dvId == null) {
			subGrupoImpl.setDvId(StringPool.BLANK);
		}
		else {
			subGrupoImpl.setDvId(dvId);
		}

		subGrupoImpl.resetOriginalValues();

		return subGrupoImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		idSubGrupo = objectInput.readInt();
		codigoSubGrupo = objectInput.readUTF();
		codigoGrupoPago = objectInput.readUTF();
		descripcionSubGrupo = objectInput.readUTF();
		codigoPais = objectInput.readUTF();
		codigoEstado = objectInput.readUTF();
		codigoCiudad = objectInput.readUTF();
		codigoMunicipio = objectInput.readUTF();
		direccion = objectInput.readUTF();
		nombreContacto = objectInput.readUTF();
		telefono = objectInput.readUTF();
		zip = objectInput.readUTF();
		colonia = objectInput.readUTF();
		stsCodigoSubGrupo = objectInput.readUTF();
		tipoId = objectInput.readUTF();
		numeroId = objectInput.readUTF();
		dvId = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeInt(idSubGrupo);

		if (codigoSubGrupo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoSubGrupo);
		}

		if (codigoGrupoPago == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoGrupoPago);
		}

		if (descripcionSubGrupo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descripcionSubGrupo);
		}

		if (codigoPais == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoPais);
		}

		if (codigoEstado == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoEstado);
		}

		if (codigoCiudad == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoCiudad);
		}

		if (codigoMunicipio == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(codigoMunicipio);
		}

		if (direccion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(direccion);
		}

		if (nombreContacto == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombreContacto);
		}

		if (telefono == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(telefono);
		}

		if (zip == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(zip);
		}

		if (colonia == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(colonia);
		}

		if (stsCodigoSubGrupo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(stsCodigoSubGrupo);
		}

		if (tipoId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipoId);
		}

		if (numeroId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(numeroId);
		}

		if (dvId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(dvId);
		}
	}

	public int idSubGrupo;
	public String codigoSubGrupo;
	public String codigoGrupoPago;
	public String descripcionSubGrupo;
	public String codigoPais;
	public String codigoEstado;
	public String codigoCiudad;
	public String codigoMunicipio;
	public String direccion;
	public String nombreContacto;
	public String telefono;
	public String zip;
	public String colonia;
	public String stsCodigoSubGrupo;
	public String tipoId;
	public String numeroId;
	public String dvId;
}