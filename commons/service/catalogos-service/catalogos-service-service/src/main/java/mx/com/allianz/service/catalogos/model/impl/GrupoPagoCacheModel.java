/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import mx.com.allianz.service.catalogos.model.GrupoPago;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing GrupoPago in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see GrupoPago
 * @generated
 */
@ProviderType
public class GrupoPagoCacheModel implements CacheModel<GrupoPago>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof GrupoPagoCacheModel)) {
			return false;
		}

		GrupoPagoCacheModel grupoPagoCacheModel = (GrupoPagoCacheModel)obj;

		if (codigoGrupoPagos == grupoPagoCacheModel.codigoGrupoPagos) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, codigoGrupoPagos);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{codigoGrupoPagos=");
		sb.append(codigoGrupoPagos);
		sb.append(", descripcionGrupo=");
		sb.append(descripcionGrupo);
		sb.append(", sistemaDirectorioGrupoPago=");
		sb.append(sistemaDirectorioGrupoPago);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public GrupoPago toEntityModel() {
		GrupoPagoImpl grupoPagoImpl = new GrupoPagoImpl();

		grupoPagoImpl.setCodigoGrupoPagos(codigoGrupoPagos);

		if (descripcionGrupo == null) {
			grupoPagoImpl.setDescripcionGrupo(StringPool.BLANK);
		}
		else {
			grupoPagoImpl.setDescripcionGrupo(descripcionGrupo);
		}

		if (sistemaDirectorioGrupoPago == null) {
			grupoPagoImpl.setSistemaDirectorioGrupoPago(StringPool.BLANK);
		}
		else {
			grupoPagoImpl.setSistemaDirectorioGrupoPago(sistemaDirectorioGrupoPago);
		}

		grupoPagoImpl.resetOriginalValues();

		return grupoPagoImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		codigoGrupoPagos = objectInput.readInt();
		descripcionGrupo = objectInput.readUTF();
		sistemaDirectorioGrupoPago = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeInt(codigoGrupoPagos);

		if (descripcionGrupo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descripcionGrupo);
		}

		if (sistemaDirectorioGrupoPago == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(sistemaDirectorioGrupoPago);
		}
	}

	public int codigoGrupoPagos;
	public String descripcionGrupo;
	public String sistemaDirectorioGrupoPago;
}