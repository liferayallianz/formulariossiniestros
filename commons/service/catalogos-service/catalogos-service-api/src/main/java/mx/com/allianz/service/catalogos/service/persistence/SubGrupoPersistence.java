/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import mx.com.allianz.service.catalogos.exception.NoSuchSubGrupoException;
import mx.com.allianz.service.catalogos.model.SubGrupo;

/**
 * The persistence interface for the sub grupo service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see mx.com.allianz.service.catalogos.service.persistence.impl.SubGrupoPersistenceImpl
 * @see SubGrupoUtil
 * @generated
 */
@ProviderType
public interface SubGrupoPersistence extends BasePersistence<SubGrupo> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link SubGrupoUtil} to access the sub grupo persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the sub grupos where nombreContacto = &#63;.
	*
	* @param nombreContacto the nombre contacto
	* @return the matching sub grupos
	*/
	public java.util.List<SubGrupo> findByfindByNombreContacto(
		java.lang.String nombreContacto);

	/**
	* Returns a range of all the sub grupos where nombreContacto = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubGrupoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombreContacto the nombre contacto
	* @param start the lower bound of the range of sub grupos
	* @param end the upper bound of the range of sub grupos (not inclusive)
	* @return the range of matching sub grupos
	*/
	public java.util.List<SubGrupo> findByfindByNombreContacto(
		java.lang.String nombreContacto, int start, int end);

	/**
	* Returns an ordered range of all the sub grupos where nombreContacto = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubGrupoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombreContacto the nombre contacto
	* @param start the lower bound of the range of sub grupos
	* @param end the upper bound of the range of sub grupos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching sub grupos
	*/
	public java.util.List<SubGrupo> findByfindByNombreContacto(
		java.lang.String nombreContacto, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<SubGrupo> orderByComparator);

	/**
	* Returns an ordered range of all the sub grupos where nombreContacto = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubGrupoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombreContacto the nombre contacto
	* @param start the lower bound of the range of sub grupos
	* @param end the upper bound of the range of sub grupos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching sub grupos
	*/
	public java.util.List<SubGrupo> findByfindByNombreContacto(
		java.lang.String nombreContacto, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<SubGrupo> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first sub grupo in the ordered set where nombreContacto = &#63;.
	*
	* @param nombreContacto the nombre contacto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sub grupo
	* @throws NoSuchSubGrupoException if a matching sub grupo could not be found
	*/
	public SubGrupo findByfindByNombreContacto_First(
		java.lang.String nombreContacto,
		com.liferay.portal.kernel.util.OrderByComparator<SubGrupo> orderByComparator)
		throws NoSuchSubGrupoException;

	/**
	* Returns the first sub grupo in the ordered set where nombreContacto = &#63;.
	*
	* @param nombreContacto the nombre contacto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sub grupo, or <code>null</code> if a matching sub grupo could not be found
	*/
	public SubGrupo fetchByfindByNombreContacto_First(
		java.lang.String nombreContacto,
		com.liferay.portal.kernel.util.OrderByComparator<SubGrupo> orderByComparator);

	/**
	* Returns the last sub grupo in the ordered set where nombreContacto = &#63;.
	*
	* @param nombreContacto the nombre contacto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sub grupo
	* @throws NoSuchSubGrupoException if a matching sub grupo could not be found
	*/
	public SubGrupo findByfindByNombreContacto_Last(
		java.lang.String nombreContacto,
		com.liferay.portal.kernel.util.OrderByComparator<SubGrupo> orderByComparator)
		throws NoSuchSubGrupoException;

	/**
	* Returns the last sub grupo in the ordered set where nombreContacto = &#63;.
	*
	* @param nombreContacto the nombre contacto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sub grupo, or <code>null</code> if a matching sub grupo could not be found
	*/
	public SubGrupo fetchByfindByNombreContacto_Last(
		java.lang.String nombreContacto,
		com.liferay.portal.kernel.util.OrderByComparator<SubGrupo> orderByComparator);

	/**
	* Returns the sub grupos before and after the current sub grupo in the ordered set where nombreContacto = &#63;.
	*
	* @param idSubGrupo the primary key of the current sub grupo
	* @param nombreContacto the nombre contacto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next sub grupo
	* @throws NoSuchSubGrupoException if a sub grupo with the primary key could not be found
	*/
	public SubGrupo[] findByfindByNombreContacto_PrevAndNext(int idSubGrupo,
		java.lang.String nombreContacto,
		com.liferay.portal.kernel.util.OrderByComparator<SubGrupo> orderByComparator)
		throws NoSuchSubGrupoException;

	/**
	* Removes all the sub grupos where nombreContacto = &#63; from the database.
	*
	* @param nombreContacto the nombre contacto
	*/
	public void removeByfindByNombreContacto(java.lang.String nombreContacto);

	/**
	* Returns the number of sub grupos where nombreContacto = &#63;.
	*
	* @param nombreContacto the nombre contacto
	* @return the number of matching sub grupos
	*/
	public int countByfindByNombreContacto(java.lang.String nombreContacto);

	/**
	* Caches the sub grupo in the entity cache if it is enabled.
	*
	* @param subGrupo the sub grupo
	*/
	public void cacheResult(SubGrupo subGrupo);

	/**
	* Caches the sub grupos in the entity cache if it is enabled.
	*
	* @param subGrupos the sub grupos
	*/
	public void cacheResult(java.util.List<SubGrupo> subGrupos);

	/**
	* Creates a new sub grupo with the primary key. Does not add the sub grupo to the database.
	*
	* @param idSubGrupo the primary key for the new sub grupo
	* @return the new sub grupo
	*/
	public SubGrupo create(int idSubGrupo);

	/**
	* Removes the sub grupo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idSubGrupo the primary key of the sub grupo
	* @return the sub grupo that was removed
	* @throws NoSuchSubGrupoException if a sub grupo with the primary key could not be found
	*/
	public SubGrupo remove(int idSubGrupo) throws NoSuchSubGrupoException;

	public SubGrupo updateImpl(SubGrupo subGrupo);

	/**
	* Returns the sub grupo with the primary key or throws a {@link NoSuchSubGrupoException} if it could not be found.
	*
	* @param idSubGrupo the primary key of the sub grupo
	* @return the sub grupo
	* @throws NoSuchSubGrupoException if a sub grupo with the primary key could not be found
	*/
	public SubGrupo findByPrimaryKey(int idSubGrupo)
		throws NoSuchSubGrupoException;

	/**
	* Returns the sub grupo with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idSubGrupo the primary key of the sub grupo
	* @return the sub grupo, or <code>null</code> if a sub grupo with the primary key could not be found
	*/
	public SubGrupo fetchByPrimaryKey(int idSubGrupo);

	@Override
	public java.util.Map<java.io.Serializable, SubGrupo> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the sub grupos.
	*
	* @return the sub grupos
	*/
	public java.util.List<SubGrupo> findAll();

	/**
	* Returns a range of all the sub grupos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubGrupoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sub grupos
	* @param end the upper bound of the range of sub grupos (not inclusive)
	* @return the range of sub grupos
	*/
	public java.util.List<SubGrupo> findAll(int start, int end);

	/**
	* Returns an ordered range of all the sub grupos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubGrupoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sub grupos
	* @param end the upper bound of the range of sub grupos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of sub grupos
	*/
	public java.util.List<SubGrupo> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<SubGrupo> orderByComparator);

	/**
	* Returns an ordered range of all the sub grupos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubGrupoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sub grupos
	* @param end the upper bound of the range of sub grupos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of sub grupos
	*/
	public java.util.List<SubGrupo> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<SubGrupo> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the sub grupos from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of sub grupos.
	*
	* @return the number of sub grupos
	*/
	public int countAll();
}