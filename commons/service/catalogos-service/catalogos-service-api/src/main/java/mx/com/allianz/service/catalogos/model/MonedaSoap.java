/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class MonedaSoap implements Serializable {
	public static MonedaSoap toSoapModel(Moneda model) {
		MonedaSoap soapModel = new MonedaSoap();

		soapModel.setIdMoneda(model.getIdMoneda());
		soapModel.setCodigoMoneda(model.getCodigoMoneda());
		soapModel.setDescripcion(model.getDescripcion());

		return soapModel;
	}

	public static MonedaSoap[] toSoapModels(Moneda[] models) {
		MonedaSoap[] soapModels = new MonedaSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static MonedaSoap[][] toSoapModels(Moneda[][] models) {
		MonedaSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new MonedaSoap[models.length][models[0].length];
		}
		else {
			soapModels = new MonedaSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static MonedaSoap[] toSoapModels(List<Moneda> models) {
		List<MonedaSoap> soapModels = new ArrayList<MonedaSoap>(models.size());

		for (Moneda model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new MonedaSoap[soapModels.size()]);
	}

	public MonedaSoap() {
	}

	public int getPrimaryKey() {
		return _idMoneda;
	}

	public void setPrimaryKey(int pk) {
		setIdMoneda(pk);
	}

	public int getIdMoneda() {
		return _idMoneda;
	}

	public void setIdMoneda(int idMoneda) {
		_idMoneda = idMoneda;
	}

	public String getCodigoMoneda() {
		return _codigoMoneda;
	}

	public void setCodigoMoneda(String codigoMoneda) {
		_codigoMoneda = codigoMoneda;
	}

	public String getDescripcion() {
		return _descripcion;
	}

	public void setDescripcion(String descripcion) {
		_descripcion = descripcion;
	}

	private int _idMoneda;
	private String _codigoMoneda;
	private String _descripcion;
}