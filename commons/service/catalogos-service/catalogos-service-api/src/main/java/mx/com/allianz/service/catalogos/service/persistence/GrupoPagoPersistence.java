/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import mx.com.allianz.service.catalogos.exception.NoSuchGrupoPagoException;
import mx.com.allianz.service.catalogos.model.GrupoPago;

/**
 * The persistence interface for the grupo pago service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see mx.com.allianz.service.catalogos.service.persistence.impl.GrupoPagoPersistenceImpl
 * @see GrupoPagoUtil
 * @generated
 */
@ProviderType
public interface GrupoPagoPersistence extends BasePersistence<GrupoPago> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link GrupoPagoUtil} to access the grupo pago persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the grupo pagos where descripcionGrupo = &#63;.
	*
	* @param descripcionGrupo the descripcion grupo
	* @return the matching grupo pagos
	*/
	public java.util.List<GrupoPago> findByfindByDescripcionGrupo(
		java.lang.String descripcionGrupo);

	/**
	* Returns a range of all the grupo pagos where descripcionGrupo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GrupoPagoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcionGrupo the descripcion grupo
	* @param start the lower bound of the range of grupo pagos
	* @param end the upper bound of the range of grupo pagos (not inclusive)
	* @return the range of matching grupo pagos
	*/
	public java.util.List<GrupoPago> findByfindByDescripcionGrupo(
		java.lang.String descripcionGrupo, int start, int end);

	/**
	* Returns an ordered range of all the grupo pagos where descripcionGrupo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GrupoPagoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcionGrupo the descripcion grupo
	* @param start the lower bound of the range of grupo pagos
	* @param end the upper bound of the range of grupo pagos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching grupo pagos
	*/
	public java.util.List<GrupoPago> findByfindByDescripcionGrupo(
		java.lang.String descripcionGrupo, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GrupoPago> orderByComparator);

	/**
	* Returns an ordered range of all the grupo pagos where descripcionGrupo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GrupoPagoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcionGrupo the descripcion grupo
	* @param start the lower bound of the range of grupo pagos
	* @param end the upper bound of the range of grupo pagos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching grupo pagos
	*/
	public java.util.List<GrupoPago> findByfindByDescripcionGrupo(
		java.lang.String descripcionGrupo, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GrupoPago> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first grupo pago in the ordered set where descripcionGrupo = &#63;.
	*
	* @param descripcionGrupo the descripcion grupo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching grupo pago
	* @throws NoSuchGrupoPagoException if a matching grupo pago could not be found
	*/
	public GrupoPago findByfindByDescripcionGrupo_First(
		java.lang.String descripcionGrupo,
		com.liferay.portal.kernel.util.OrderByComparator<GrupoPago> orderByComparator)
		throws NoSuchGrupoPagoException;

	/**
	* Returns the first grupo pago in the ordered set where descripcionGrupo = &#63;.
	*
	* @param descripcionGrupo the descripcion grupo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching grupo pago, or <code>null</code> if a matching grupo pago could not be found
	*/
	public GrupoPago fetchByfindByDescripcionGrupo_First(
		java.lang.String descripcionGrupo,
		com.liferay.portal.kernel.util.OrderByComparator<GrupoPago> orderByComparator);

	/**
	* Returns the last grupo pago in the ordered set where descripcionGrupo = &#63;.
	*
	* @param descripcionGrupo the descripcion grupo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching grupo pago
	* @throws NoSuchGrupoPagoException if a matching grupo pago could not be found
	*/
	public GrupoPago findByfindByDescripcionGrupo_Last(
		java.lang.String descripcionGrupo,
		com.liferay.portal.kernel.util.OrderByComparator<GrupoPago> orderByComparator)
		throws NoSuchGrupoPagoException;

	/**
	* Returns the last grupo pago in the ordered set where descripcionGrupo = &#63;.
	*
	* @param descripcionGrupo the descripcion grupo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching grupo pago, or <code>null</code> if a matching grupo pago could not be found
	*/
	public GrupoPago fetchByfindByDescripcionGrupo_Last(
		java.lang.String descripcionGrupo,
		com.liferay.portal.kernel.util.OrderByComparator<GrupoPago> orderByComparator);

	/**
	* Returns the grupo pagos before and after the current grupo pago in the ordered set where descripcionGrupo = &#63;.
	*
	* @param codigoGrupoPagos the primary key of the current grupo pago
	* @param descripcionGrupo the descripcion grupo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next grupo pago
	* @throws NoSuchGrupoPagoException if a grupo pago with the primary key could not be found
	*/
	public GrupoPago[] findByfindByDescripcionGrupo_PrevAndNext(
		int codigoGrupoPagos, java.lang.String descripcionGrupo,
		com.liferay.portal.kernel.util.OrderByComparator<GrupoPago> orderByComparator)
		throws NoSuchGrupoPagoException;

	/**
	* Removes all the grupo pagos where descripcionGrupo = &#63; from the database.
	*
	* @param descripcionGrupo the descripcion grupo
	*/
	public void removeByfindByDescripcionGrupo(
		java.lang.String descripcionGrupo);

	/**
	* Returns the number of grupo pagos where descripcionGrupo = &#63;.
	*
	* @param descripcionGrupo the descripcion grupo
	* @return the number of matching grupo pagos
	*/
	public int countByfindByDescripcionGrupo(java.lang.String descripcionGrupo);

	/**
	* Caches the grupo pago in the entity cache if it is enabled.
	*
	* @param grupoPago the grupo pago
	*/
	public void cacheResult(GrupoPago grupoPago);

	/**
	* Caches the grupo pagos in the entity cache if it is enabled.
	*
	* @param grupoPagos the grupo pagos
	*/
	public void cacheResult(java.util.List<GrupoPago> grupoPagos);

	/**
	* Creates a new grupo pago with the primary key. Does not add the grupo pago to the database.
	*
	* @param codigoGrupoPagos the primary key for the new grupo pago
	* @return the new grupo pago
	*/
	public GrupoPago create(int codigoGrupoPagos);

	/**
	* Removes the grupo pago with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param codigoGrupoPagos the primary key of the grupo pago
	* @return the grupo pago that was removed
	* @throws NoSuchGrupoPagoException if a grupo pago with the primary key could not be found
	*/
	public GrupoPago remove(int codigoGrupoPagos)
		throws NoSuchGrupoPagoException;

	public GrupoPago updateImpl(GrupoPago grupoPago);

	/**
	* Returns the grupo pago with the primary key or throws a {@link NoSuchGrupoPagoException} if it could not be found.
	*
	* @param codigoGrupoPagos the primary key of the grupo pago
	* @return the grupo pago
	* @throws NoSuchGrupoPagoException if a grupo pago with the primary key could not be found
	*/
	public GrupoPago findByPrimaryKey(int codigoGrupoPagos)
		throws NoSuchGrupoPagoException;

	/**
	* Returns the grupo pago with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param codigoGrupoPagos the primary key of the grupo pago
	* @return the grupo pago, or <code>null</code> if a grupo pago with the primary key could not be found
	*/
	public GrupoPago fetchByPrimaryKey(int codigoGrupoPagos);

	@Override
	public java.util.Map<java.io.Serializable, GrupoPago> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the grupo pagos.
	*
	* @return the grupo pagos
	*/
	public java.util.List<GrupoPago> findAll();

	/**
	* Returns a range of all the grupo pagos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GrupoPagoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of grupo pagos
	* @param end the upper bound of the range of grupo pagos (not inclusive)
	* @return the range of grupo pagos
	*/
	public java.util.List<GrupoPago> findAll(int start, int end);

	/**
	* Returns an ordered range of all the grupo pagos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GrupoPagoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of grupo pagos
	* @param end the upper bound of the range of grupo pagos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of grupo pagos
	*/
	public java.util.List<GrupoPago> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GrupoPago> orderByComparator);

	/**
	* Returns an ordered range of all the grupo pagos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GrupoPagoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of grupo pagos
	* @param end the upper bound of the range of grupo pagos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of grupo pagos
	*/
	public java.util.List<GrupoPago> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GrupoPago> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the grupo pagos from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of grupo pagos.
	*
	* @return the number of grupo pagos
	*/
	public int countAll();
}