/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Clasificador}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Clasificador
 * @generated
 */
@ProviderType
public class ClasificadorWrapper implements Clasificador,
	ModelWrapper<Clasificador> {
	public ClasificadorWrapper(Clasificador clasificador) {
		_clasificador = clasificador;
	}

	@Override
	public Class<?> getModelClass() {
		return Clasificador.class;
	}

	@Override
	public String getModelClassName() {
		return Clasificador.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("idClasificador", getIdClasificador());
		attributes.put("nombre", getNombre());
		attributes.put("descripcion", getDescripcion());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer idClasificador = (Integer)attributes.get("idClasificador");

		if (idClasificador != null) {
			setIdClasificador(idClasificador);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		String descripcion = (String)attributes.get("descripcion");

		if (descripcion != null) {
			setDescripcion(descripcion);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _clasificador.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _clasificador.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _clasificador.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _clasificador.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Clasificador> toCacheModel() {
		return _clasificador.toCacheModel();
	}

	@Override
	public int compareTo(Clasificador clasificador) {
		return _clasificador.compareTo(clasificador);
	}

	/**
	* Returns the id clasificador of this clasificador.
	*
	* @return the id clasificador of this clasificador
	*/
	@Override
	public int getIdClasificador() {
		return _clasificador.getIdClasificador();
	}

	/**
	* Returns the primary key of this clasificador.
	*
	* @return the primary key of this clasificador
	*/
	@Override
	public int getPrimaryKey() {
		return _clasificador.getPrimaryKey();
	}

	@Override
	public int hashCode() {
		return _clasificador.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _clasificador.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ClasificadorWrapper((Clasificador)_clasificador.clone());
	}

	/**
	* Returns the descripcion of this clasificador.
	*
	* @return the descripcion of this clasificador
	*/
	@Override
	public java.lang.String getDescripcion() {
		return _clasificador.getDescripcion();
	}

	/**
	* Returns the nombre of this clasificador.
	*
	* @return the nombre of this clasificador
	*/
	@Override
	public java.lang.String getNombre() {
		return _clasificador.getNombre();
	}

	@Override
	public java.lang.String toString() {
		return _clasificador.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _clasificador.toXmlString();
	}

	@Override
	public Clasificador toEscapedModel() {
		return new ClasificadorWrapper(_clasificador.toEscapedModel());
	}

	@Override
	public Clasificador toUnescapedModel() {
		return new ClasificadorWrapper(_clasificador.toUnescapedModel());
	}

	@Override
	public void persist() {
		_clasificador.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_clasificador.setCachedModel(cachedModel);
	}

	/**
	* Sets the descripcion of this clasificador.
	*
	* @param descripcion the descripcion of this clasificador
	*/
	@Override
	public void setDescripcion(java.lang.String descripcion) {
		_clasificador.setDescripcion(descripcion);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_clasificador.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_clasificador.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_clasificador.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the id clasificador of this clasificador.
	*
	* @param idClasificador the id clasificador of this clasificador
	*/
	@Override
	public void setIdClasificador(int idClasificador) {
		_clasificador.setIdClasificador(idClasificador);
	}

	@Override
	public void setNew(boolean n) {
		_clasificador.setNew(n);
	}

	/**
	* Sets the nombre of this clasificador.
	*
	* @param nombre the nombre of this clasificador
	*/
	@Override
	public void setNombre(java.lang.String nombre) {
		_clasificador.setNombre(nombre);
	}

	/**
	* Sets the primary key of this clasificador.
	*
	* @param primaryKey the primary key of this clasificador
	*/
	@Override
	public void setPrimaryKey(int primaryKey) {
		_clasificador.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_clasificador.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ClasificadorWrapper)) {
			return false;
		}

		ClasificadorWrapper clasificadorWrapper = (ClasificadorWrapper)obj;

		if (Objects.equals(_clasificador, clasificadorWrapper._clasificador)) {
			return true;
		}

		return false;
	}

	@Override
	public Clasificador getWrappedModel() {
		return _clasificador;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _clasificador.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _clasificador.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_clasificador.resetOriginalValues();
	}

	private final Clasificador _clasificador;
}