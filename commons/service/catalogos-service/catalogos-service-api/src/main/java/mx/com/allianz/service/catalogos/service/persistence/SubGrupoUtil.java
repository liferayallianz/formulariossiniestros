/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import mx.com.allianz.service.catalogos.model.SubGrupo;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the sub grupo service. This utility wraps {@link mx.com.allianz.service.catalogos.service.persistence.impl.SubGrupoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SubGrupoPersistence
 * @see mx.com.allianz.service.catalogos.service.persistence.impl.SubGrupoPersistenceImpl
 * @generated
 */
@ProviderType
public class SubGrupoUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(SubGrupo subGrupo) {
		getPersistence().clearCache(subGrupo);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<SubGrupo> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<SubGrupo> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<SubGrupo> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<SubGrupo> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static SubGrupo update(SubGrupo subGrupo) {
		return getPersistence().update(subGrupo);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static SubGrupo update(SubGrupo subGrupo,
		ServiceContext serviceContext) {
		return getPersistence().update(subGrupo, serviceContext);
	}

	/**
	* Returns all the sub grupos where nombreContacto = &#63;.
	*
	* @param nombreContacto the nombre contacto
	* @return the matching sub grupos
	*/
	public static List<SubGrupo> findByfindByNombreContacto(
		java.lang.String nombreContacto) {
		return getPersistence().findByfindByNombreContacto(nombreContacto);
	}

	/**
	* Returns a range of all the sub grupos where nombreContacto = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubGrupoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombreContacto the nombre contacto
	* @param start the lower bound of the range of sub grupos
	* @param end the upper bound of the range of sub grupos (not inclusive)
	* @return the range of matching sub grupos
	*/
	public static List<SubGrupo> findByfindByNombreContacto(
		java.lang.String nombreContacto, int start, int end) {
		return getPersistence()
				   .findByfindByNombreContacto(nombreContacto, start, end);
	}

	/**
	* Returns an ordered range of all the sub grupos where nombreContacto = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubGrupoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombreContacto the nombre contacto
	* @param start the lower bound of the range of sub grupos
	* @param end the upper bound of the range of sub grupos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching sub grupos
	*/
	public static List<SubGrupo> findByfindByNombreContacto(
		java.lang.String nombreContacto, int start, int end,
		OrderByComparator<SubGrupo> orderByComparator) {
		return getPersistence()
				   .findByfindByNombreContacto(nombreContacto, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the sub grupos where nombreContacto = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubGrupoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombreContacto the nombre contacto
	* @param start the lower bound of the range of sub grupos
	* @param end the upper bound of the range of sub grupos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching sub grupos
	*/
	public static List<SubGrupo> findByfindByNombreContacto(
		java.lang.String nombreContacto, int start, int end,
		OrderByComparator<SubGrupo> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findByfindByNombreContacto(nombreContacto, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first sub grupo in the ordered set where nombreContacto = &#63;.
	*
	* @param nombreContacto the nombre contacto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sub grupo
	* @throws NoSuchSubGrupoException if a matching sub grupo could not be found
	*/
	public static SubGrupo findByfindByNombreContacto_First(
		java.lang.String nombreContacto,
		OrderByComparator<SubGrupo> orderByComparator)
		throws mx.com.allianz.service.catalogos.exception.NoSuchSubGrupoException {
		return getPersistence()
				   .findByfindByNombreContacto_First(nombreContacto,
			orderByComparator);
	}

	/**
	* Returns the first sub grupo in the ordered set where nombreContacto = &#63;.
	*
	* @param nombreContacto the nombre contacto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sub grupo, or <code>null</code> if a matching sub grupo could not be found
	*/
	public static SubGrupo fetchByfindByNombreContacto_First(
		java.lang.String nombreContacto,
		OrderByComparator<SubGrupo> orderByComparator) {
		return getPersistence()
				   .fetchByfindByNombreContacto_First(nombreContacto,
			orderByComparator);
	}

	/**
	* Returns the last sub grupo in the ordered set where nombreContacto = &#63;.
	*
	* @param nombreContacto the nombre contacto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sub grupo
	* @throws NoSuchSubGrupoException if a matching sub grupo could not be found
	*/
	public static SubGrupo findByfindByNombreContacto_Last(
		java.lang.String nombreContacto,
		OrderByComparator<SubGrupo> orderByComparator)
		throws mx.com.allianz.service.catalogos.exception.NoSuchSubGrupoException {
		return getPersistence()
				   .findByfindByNombreContacto_Last(nombreContacto,
			orderByComparator);
	}

	/**
	* Returns the last sub grupo in the ordered set where nombreContacto = &#63;.
	*
	* @param nombreContacto the nombre contacto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sub grupo, or <code>null</code> if a matching sub grupo could not be found
	*/
	public static SubGrupo fetchByfindByNombreContacto_Last(
		java.lang.String nombreContacto,
		OrderByComparator<SubGrupo> orderByComparator) {
		return getPersistence()
				   .fetchByfindByNombreContacto_Last(nombreContacto,
			orderByComparator);
	}

	/**
	* Returns the sub grupos before and after the current sub grupo in the ordered set where nombreContacto = &#63;.
	*
	* @param idSubGrupo the primary key of the current sub grupo
	* @param nombreContacto the nombre contacto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next sub grupo
	* @throws NoSuchSubGrupoException if a sub grupo with the primary key could not be found
	*/
	public static SubGrupo[] findByfindByNombreContacto_PrevAndNext(
		int idSubGrupo, java.lang.String nombreContacto,
		OrderByComparator<SubGrupo> orderByComparator)
		throws mx.com.allianz.service.catalogos.exception.NoSuchSubGrupoException {
		return getPersistence()
				   .findByfindByNombreContacto_PrevAndNext(idSubGrupo,
			nombreContacto, orderByComparator);
	}

	/**
	* Removes all the sub grupos where nombreContacto = &#63; from the database.
	*
	* @param nombreContacto the nombre contacto
	*/
	public static void removeByfindByNombreContacto(
		java.lang.String nombreContacto) {
		getPersistence().removeByfindByNombreContacto(nombreContacto);
	}

	/**
	* Returns the number of sub grupos where nombreContacto = &#63;.
	*
	* @param nombreContacto the nombre contacto
	* @return the number of matching sub grupos
	*/
	public static int countByfindByNombreContacto(
		java.lang.String nombreContacto) {
		return getPersistence().countByfindByNombreContacto(nombreContacto);
	}

	/**
	* Caches the sub grupo in the entity cache if it is enabled.
	*
	* @param subGrupo the sub grupo
	*/
	public static void cacheResult(SubGrupo subGrupo) {
		getPersistence().cacheResult(subGrupo);
	}

	/**
	* Caches the sub grupos in the entity cache if it is enabled.
	*
	* @param subGrupos the sub grupos
	*/
	public static void cacheResult(List<SubGrupo> subGrupos) {
		getPersistence().cacheResult(subGrupos);
	}

	/**
	* Creates a new sub grupo with the primary key. Does not add the sub grupo to the database.
	*
	* @param idSubGrupo the primary key for the new sub grupo
	* @return the new sub grupo
	*/
	public static SubGrupo create(int idSubGrupo) {
		return getPersistence().create(idSubGrupo);
	}

	/**
	* Removes the sub grupo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idSubGrupo the primary key of the sub grupo
	* @return the sub grupo that was removed
	* @throws NoSuchSubGrupoException if a sub grupo with the primary key could not be found
	*/
	public static SubGrupo remove(int idSubGrupo)
		throws mx.com.allianz.service.catalogos.exception.NoSuchSubGrupoException {
		return getPersistence().remove(idSubGrupo);
	}

	public static SubGrupo updateImpl(SubGrupo subGrupo) {
		return getPersistence().updateImpl(subGrupo);
	}

	/**
	* Returns the sub grupo with the primary key or throws a {@link NoSuchSubGrupoException} if it could not be found.
	*
	* @param idSubGrupo the primary key of the sub grupo
	* @return the sub grupo
	* @throws NoSuchSubGrupoException if a sub grupo with the primary key could not be found
	*/
	public static SubGrupo findByPrimaryKey(int idSubGrupo)
		throws mx.com.allianz.service.catalogos.exception.NoSuchSubGrupoException {
		return getPersistence().findByPrimaryKey(idSubGrupo);
	}

	/**
	* Returns the sub grupo with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idSubGrupo the primary key of the sub grupo
	* @return the sub grupo, or <code>null</code> if a sub grupo with the primary key could not be found
	*/
	public static SubGrupo fetchByPrimaryKey(int idSubGrupo) {
		return getPersistence().fetchByPrimaryKey(idSubGrupo);
	}

	public static java.util.Map<java.io.Serializable, SubGrupo> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the sub grupos.
	*
	* @return the sub grupos
	*/
	public static List<SubGrupo> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the sub grupos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubGrupoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sub grupos
	* @param end the upper bound of the range of sub grupos (not inclusive)
	* @return the range of sub grupos
	*/
	public static List<SubGrupo> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the sub grupos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubGrupoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sub grupos
	* @param end the upper bound of the range of sub grupos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of sub grupos
	*/
	public static List<SubGrupo> findAll(int start, int end,
		OrderByComparator<SubGrupo> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the sub grupos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SubGrupoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sub grupos
	* @param end the upper bound of the range of sub grupos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of sub grupos
	*/
	public static List<SubGrupo> findAll(int start, int end,
		OrderByComparator<SubGrupo> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the sub grupos from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of sub grupos.
	*
	* @return the number of sub grupos
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static SubGrupoPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<SubGrupoPersistence, SubGrupoPersistence> _serviceTracker =
		ServiceTrackerFactory.open(SubGrupoPersistence.class);
}