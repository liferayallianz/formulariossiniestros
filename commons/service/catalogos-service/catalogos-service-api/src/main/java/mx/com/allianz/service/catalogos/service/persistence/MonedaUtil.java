/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import mx.com.allianz.service.catalogos.model.Moneda;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the moneda service. This utility wraps {@link mx.com.allianz.service.catalogos.service.persistence.impl.MonedaPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MonedaPersistence
 * @see mx.com.allianz.service.catalogos.service.persistence.impl.MonedaPersistenceImpl
 * @generated
 */
@ProviderType
public class MonedaUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Moneda moneda) {
		getPersistence().clearCache(moneda);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Moneda> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Moneda> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Moneda> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator<Moneda> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Moneda update(Moneda moneda) {
		return getPersistence().update(moneda);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Moneda update(Moneda moneda, ServiceContext serviceContext) {
		return getPersistence().update(moneda, serviceContext);
	}

	/**
	* Returns all the monedas where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the matching monedas
	*/
	public static List<Moneda> findByfindByDescripcion(
		java.lang.String descripcion) {
		return getPersistence().findByfindByDescripcion(descripcion);
	}

	/**
	* Returns a range of all the monedas where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MonedaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of monedas
	* @param end the upper bound of the range of monedas (not inclusive)
	* @return the range of matching monedas
	*/
	public static List<Moneda> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end) {
		return getPersistence().findByfindByDescripcion(descripcion, start, end);
	}

	/**
	* Returns an ordered range of all the monedas where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MonedaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of monedas
	* @param end the upper bound of the range of monedas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching monedas
	*/
	public static List<Moneda> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		OrderByComparator<Moneda> orderByComparator) {
		return getPersistence()
				   .findByfindByDescripcion(descripcion, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the monedas where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MonedaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of monedas
	* @param end the upper bound of the range of monedas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching monedas
	*/
	public static List<Moneda> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		OrderByComparator<Moneda> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findByfindByDescripcion(descripcion, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first moneda in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching moneda
	* @throws NoSuchMonedaException if a matching moneda could not be found
	*/
	public static Moneda findByfindByDescripcion_First(
		java.lang.String descripcion,
		OrderByComparator<Moneda> orderByComparator)
		throws mx.com.allianz.service.catalogos.exception.NoSuchMonedaException {
		return getPersistence()
				   .findByfindByDescripcion_First(descripcion, orderByComparator);
	}

	/**
	* Returns the first moneda in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching moneda, or <code>null</code> if a matching moneda could not be found
	*/
	public static Moneda fetchByfindByDescripcion_First(
		java.lang.String descripcion,
		OrderByComparator<Moneda> orderByComparator) {
		return getPersistence()
				   .fetchByfindByDescripcion_First(descripcion,
			orderByComparator);
	}

	/**
	* Returns the last moneda in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching moneda
	* @throws NoSuchMonedaException if a matching moneda could not be found
	*/
	public static Moneda findByfindByDescripcion_Last(
		java.lang.String descripcion,
		OrderByComparator<Moneda> orderByComparator)
		throws mx.com.allianz.service.catalogos.exception.NoSuchMonedaException {
		return getPersistence()
				   .findByfindByDescripcion_Last(descripcion, orderByComparator);
	}

	/**
	* Returns the last moneda in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching moneda, or <code>null</code> if a matching moneda could not be found
	*/
	public static Moneda fetchByfindByDescripcion_Last(
		java.lang.String descripcion,
		OrderByComparator<Moneda> orderByComparator) {
		return getPersistence()
				   .fetchByfindByDescripcion_Last(descripcion, orderByComparator);
	}

	/**
	* Returns the monedas before and after the current moneda in the ordered set where descripcion = &#63;.
	*
	* @param idMoneda the primary key of the current moneda
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next moneda
	* @throws NoSuchMonedaException if a moneda with the primary key could not be found
	*/
	public static Moneda[] findByfindByDescripcion_PrevAndNext(int idMoneda,
		java.lang.String descripcion,
		OrderByComparator<Moneda> orderByComparator)
		throws mx.com.allianz.service.catalogos.exception.NoSuchMonedaException {
		return getPersistence()
				   .findByfindByDescripcion_PrevAndNext(idMoneda, descripcion,
			orderByComparator);
	}

	/**
	* Removes all the monedas where descripcion = &#63; from the database.
	*
	* @param descripcion the descripcion
	*/
	public static void removeByfindByDescripcion(java.lang.String descripcion) {
		getPersistence().removeByfindByDescripcion(descripcion);
	}

	/**
	* Returns the number of monedas where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the number of matching monedas
	*/
	public static int countByfindByDescripcion(java.lang.String descripcion) {
		return getPersistence().countByfindByDescripcion(descripcion);
	}

	/**
	* Caches the moneda in the entity cache if it is enabled.
	*
	* @param moneda the moneda
	*/
	public static void cacheResult(Moneda moneda) {
		getPersistence().cacheResult(moneda);
	}

	/**
	* Caches the monedas in the entity cache if it is enabled.
	*
	* @param monedas the monedas
	*/
	public static void cacheResult(List<Moneda> monedas) {
		getPersistence().cacheResult(monedas);
	}

	/**
	* Creates a new moneda with the primary key. Does not add the moneda to the database.
	*
	* @param idMoneda the primary key for the new moneda
	* @return the new moneda
	*/
	public static Moneda create(int idMoneda) {
		return getPersistence().create(idMoneda);
	}

	/**
	* Removes the moneda with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idMoneda the primary key of the moneda
	* @return the moneda that was removed
	* @throws NoSuchMonedaException if a moneda with the primary key could not be found
	*/
	public static Moneda remove(int idMoneda)
		throws mx.com.allianz.service.catalogos.exception.NoSuchMonedaException {
		return getPersistence().remove(idMoneda);
	}

	public static Moneda updateImpl(Moneda moneda) {
		return getPersistence().updateImpl(moneda);
	}

	/**
	* Returns the moneda with the primary key or throws a {@link NoSuchMonedaException} if it could not be found.
	*
	* @param idMoneda the primary key of the moneda
	* @return the moneda
	* @throws NoSuchMonedaException if a moneda with the primary key could not be found
	*/
	public static Moneda findByPrimaryKey(int idMoneda)
		throws mx.com.allianz.service.catalogos.exception.NoSuchMonedaException {
		return getPersistence().findByPrimaryKey(idMoneda);
	}

	/**
	* Returns the moneda with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idMoneda the primary key of the moneda
	* @return the moneda, or <code>null</code> if a moneda with the primary key could not be found
	*/
	public static Moneda fetchByPrimaryKey(int idMoneda) {
		return getPersistence().fetchByPrimaryKey(idMoneda);
	}

	public static java.util.Map<java.io.Serializable, Moneda> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the monedas.
	*
	* @return the monedas
	*/
	public static List<Moneda> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the monedas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MonedaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of monedas
	* @param end the upper bound of the range of monedas (not inclusive)
	* @return the range of monedas
	*/
	public static List<Moneda> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the monedas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MonedaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of monedas
	* @param end the upper bound of the range of monedas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of monedas
	*/
	public static List<Moneda> findAll(int start, int end,
		OrderByComparator<Moneda> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the monedas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link MonedaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of monedas
	* @param end the upper bound of the range of monedas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of monedas
	*/
	public static List<Moneda> findAll(int start, int end,
		OrderByComparator<Moneda> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the monedas from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of monedas.
	*
	* @return the number of monedas
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static MonedaPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<MonedaPersistence, MonedaPersistence> _serviceTracker =
		ServiceTrackerFactory.open(MonedaPersistence.class);
}