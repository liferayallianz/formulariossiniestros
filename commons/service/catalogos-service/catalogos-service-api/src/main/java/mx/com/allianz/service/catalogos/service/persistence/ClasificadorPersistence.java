/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import mx.com.allianz.service.catalogos.exception.NoSuchClasificadorException;
import mx.com.allianz.service.catalogos.model.Clasificador;

/**
 * The persistence interface for the clasificador service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see mx.com.allianz.service.catalogos.service.persistence.impl.ClasificadorPersistenceImpl
 * @see ClasificadorUtil
 * @generated
 */
@ProviderType
public interface ClasificadorPersistence extends BasePersistence<Clasificador> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ClasificadorUtil} to access the clasificador persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the clasificadors where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the matching clasificadors
	*/
	public java.util.List<Clasificador> findByfindByDescripcion(
		java.lang.String descripcion);

	/**
	* Returns a range of all the clasificadors where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ClasificadorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of clasificadors
	* @param end the upper bound of the range of clasificadors (not inclusive)
	* @return the range of matching clasificadors
	*/
	public java.util.List<Clasificador> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end);

	/**
	* Returns an ordered range of all the clasificadors where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ClasificadorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of clasificadors
	* @param end the upper bound of the range of clasificadors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching clasificadors
	*/
	public java.util.List<Clasificador> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Clasificador> orderByComparator);

	/**
	* Returns an ordered range of all the clasificadors where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ClasificadorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of clasificadors
	* @param end the upper bound of the range of clasificadors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching clasificadors
	*/
	public java.util.List<Clasificador> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Clasificador> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first clasificador in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching clasificador
	* @throws NoSuchClasificadorException if a matching clasificador could not be found
	*/
	public Clasificador findByfindByDescripcion_First(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Clasificador> orderByComparator)
		throws NoSuchClasificadorException;

	/**
	* Returns the first clasificador in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching clasificador, or <code>null</code> if a matching clasificador could not be found
	*/
	public Clasificador fetchByfindByDescripcion_First(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Clasificador> orderByComparator);

	/**
	* Returns the last clasificador in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching clasificador
	* @throws NoSuchClasificadorException if a matching clasificador could not be found
	*/
	public Clasificador findByfindByDescripcion_Last(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Clasificador> orderByComparator)
		throws NoSuchClasificadorException;

	/**
	* Returns the last clasificador in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching clasificador, or <code>null</code> if a matching clasificador could not be found
	*/
	public Clasificador fetchByfindByDescripcion_Last(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Clasificador> orderByComparator);

	/**
	* Returns the clasificadors before and after the current clasificador in the ordered set where descripcion = &#63;.
	*
	* @param idClasificador the primary key of the current clasificador
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next clasificador
	* @throws NoSuchClasificadorException if a clasificador with the primary key could not be found
	*/
	public Clasificador[] findByfindByDescripcion_PrevAndNext(
		int idClasificador, java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Clasificador> orderByComparator)
		throws NoSuchClasificadorException;

	/**
	* Removes all the clasificadors where descripcion = &#63; from the database.
	*
	* @param descripcion the descripcion
	*/
	public void removeByfindByDescripcion(java.lang.String descripcion);

	/**
	* Returns the number of clasificadors where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the number of matching clasificadors
	*/
	public int countByfindByDescripcion(java.lang.String descripcion);

	/**
	* Caches the clasificador in the entity cache if it is enabled.
	*
	* @param clasificador the clasificador
	*/
	public void cacheResult(Clasificador clasificador);

	/**
	* Caches the clasificadors in the entity cache if it is enabled.
	*
	* @param clasificadors the clasificadors
	*/
	public void cacheResult(java.util.List<Clasificador> clasificadors);

	/**
	* Creates a new clasificador with the primary key. Does not add the clasificador to the database.
	*
	* @param idClasificador the primary key for the new clasificador
	* @return the new clasificador
	*/
	public Clasificador create(int idClasificador);

	/**
	* Removes the clasificador with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idClasificador the primary key of the clasificador
	* @return the clasificador that was removed
	* @throws NoSuchClasificadorException if a clasificador with the primary key could not be found
	*/
	public Clasificador remove(int idClasificador)
		throws NoSuchClasificadorException;

	public Clasificador updateImpl(Clasificador clasificador);

	/**
	* Returns the clasificador with the primary key or throws a {@link NoSuchClasificadorException} if it could not be found.
	*
	* @param idClasificador the primary key of the clasificador
	* @return the clasificador
	* @throws NoSuchClasificadorException if a clasificador with the primary key could not be found
	*/
	public Clasificador findByPrimaryKey(int idClasificador)
		throws NoSuchClasificadorException;

	/**
	* Returns the clasificador with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idClasificador the primary key of the clasificador
	* @return the clasificador, or <code>null</code> if a clasificador with the primary key could not be found
	*/
	public Clasificador fetchByPrimaryKey(int idClasificador);

	@Override
	public java.util.Map<java.io.Serializable, Clasificador> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the clasificadors.
	*
	* @return the clasificadors
	*/
	public java.util.List<Clasificador> findAll();

	/**
	* Returns a range of all the clasificadors.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ClasificadorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of clasificadors
	* @param end the upper bound of the range of clasificadors (not inclusive)
	* @return the range of clasificadors
	*/
	public java.util.List<Clasificador> findAll(int start, int end);

	/**
	* Returns an ordered range of all the clasificadors.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ClasificadorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of clasificadors
	* @param end the upper bound of the range of clasificadors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of clasificadors
	*/
	public java.util.List<Clasificador> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Clasificador> orderByComparator);

	/**
	* Returns an ordered range of all the clasificadors.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ClasificadorModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of clasificadors
	* @param end the upper bound of the range of clasificadors (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of clasificadors
	*/
	public java.util.List<Clasificador> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Clasificador> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the clasificadors from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of clasificadors.
	*
	* @return the number of clasificadors
	*/
	public int countAll();
}