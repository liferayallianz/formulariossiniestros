/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link GrupoPagoLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see GrupoPagoLocalService
 * @generated
 */
@ProviderType
public class GrupoPagoLocalServiceWrapper implements GrupoPagoLocalService,
	ServiceWrapper<GrupoPagoLocalService> {
	public GrupoPagoLocalServiceWrapper(
		GrupoPagoLocalService grupoPagoLocalService) {
		_grupoPagoLocalService = grupoPagoLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _grupoPagoLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _grupoPagoLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _grupoPagoLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _grupoPagoLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _grupoPagoLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of grupo pagos.
	*
	* @return the number of grupo pagos
	*/
	@Override
	public int getGrupoPagosCount() {
		return _grupoPagoLocalService.getGrupoPagosCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _grupoPagoLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _grupoPagoLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.catalogos.model.impl.GrupoPagoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _grupoPagoLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.catalogos.model.impl.GrupoPagoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _grupoPagoLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the grupo pagos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.catalogos.model.impl.GrupoPagoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of grupo pagos
	* @param end the upper bound of the range of grupo pagos (not inclusive)
	* @return the range of grupo pagos
	*/
	@Override
	public java.util.List<mx.com.allianz.service.catalogos.model.GrupoPago> getGrupoPagos(
		int start, int end) {
		return _grupoPagoLocalService.getGrupoPagos(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _grupoPagoLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _grupoPagoLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Adds the grupo pago to the database. Also notifies the appropriate model listeners.
	*
	* @param grupoPago the grupo pago
	* @return the grupo pago that was added
	*/
	@Override
	public mx.com.allianz.service.catalogos.model.GrupoPago addGrupoPago(
		mx.com.allianz.service.catalogos.model.GrupoPago grupoPago) {
		return _grupoPagoLocalService.addGrupoPago(grupoPago);
	}

	/**
	* Creates a new grupo pago with the primary key. Does not add the grupo pago to the database.
	*
	* @param codigoGrupoPagos the primary key for the new grupo pago
	* @return the new grupo pago
	*/
	@Override
	public mx.com.allianz.service.catalogos.model.GrupoPago createGrupoPago(
		int codigoGrupoPagos) {
		return _grupoPagoLocalService.createGrupoPago(codigoGrupoPagos);
	}

	/**
	* Deletes the grupo pago with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param codigoGrupoPagos the primary key of the grupo pago
	* @return the grupo pago that was removed
	* @throws PortalException if a grupo pago with the primary key could not be found
	*/
	@Override
	public mx.com.allianz.service.catalogos.model.GrupoPago deleteGrupoPago(
		int codigoGrupoPagos)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _grupoPagoLocalService.deleteGrupoPago(codigoGrupoPagos);
	}

	/**
	* Deletes the grupo pago from the database. Also notifies the appropriate model listeners.
	*
	* @param grupoPago the grupo pago
	* @return the grupo pago that was removed
	*/
	@Override
	public mx.com.allianz.service.catalogos.model.GrupoPago deleteGrupoPago(
		mx.com.allianz.service.catalogos.model.GrupoPago grupoPago) {
		return _grupoPagoLocalService.deleteGrupoPago(grupoPago);
	}

	@Override
	public mx.com.allianz.service.catalogos.model.GrupoPago fetchGrupoPago(
		int codigoGrupoPagos) {
		return _grupoPagoLocalService.fetchGrupoPago(codigoGrupoPagos);
	}

	/**
	* Returns the grupo pago with the primary key.
	*
	* @param codigoGrupoPagos the primary key of the grupo pago
	* @return the grupo pago
	* @throws PortalException if a grupo pago with the primary key could not be found
	*/
	@Override
	public mx.com.allianz.service.catalogos.model.GrupoPago getGrupoPago(
		int codigoGrupoPagos)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _grupoPagoLocalService.getGrupoPago(codigoGrupoPagos);
	}

	/**
	* Updates the grupo pago in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param grupoPago the grupo pago
	* @return the grupo pago that was updated
	*/
	@Override
	public mx.com.allianz.service.catalogos.model.GrupoPago updateGrupoPago(
		mx.com.allianz.service.catalogos.model.GrupoPago grupoPago) {
		return _grupoPagoLocalService.updateGrupoPago(grupoPago);
	}

	@Override
	public GrupoPagoLocalService getWrappedService() {
		return _grupoPagoLocalService;
	}

	@Override
	public void setWrappedService(GrupoPagoLocalService grupoPagoLocalService) {
		_grupoPagoLocalService = grupoPagoLocalService;
	}

	private GrupoPagoLocalService _grupoPagoLocalService;
}