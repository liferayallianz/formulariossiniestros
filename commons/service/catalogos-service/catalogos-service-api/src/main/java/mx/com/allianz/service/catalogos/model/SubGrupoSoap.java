/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class SubGrupoSoap implements Serializable {
	public static SubGrupoSoap toSoapModel(SubGrupo model) {
		SubGrupoSoap soapModel = new SubGrupoSoap();

		soapModel.setIdSubGrupo(model.getIdSubGrupo());
		soapModel.setCodigoSubGrupo(model.getCodigoSubGrupo());
		soapModel.setCodigoGrupoPago(model.getCodigoGrupoPago());
		soapModel.setDescripcionSubGrupo(model.getDescripcionSubGrupo());
		soapModel.setCodigoPais(model.getCodigoPais());
		soapModel.setCodigoEstado(model.getCodigoEstado());
		soapModel.setCodigoCiudad(model.getCodigoCiudad());
		soapModel.setCodigoMunicipio(model.getCodigoMunicipio());
		soapModel.setDireccion(model.getDireccion());
		soapModel.setNombreContacto(model.getNombreContacto());
		soapModel.setTelefono(model.getTelefono());
		soapModel.setZip(model.getZip());
		soapModel.setColonia(model.getColonia());
		soapModel.setStsCodigoSubGrupo(model.getStsCodigoSubGrupo());
		soapModel.setTipoId(model.getTipoId());
		soapModel.setNumeroId(model.getNumeroId());
		soapModel.setDvId(model.getDvId());

		return soapModel;
	}

	public static SubGrupoSoap[] toSoapModels(SubGrupo[] models) {
		SubGrupoSoap[] soapModels = new SubGrupoSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static SubGrupoSoap[][] toSoapModels(SubGrupo[][] models) {
		SubGrupoSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new SubGrupoSoap[models.length][models[0].length];
		}
		else {
			soapModels = new SubGrupoSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static SubGrupoSoap[] toSoapModels(List<SubGrupo> models) {
		List<SubGrupoSoap> soapModels = new ArrayList<SubGrupoSoap>(models.size());

		for (SubGrupo model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new SubGrupoSoap[soapModels.size()]);
	}

	public SubGrupoSoap() {
	}

	public int getPrimaryKey() {
		return _idSubGrupo;
	}

	public void setPrimaryKey(int pk) {
		setIdSubGrupo(pk);
	}

	public int getIdSubGrupo() {
		return _idSubGrupo;
	}

	public void setIdSubGrupo(int idSubGrupo) {
		_idSubGrupo = idSubGrupo;
	}

	public String getCodigoSubGrupo() {
		return _codigoSubGrupo;
	}

	public void setCodigoSubGrupo(String codigoSubGrupo) {
		_codigoSubGrupo = codigoSubGrupo;
	}

	public String getCodigoGrupoPago() {
		return _codigoGrupoPago;
	}

	public void setCodigoGrupoPago(String codigoGrupoPago) {
		_codigoGrupoPago = codigoGrupoPago;
	}

	public String getDescripcionSubGrupo() {
		return _descripcionSubGrupo;
	}

	public void setDescripcionSubGrupo(String descripcionSubGrupo) {
		_descripcionSubGrupo = descripcionSubGrupo;
	}

	public String getCodigoPais() {
		return _codigoPais;
	}

	public void setCodigoPais(String codigoPais) {
		_codigoPais = codigoPais;
	}

	public String getCodigoEstado() {
		return _codigoEstado;
	}

	public void setCodigoEstado(String codigoEstado) {
		_codigoEstado = codigoEstado;
	}

	public String getCodigoCiudad() {
		return _codigoCiudad;
	}

	public void setCodigoCiudad(String codigoCiudad) {
		_codigoCiudad = codigoCiudad;
	}

	public String getCodigoMunicipio() {
		return _codigoMunicipio;
	}

	public void setCodigoMunicipio(String codigoMunicipio) {
		_codigoMunicipio = codigoMunicipio;
	}

	public String getDireccion() {
		return _direccion;
	}

	public void setDireccion(String direccion) {
		_direccion = direccion;
	}

	public String getNombreContacto() {
		return _nombreContacto;
	}

	public void setNombreContacto(String nombreContacto) {
		_nombreContacto = nombreContacto;
	}

	public String getTelefono() {
		return _telefono;
	}

	public void setTelefono(String telefono) {
		_telefono = telefono;
	}

	public String getZip() {
		return _zip;
	}

	public void setZip(String zip) {
		_zip = zip;
	}

	public String getColonia() {
		return _colonia;
	}

	public void setColonia(String colonia) {
		_colonia = colonia;
	}

	public String getStsCodigoSubGrupo() {
		return _stsCodigoSubGrupo;
	}

	public void setStsCodigoSubGrupo(String stsCodigoSubGrupo) {
		_stsCodigoSubGrupo = stsCodigoSubGrupo;
	}

	public String getTipoId() {
		return _tipoId;
	}

	public void setTipoId(String tipoId) {
		_tipoId = tipoId;
	}

	public String getNumeroId() {
		return _numeroId;
	}

	public void setNumeroId(String numeroId) {
		_numeroId = numeroId;
	}

	public String getDvId() {
		return _dvId;
	}

	public void setDvId(String dvId) {
		_dvId = dvId;
	}

	private int _idSubGrupo;
	private String _codigoSubGrupo;
	private String _codigoGrupoPago;
	private String _descripcionSubGrupo;
	private String _codigoPais;
	private String _codigoEstado;
	private String _codigoCiudad;
	private String _codigoMunicipio;
	private String _direccion;
	private String _nombreContacto;
	private String _telefono;
	private String _zip;
	private String _colonia;
	private String _stsCodigoSubGrupo;
	private String _tipoId;
	private String _numeroId;
	private String _dvId;
}