/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import mx.com.allianz.service.catalogos.exception.NoSuchDocumentoException;
import mx.com.allianz.service.catalogos.model.Documento;

/**
 * The persistence interface for the documento service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see mx.com.allianz.service.catalogos.service.persistence.impl.DocumentoPersistenceImpl
 * @see DocumentoUtil
 * @generated
 */
@ProviderType
public interface DocumentoPersistence extends BasePersistence<Documento> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link DocumentoUtil} to access the documento persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the documentos where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the matching documentos
	*/
	public java.util.List<Documento> findByfindByDescripcion(
		java.lang.String descripcion);

	/**
	* Returns a range of all the documentos where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of documentos
	* @param end the upper bound of the range of documentos (not inclusive)
	* @return the range of matching documentos
	*/
	public java.util.List<Documento> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end);

	/**
	* Returns an ordered range of all the documentos where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of documentos
	* @param end the upper bound of the range of documentos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching documentos
	*/
	public java.util.List<Documento> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Documento> orderByComparator);

	/**
	* Returns an ordered range of all the documentos where descripcion = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcion the descripcion
	* @param start the lower bound of the range of documentos
	* @param end the upper bound of the range of documentos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching documentos
	*/
	public java.util.List<Documento> findByfindByDescripcion(
		java.lang.String descripcion, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Documento> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first documento in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento
	* @throws NoSuchDocumentoException if a matching documento could not be found
	*/
	public Documento findByfindByDescripcion_First(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Documento> orderByComparator)
		throws NoSuchDocumentoException;

	/**
	* Returns the first documento in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento, or <code>null</code> if a matching documento could not be found
	*/
	public Documento fetchByfindByDescripcion_First(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Documento> orderByComparator);

	/**
	* Returns the last documento in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento
	* @throws NoSuchDocumentoException if a matching documento could not be found
	*/
	public Documento findByfindByDescripcion_Last(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Documento> orderByComparator)
		throws NoSuchDocumentoException;

	/**
	* Returns the last documento in the ordered set where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento, or <code>null</code> if a matching documento could not be found
	*/
	public Documento fetchByfindByDescripcion_Last(
		java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Documento> orderByComparator);

	/**
	* Returns the documentos before and after the current documento in the ordered set where descripcion = &#63;.
	*
	* @param idDocumentos the primary key of the current documento
	* @param descripcion the descripcion
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next documento
	* @throws NoSuchDocumentoException if a documento with the primary key could not be found
	*/
	public Documento[] findByfindByDescripcion_PrevAndNext(
		java.lang.String idDocumentos, java.lang.String descripcion,
		com.liferay.portal.kernel.util.OrderByComparator<Documento> orderByComparator)
		throws NoSuchDocumentoException;

	/**
	* Removes all the documentos where descripcion = &#63; from the database.
	*
	* @param descripcion the descripcion
	*/
	public void removeByfindByDescripcion(java.lang.String descripcion);

	/**
	* Returns the number of documentos where descripcion = &#63;.
	*
	* @param descripcion the descripcion
	* @return the number of matching documentos
	*/
	public int countByfindByDescripcion(java.lang.String descripcion);

	/**
	* Caches the documento in the entity cache if it is enabled.
	*
	* @param documento the documento
	*/
	public void cacheResult(Documento documento);

	/**
	* Caches the documentos in the entity cache if it is enabled.
	*
	* @param documentos the documentos
	*/
	public void cacheResult(java.util.List<Documento> documentos);

	/**
	* Creates a new documento with the primary key. Does not add the documento to the database.
	*
	* @param idDocumentos the primary key for the new documento
	* @return the new documento
	*/
	public Documento create(java.lang.String idDocumentos);

	/**
	* Removes the documento with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idDocumentos the primary key of the documento
	* @return the documento that was removed
	* @throws NoSuchDocumentoException if a documento with the primary key could not be found
	*/
	public Documento remove(java.lang.String idDocumentos)
		throws NoSuchDocumentoException;

	public Documento updateImpl(Documento documento);

	/**
	* Returns the documento with the primary key or throws a {@link NoSuchDocumentoException} if it could not be found.
	*
	* @param idDocumentos the primary key of the documento
	* @return the documento
	* @throws NoSuchDocumentoException if a documento with the primary key could not be found
	*/
	public Documento findByPrimaryKey(java.lang.String idDocumentos)
		throws NoSuchDocumentoException;

	/**
	* Returns the documento with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param idDocumentos the primary key of the documento
	* @return the documento, or <code>null</code> if a documento with the primary key could not be found
	*/
	public Documento fetchByPrimaryKey(java.lang.String idDocumentos);

	@Override
	public java.util.Map<java.io.Serializable, Documento> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the documentos.
	*
	* @return the documentos
	*/
	public java.util.List<Documento> findAll();

	/**
	* Returns a range of all the documentos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of documentos
	* @param end the upper bound of the range of documentos (not inclusive)
	* @return the range of documentos
	*/
	public java.util.List<Documento> findAll(int start, int end);

	/**
	* Returns an ordered range of all the documentos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of documentos
	* @param end the upper bound of the range of documentos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of documentos
	*/
	public java.util.List<Documento> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Documento> orderByComparator);

	/**
	* Returns an ordered range of all the documentos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of documentos
	* @param end the upper bound of the range of documentos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of documentos
	*/
	public java.util.List<Documento> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Documento> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the documentos from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of documentos.
	*
	* @return the number of documentos
	*/
	public int countAll();
}