/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class HospitalSoap implements Serializable {
	public static HospitalSoap toSoapModel(Hospital model) {
		HospitalSoap soapModel = new HospitalSoap();

		soapModel.setIdHospital(model.getIdHospital());
		soapModel.setCodigoGrupo(model.getCodigoGrupo());
		soapModel.setCodigoSubGrupo(model.getCodigoSubGrupo());
		soapModel.setDescripcionHospital(model.getDescripcionHospital());
		soapModel.setCodigoEstado(model.getCodigoEstado());

		return soapModel;
	}

	public static HospitalSoap[] toSoapModels(Hospital[] models) {
		HospitalSoap[] soapModels = new HospitalSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static HospitalSoap[][] toSoapModels(Hospital[][] models) {
		HospitalSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new HospitalSoap[models.length][models[0].length];
		}
		else {
			soapModels = new HospitalSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static HospitalSoap[] toSoapModels(List<Hospital> models) {
		List<HospitalSoap> soapModels = new ArrayList<HospitalSoap>(models.size());

		for (Hospital model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new HospitalSoap[soapModels.size()]);
	}

	public HospitalSoap() {
	}

	public int getPrimaryKey() {
		return _idHospital;
	}

	public void setPrimaryKey(int pk) {
		setIdHospital(pk);
	}

	public int getIdHospital() {
		return _idHospital;
	}

	public void setIdHospital(int idHospital) {
		_idHospital = idHospital;
	}

	public int getCodigoGrupo() {
		return _codigoGrupo;
	}

	public void setCodigoGrupo(int codigoGrupo) {
		_codigoGrupo = codigoGrupo;
	}

	public int getCodigoSubGrupo() {
		return _codigoSubGrupo;
	}

	public void setCodigoSubGrupo(int codigoSubGrupo) {
		_codigoSubGrupo = codigoSubGrupo;
	}

	public String getDescripcionHospital() {
		return _descripcionHospital;
	}

	public void setDescripcionHospital(String descripcionHospital) {
		_descripcionHospital = descripcionHospital;
	}

	public int getCodigoEstado() {
		return _codigoEstado;
	}

	public void setCodigoEstado(int codigoEstado) {
		_codigoEstado = codigoEstado;
	}

	private int _idHospital;
	private int _codigoGrupo;
	private int _codigoSubGrupo;
	private String _descripcionHospital;
	private int _codigoEstado;
}