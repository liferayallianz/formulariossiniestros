/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import mx.com.allianz.service.catalogos.exception.NoSuchProductoACXException;
import mx.com.allianz.service.catalogos.model.ProductoACX;

/**
 * The persistence interface for the producto a c x service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see mx.com.allianz.service.catalogos.service.persistence.impl.ProductoACXPersistenceImpl
 * @see ProductoACXUtil
 * @generated
 */
@ProviderType
public interface ProductoACXPersistence extends BasePersistence<ProductoACX> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ProductoACXUtil} to access the producto a c x persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the producto a c xs where descripcionProducto = &#63;.
	*
	* @param descripcionProducto the descripcion producto
	* @return the matching producto a c xs
	*/
	public java.util.List<ProductoACX> findByfindBuDescripcion(
		java.lang.String descripcionProducto);

	/**
	* Returns a range of all the producto a c xs where descripcionProducto = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProductoACXModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcionProducto the descripcion producto
	* @param start the lower bound of the range of producto a c xs
	* @param end the upper bound of the range of producto a c xs (not inclusive)
	* @return the range of matching producto a c xs
	*/
	public java.util.List<ProductoACX> findByfindBuDescripcion(
		java.lang.String descripcionProducto, int start, int end);

	/**
	* Returns an ordered range of all the producto a c xs where descripcionProducto = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProductoACXModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcionProducto the descripcion producto
	* @param start the lower bound of the range of producto a c xs
	* @param end the upper bound of the range of producto a c xs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching producto a c xs
	*/
	public java.util.List<ProductoACX> findByfindBuDescripcion(
		java.lang.String descripcionProducto, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProductoACX> orderByComparator);

	/**
	* Returns an ordered range of all the producto a c xs where descripcionProducto = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProductoACXModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcionProducto the descripcion producto
	* @param start the lower bound of the range of producto a c xs
	* @param end the upper bound of the range of producto a c xs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching producto a c xs
	*/
	public java.util.List<ProductoACX> findByfindBuDescripcion(
		java.lang.String descripcionProducto, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProductoACX> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first producto a c x in the ordered set where descripcionProducto = &#63;.
	*
	* @param descripcionProducto the descripcion producto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching producto a c x
	* @throws NoSuchProductoACXException if a matching producto a c x could not be found
	*/
	public ProductoACX findByfindBuDescripcion_First(
		java.lang.String descripcionProducto,
		com.liferay.portal.kernel.util.OrderByComparator<ProductoACX> orderByComparator)
		throws NoSuchProductoACXException;

	/**
	* Returns the first producto a c x in the ordered set where descripcionProducto = &#63;.
	*
	* @param descripcionProducto the descripcion producto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching producto a c x, or <code>null</code> if a matching producto a c x could not be found
	*/
	public ProductoACX fetchByfindBuDescripcion_First(
		java.lang.String descripcionProducto,
		com.liferay.portal.kernel.util.OrderByComparator<ProductoACX> orderByComparator);

	/**
	* Returns the last producto a c x in the ordered set where descripcionProducto = &#63;.
	*
	* @param descripcionProducto the descripcion producto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching producto a c x
	* @throws NoSuchProductoACXException if a matching producto a c x could not be found
	*/
	public ProductoACX findByfindBuDescripcion_Last(
		java.lang.String descripcionProducto,
		com.liferay.portal.kernel.util.OrderByComparator<ProductoACX> orderByComparator)
		throws NoSuchProductoACXException;

	/**
	* Returns the last producto a c x in the ordered set where descripcionProducto = &#63;.
	*
	* @param descripcionProducto the descripcion producto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching producto a c x, or <code>null</code> if a matching producto a c x could not be found
	*/
	public ProductoACX fetchByfindBuDescripcion_Last(
		java.lang.String descripcionProducto,
		com.liferay.portal.kernel.util.OrderByComparator<ProductoACX> orderByComparator);

	/**
	* Returns the producto a c xs before and after the current producto a c x in the ordered set where descripcionProducto = &#63;.
	*
	* @param codigoProducto the primary key of the current producto a c x
	* @param descripcionProducto the descripcion producto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next producto a c x
	* @throws NoSuchProductoACXException if a producto a c x with the primary key could not be found
	*/
	public ProductoACX[] findByfindBuDescripcion_PrevAndNext(
		java.lang.String codigoProducto, java.lang.String descripcionProducto,
		com.liferay.portal.kernel.util.OrderByComparator<ProductoACX> orderByComparator)
		throws NoSuchProductoACXException;

	/**
	* Removes all the producto a c xs where descripcionProducto = &#63; from the database.
	*
	* @param descripcionProducto the descripcion producto
	*/
	public void removeByfindBuDescripcion(java.lang.String descripcionProducto);

	/**
	* Returns the number of producto a c xs where descripcionProducto = &#63;.
	*
	* @param descripcionProducto the descripcion producto
	* @return the number of matching producto a c xs
	*/
	public int countByfindBuDescripcion(java.lang.String descripcionProducto);

	/**
	* Caches the producto a c x in the entity cache if it is enabled.
	*
	* @param productoACX the producto a c x
	*/
	public void cacheResult(ProductoACX productoACX);

	/**
	* Caches the producto a c xs in the entity cache if it is enabled.
	*
	* @param productoACXs the producto a c xs
	*/
	public void cacheResult(java.util.List<ProductoACX> productoACXs);

	/**
	* Creates a new producto a c x with the primary key. Does not add the producto a c x to the database.
	*
	* @param codigoProducto the primary key for the new producto a c x
	* @return the new producto a c x
	*/
	public ProductoACX create(java.lang.String codigoProducto);

	/**
	* Removes the producto a c x with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param codigoProducto the primary key of the producto a c x
	* @return the producto a c x that was removed
	* @throws NoSuchProductoACXException if a producto a c x with the primary key could not be found
	*/
	public ProductoACX remove(java.lang.String codigoProducto)
		throws NoSuchProductoACXException;

	public ProductoACX updateImpl(ProductoACX productoACX);

	/**
	* Returns the producto a c x with the primary key or throws a {@link NoSuchProductoACXException} if it could not be found.
	*
	* @param codigoProducto the primary key of the producto a c x
	* @return the producto a c x
	* @throws NoSuchProductoACXException if a producto a c x with the primary key could not be found
	*/
	public ProductoACX findByPrimaryKey(java.lang.String codigoProducto)
		throws NoSuchProductoACXException;

	/**
	* Returns the producto a c x with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param codigoProducto the primary key of the producto a c x
	* @return the producto a c x, or <code>null</code> if a producto a c x with the primary key could not be found
	*/
	public ProductoACX fetchByPrimaryKey(java.lang.String codigoProducto);

	@Override
	public java.util.Map<java.io.Serializable, ProductoACX> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the producto a c xs.
	*
	* @return the producto a c xs
	*/
	public java.util.List<ProductoACX> findAll();

	/**
	* Returns a range of all the producto a c xs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProductoACXModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of producto a c xs
	* @param end the upper bound of the range of producto a c xs (not inclusive)
	* @return the range of producto a c xs
	*/
	public java.util.List<ProductoACX> findAll(int start, int end);

	/**
	* Returns an ordered range of all the producto a c xs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProductoACXModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of producto a c xs
	* @param end the upper bound of the range of producto a c xs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of producto a c xs
	*/
	public java.util.List<ProductoACX> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProductoACX> orderByComparator);

	/**
	* Returns an ordered range of all the producto a c xs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProductoACXModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of producto a c xs
	* @param end the upper bound of the range of producto a c xs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of producto a c xs
	*/
	public java.util.List<ProductoACX> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProductoACX> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the producto a c xs from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of producto a c xs.
	*
	* @return the number of producto a c xs
	*/
	public int countAll();
}