/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class ClasificadorSoap implements Serializable {
	public static ClasificadorSoap toSoapModel(Clasificador model) {
		ClasificadorSoap soapModel = new ClasificadorSoap();

		soapModel.setIdClasificador(model.getIdClasificador());
		soapModel.setNombre(model.getNombre());
		soapModel.setDescripcion(model.getDescripcion());

		return soapModel;
	}

	public static ClasificadorSoap[] toSoapModels(Clasificador[] models) {
		ClasificadorSoap[] soapModels = new ClasificadorSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ClasificadorSoap[][] toSoapModels(Clasificador[][] models) {
		ClasificadorSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ClasificadorSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ClasificadorSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ClasificadorSoap[] toSoapModels(List<Clasificador> models) {
		List<ClasificadorSoap> soapModels = new ArrayList<ClasificadorSoap>(models.size());

		for (Clasificador model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ClasificadorSoap[soapModels.size()]);
	}

	public ClasificadorSoap() {
	}

	public int getPrimaryKey() {
		return _idClasificador;
	}

	public void setPrimaryKey(int pk) {
		setIdClasificador(pk);
	}

	public int getIdClasificador() {
		return _idClasificador;
	}

	public void setIdClasificador(int idClasificador) {
		_idClasificador = idClasificador;
	}

	public String getNombre() {
		return _nombre;
	}

	public void setNombre(String nombre) {
		_nombre = nombre;
	}

	public String getDescripcion() {
		return _descripcion;
	}

	public void setDescripcion(String descripcion) {
		_descripcion = descripcion;
	}

	private int _idClasificador;
	private String _nombre;
	private String _descripcion;
}