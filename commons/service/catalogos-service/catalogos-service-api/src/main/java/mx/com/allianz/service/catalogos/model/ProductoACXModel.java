/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

/**
 * The base model interface for the ProductoACX service. Represents a row in the &quot;PLT_CAT_PRODUCTO_ACX&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link mx.com.allianz.service.catalogos.model.impl.ProductoACXModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link mx.com.allianz.service.catalogos.model.impl.ProductoACXImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ProductoACX
 * @see mx.com.allianz.service.catalogos.model.impl.ProductoACXImpl
 * @see mx.com.allianz.service.catalogos.model.impl.ProductoACXModelImpl
 * @generated
 */
@ProviderType
public interface ProductoACXModel extends BaseModel<ProductoACX> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a producto a c x model instance should use the {@link ProductoACX} interface instead.
	 */

	/**
	 * Returns the primary key of this producto a c x.
	 *
	 * @return the primary key of this producto a c x
	 */
	public String getPrimaryKey();

	/**
	 * Sets the primary key of this producto a c x.
	 *
	 * @param primaryKey the primary key of this producto a c x
	 */
	public void setPrimaryKey(String primaryKey);

	/**
	 * Returns the codigo producto of this producto a c x.
	 *
	 * @return the codigo producto of this producto a c x
	 */
	@AutoEscape
	public String getCodigoProducto();

	/**
	 * Sets the codigo producto of this producto a c x.
	 *
	 * @param codigoProducto the codigo producto of this producto a c x
	 */
	public void setCodigoProducto(String codigoProducto);

	/**
	 * Returns the descripcion producto of this producto a c x.
	 *
	 * @return the descripcion producto of this producto a c x
	 */
	@AutoEscape
	public String getDescripcionProducto();

	/**
	 * Sets the descripcion producto of this producto a c x.
	 *
	 * @param descripcionProducto the descripcion producto of this producto a c x
	 */
	public void setDescripcionProducto(String descripcionProducto);

	/**
	 * Returns the tipo producto of this producto a c x.
	 *
	 * @return the tipo producto of this producto a c x
	 */
	@AutoEscape
	public String getTipoProducto();

	/**
	 * Sets the tipo producto of this producto a c x.
	 *
	 * @param tipoProducto the tipo producto of this producto a c x
	 */
	public void setTipoProducto(String tipoProducto);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(ProductoACX productoACX);

	@Override
	public int hashCode();

	@Override
	public CacheModel<ProductoACX> toCacheModel();

	@Override
	public ProductoACX toEscapedModel();

	@Override
	public ProductoACX toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}