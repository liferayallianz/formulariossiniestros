/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import mx.com.allianz.service.catalogos.model.ProductoACX;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the producto a c x service. This utility wraps {@link mx.com.allianz.service.catalogos.service.persistence.impl.ProductoACXPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ProductoACXPersistence
 * @see mx.com.allianz.service.catalogos.service.persistence.impl.ProductoACXPersistenceImpl
 * @generated
 */
@ProviderType
public class ProductoACXUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(ProductoACX productoACX) {
		getPersistence().clearCache(productoACX);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ProductoACX> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ProductoACX> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ProductoACX> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<ProductoACX> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static ProductoACX update(ProductoACX productoACX) {
		return getPersistence().update(productoACX);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static ProductoACX update(ProductoACX productoACX,
		ServiceContext serviceContext) {
		return getPersistence().update(productoACX, serviceContext);
	}

	/**
	* Returns all the producto a c xs where descripcionProducto = &#63;.
	*
	* @param descripcionProducto the descripcion producto
	* @return the matching producto a c xs
	*/
	public static List<ProductoACX> findByfindBuDescripcion(
		java.lang.String descripcionProducto) {
		return getPersistence().findByfindBuDescripcion(descripcionProducto);
	}

	/**
	* Returns a range of all the producto a c xs where descripcionProducto = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProductoACXModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcionProducto the descripcion producto
	* @param start the lower bound of the range of producto a c xs
	* @param end the upper bound of the range of producto a c xs (not inclusive)
	* @return the range of matching producto a c xs
	*/
	public static List<ProductoACX> findByfindBuDescripcion(
		java.lang.String descripcionProducto, int start, int end) {
		return getPersistence()
				   .findByfindBuDescripcion(descripcionProducto, start, end);
	}

	/**
	* Returns an ordered range of all the producto a c xs where descripcionProducto = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProductoACXModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcionProducto the descripcion producto
	* @param start the lower bound of the range of producto a c xs
	* @param end the upper bound of the range of producto a c xs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching producto a c xs
	*/
	public static List<ProductoACX> findByfindBuDescripcion(
		java.lang.String descripcionProducto, int start, int end,
		OrderByComparator<ProductoACX> orderByComparator) {
		return getPersistence()
				   .findByfindBuDescripcion(descripcionProducto, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the producto a c xs where descripcionProducto = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProductoACXModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcionProducto the descripcion producto
	* @param start the lower bound of the range of producto a c xs
	* @param end the upper bound of the range of producto a c xs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching producto a c xs
	*/
	public static List<ProductoACX> findByfindBuDescripcion(
		java.lang.String descripcionProducto, int start, int end,
		OrderByComparator<ProductoACX> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByfindBuDescripcion(descripcionProducto, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first producto a c x in the ordered set where descripcionProducto = &#63;.
	*
	* @param descripcionProducto the descripcion producto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching producto a c x
	* @throws NoSuchProductoACXException if a matching producto a c x could not be found
	*/
	public static ProductoACX findByfindBuDescripcion_First(
		java.lang.String descripcionProducto,
		OrderByComparator<ProductoACX> orderByComparator)
		throws mx.com.allianz.service.catalogos.exception.NoSuchProductoACXException {
		return getPersistence()
				   .findByfindBuDescripcion_First(descripcionProducto,
			orderByComparator);
	}

	/**
	* Returns the first producto a c x in the ordered set where descripcionProducto = &#63;.
	*
	* @param descripcionProducto the descripcion producto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching producto a c x, or <code>null</code> if a matching producto a c x could not be found
	*/
	public static ProductoACX fetchByfindBuDescripcion_First(
		java.lang.String descripcionProducto,
		OrderByComparator<ProductoACX> orderByComparator) {
		return getPersistence()
				   .fetchByfindBuDescripcion_First(descripcionProducto,
			orderByComparator);
	}

	/**
	* Returns the last producto a c x in the ordered set where descripcionProducto = &#63;.
	*
	* @param descripcionProducto the descripcion producto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching producto a c x
	* @throws NoSuchProductoACXException if a matching producto a c x could not be found
	*/
	public static ProductoACX findByfindBuDescripcion_Last(
		java.lang.String descripcionProducto,
		OrderByComparator<ProductoACX> orderByComparator)
		throws mx.com.allianz.service.catalogos.exception.NoSuchProductoACXException {
		return getPersistence()
				   .findByfindBuDescripcion_Last(descripcionProducto,
			orderByComparator);
	}

	/**
	* Returns the last producto a c x in the ordered set where descripcionProducto = &#63;.
	*
	* @param descripcionProducto the descripcion producto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching producto a c x, or <code>null</code> if a matching producto a c x could not be found
	*/
	public static ProductoACX fetchByfindBuDescripcion_Last(
		java.lang.String descripcionProducto,
		OrderByComparator<ProductoACX> orderByComparator) {
		return getPersistence()
				   .fetchByfindBuDescripcion_Last(descripcionProducto,
			orderByComparator);
	}

	/**
	* Returns the producto a c xs before and after the current producto a c x in the ordered set where descripcionProducto = &#63;.
	*
	* @param codigoProducto the primary key of the current producto a c x
	* @param descripcionProducto the descripcion producto
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next producto a c x
	* @throws NoSuchProductoACXException if a producto a c x with the primary key could not be found
	*/
	public static ProductoACX[] findByfindBuDescripcion_PrevAndNext(
		java.lang.String codigoProducto, java.lang.String descripcionProducto,
		OrderByComparator<ProductoACX> orderByComparator)
		throws mx.com.allianz.service.catalogos.exception.NoSuchProductoACXException {
		return getPersistence()
				   .findByfindBuDescripcion_PrevAndNext(codigoProducto,
			descripcionProducto, orderByComparator);
	}

	/**
	* Removes all the producto a c xs where descripcionProducto = &#63; from the database.
	*
	* @param descripcionProducto the descripcion producto
	*/
	public static void removeByfindBuDescripcion(
		java.lang.String descripcionProducto) {
		getPersistence().removeByfindBuDescripcion(descripcionProducto);
	}

	/**
	* Returns the number of producto a c xs where descripcionProducto = &#63;.
	*
	* @param descripcionProducto the descripcion producto
	* @return the number of matching producto a c xs
	*/
	public static int countByfindBuDescripcion(
		java.lang.String descripcionProducto) {
		return getPersistence().countByfindBuDescripcion(descripcionProducto);
	}

	/**
	* Caches the producto a c x in the entity cache if it is enabled.
	*
	* @param productoACX the producto a c x
	*/
	public static void cacheResult(ProductoACX productoACX) {
		getPersistence().cacheResult(productoACX);
	}

	/**
	* Caches the producto a c xs in the entity cache if it is enabled.
	*
	* @param productoACXs the producto a c xs
	*/
	public static void cacheResult(List<ProductoACX> productoACXs) {
		getPersistence().cacheResult(productoACXs);
	}

	/**
	* Creates a new producto a c x with the primary key. Does not add the producto a c x to the database.
	*
	* @param codigoProducto the primary key for the new producto a c x
	* @return the new producto a c x
	*/
	public static ProductoACX create(java.lang.String codigoProducto) {
		return getPersistence().create(codigoProducto);
	}

	/**
	* Removes the producto a c x with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param codigoProducto the primary key of the producto a c x
	* @return the producto a c x that was removed
	* @throws NoSuchProductoACXException if a producto a c x with the primary key could not be found
	*/
	public static ProductoACX remove(java.lang.String codigoProducto)
		throws mx.com.allianz.service.catalogos.exception.NoSuchProductoACXException {
		return getPersistence().remove(codigoProducto);
	}

	public static ProductoACX updateImpl(ProductoACX productoACX) {
		return getPersistence().updateImpl(productoACX);
	}

	/**
	* Returns the producto a c x with the primary key or throws a {@link NoSuchProductoACXException} if it could not be found.
	*
	* @param codigoProducto the primary key of the producto a c x
	* @return the producto a c x
	* @throws NoSuchProductoACXException if a producto a c x with the primary key could not be found
	*/
	public static ProductoACX findByPrimaryKey(java.lang.String codigoProducto)
		throws mx.com.allianz.service.catalogos.exception.NoSuchProductoACXException {
		return getPersistence().findByPrimaryKey(codigoProducto);
	}

	/**
	* Returns the producto a c x with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param codigoProducto the primary key of the producto a c x
	* @return the producto a c x, or <code>null</code> if a producto a c x with the primary key could not be found
	*/
	public static ProductoACX fetchByPrimaryKey(java.lang.String codigoProducto) {
		return getPersistence().fetchByPrimaryKey(codigoProducto);
	}

	public static java.util.Map<java.io.Serializable, ProductoACX> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the producto a c xs.
	*
	* @return the producto a c xs
	*/
	public static List<ProductoACX> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the producto a c xs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProductoACXModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of producto a c xs
	* @param end the upper bound of the range of producto a c xs (not inclusive)
	* @return the range of producto a c xs
	*/
	public static List<ProductoACX> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the producto a c xs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProductoACXModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of producto a c xs
	* @param end the upper bound of the range of producto a c xs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of producto a c xs
	*/
	public static List<ProductoACX> findAll(int start, int end,
		OrderByComparator<ProductoACX> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the producto a c xs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProductoACXModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of producto a c xs
	* @param end the upper bound of the range of producto a c xs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of producto a c xs
	*/
	public static List<ProductoACX> findAll(int start, int end,
		OrderByComparator<ProductoACX> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the producto a c xs from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of producto a c xs.
	*
	* @return the number of producto a c xs
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static ProductoACXPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ProductoACXPersistence, ProductoACXPersistence> _serviceTracker =
		ServiceTrackerFactory.open(ProductoACXPersistence.class);
}