/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import mx.com.allianz.service.catalogos.model.GrupoPago;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the grupo pago service. This utility wraps {@link mx.com.allianz.service.catalogos.service.persistence.impl.GrupoPagoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see GrupoPagoPersistence
 * @see mx.com.allianz.service.catalogos.service.persistence.impl.GrupoPagoPersistenceImpl
 * @generated
 */
@ProviderType
public class GrupoPagoUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(GrupoPago grupoPago) {
		getPersistence().clearCache(grupoPago);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<GrupoPago> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<GrupoPago> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<GrupoPago> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<GrupoPago> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static GrupoPago update(GrupoPago grupoPago) {
		return getPersistence().update(grupoPago);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static GrupoPago update(GrupoPago grupoPago,
		ServiceContext serviceContext) {
		return getPersistence().update(grupoPago, serviceContext);
	}

	/**
	* Returns all the grupo pagos where descripcionGrupo = &#63;.
	*
	* @param descripcionGrupo the descripcion grupo
	* @return the matching grupo pagos
	*/
	public static List<GrupoPago> findByfindByDescripcionGrupo(
		java.lang.String descripcionGrupo) {
		return getPersistence().findByfindByDescripcionGrupo(descripcionGrupo);
	}

	/**
	* Returns a range of all the grupo pagos where descripcionGrupo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GrupoPagoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcionGrupo the descripcion grupo
	* @param start the lower bound of the range of grupo pagos
	* @param end the upper bound of the range of grupo pagos (not inclusive)
	* @return the range of matching grupo pagos
	*/
	public static List<GrupoPago> findByfindByDescripcionGrupo(
		java.lang.String descripcionGrupo, int start, int end) {
		return getPersistence()
				   .findByfindByDescripcionGrupo(descripcionGrupo, start, end);
	}

	/**
	* Returns an ordered range of all the grupo pagos where descripcionGrupo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GrupoPagoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcionGrupo the descripcion grupo
	* @param start the lower bound of the range of grupo pagos
	* @param end the upper bound of the range of grupo pagos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching grupo pagos
	*/
	public static List<GrupoPago> findByfindByDescripcionGrupo(
		java.lang.String descripcionGrupo, int start, int end,
		OrderByComparator<GrupoPago> orderByComparator) {
		return getPersistence()
				   .findByfindByDescripcionGrupo(descripcionGrupo, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the grupo pagos where descripcionGrupo = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GrupoPagoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param descripcionGrupo the descripcion grupo
	* @param start the lower bound of the range of grupo pagos
	* @param end the upper bound of the range of grupo pagos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching grupo pagos
	*/
	public static List<GrupoPago> findByfindByDescripcionGrupo(
		java.lang.String descripcionGrupo, int start, int end,
		OrderByComparator<GrupoPago> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByfindByDescripcionGrupo(descripcionGrupo, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first grupo pago in the ordered set where descripcionGrupo = &#63;.
	*
	* @param descripcionGrupo the descripcion grupo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching grupo pago
	* @throws NoSuchGrupoPagoException if a matching grupo pago could not be found
	*/
	public static GrupoPago findByfindByDescripcionGrupo_First(
		java.lang.String descripcionGrupo,
		OrderByComparator<GrupoPago> orderByComparator)
		throws mx.com.allianz.service.catalogos.exception.NoSuchGrupoPagoException {
		return getPersistence()
				   .findByfindByDescripcionGrupo_First(descripcionGrupo,
			orderByComparator);
	}

	/**
	* Returns the first grupo pago in the ordered set where descripcionGrupo = &#63;.
	*
	* @param descripcionGrupo the descripcion grupo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching grupo pago, or <code>null</code> if a matching grupo pago could not be found
	*/
	public static GrupoPago fetchByfindByDescripcionGrupo_First(
		java.lang.String descripcionGrupo,
		OrderByComparator<GrupoPago> orderByComparator) {
		return getPersistence()
				   .fetchByfindByDescripcionGrupo_First(descripcionGrupo,
			orderByComparator);
	}

	/**
	* Returns the last grupo pago in the ordered set where descripcionGrupo = &#63;.
	*
	* @param descripcionGrupo the descripcion grupo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching grupo pago
	* @throws NoSuchGrupoPagoException if a matching grupo pago could not be found
	*/
	public static GrupoPago findByfindByDescripcionGrupo_Last(
		java.lang.String descripcionGrupo,
		OrderByComparator<GrupoPago> orderByComparator)
		throws mx.com.allianz.service.catalogos.exception.NoSuchGrupoPagoException {
		return getPersistence()
				   .findByfindByDescripcionGrupo_Last(descripcionGrupo,
			orderByComparator);
	}

	/**
	* Returns the last grupo pago in the ordered set where descripcionGrupo = &#63;.
	*
	* @param descripcionGrupo the descripcion grupo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching grupo pago, or <code>null</code> if a matching grupo pago could not be found
	*/
	public static GrupoPago fetchByfindByDescripcionGrupo_Last(
		java.lang.String descripcionGrupo,
		OrderByComparator<GrupoPago> orderByComparator) {
		return getPersistence()
				   .fetchByfindByDescripcionGrupo_Last(descripcionGrupo,
			orderByComparator);
	}

	/**
	* Returns the grupo pagos before and after the current grupo pago in the ordered set where descripcionGrupo = &#63;.
	*
	* @param codigoGrupoPagos the primary key of the current grupo pago
	* @param descripcionGrupo the descripcion grupo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next grupo pago
	* @throws NoSuchGrupoPagoException if a grupo pago with the primary key could not be found
	*/
	public static GrupoPago[] findByfindByDescripcionGrupo_PrevAndNext(
		int codigoGrupoPagos, java.lang.String descripcionGrupo,
		OrderByComparator<GrupoPago> orderByComparator)
		throws mx.com.allianz.service.catalogos.exception.NoSuchGrupoPagoException {
		return getPersistence()
				   .findByfindByDescripcionGrupo_PrevAndNext(codigoGrupoPagos,
			descripcionGrupo, orderByComparator);
	}

	/**
	* Removes all the grupo pagos where descripcionGrupo = &#63; from the database.
	*
	* @param descripcionGrupo the descripcion grupo
	*/
	public static void removeByfindByDescripcionGrupo(
		java.lang.String descripcionGrupo) {
		getPersistence().removeByfindByDescripcionGrupo(descripcionGrupo);
	}

	/**
	* Returns the number of grupo pagos where descripcionGrupo = &#63;.
	*
	* @param descripcionGrupo the descripcion grupo
	* @return the number of matching grupo pagos
	*/
	public static int countByfindByDescripcionGrupo(
		java.lang.String descripcionGrupo) {
		return getPersistence().countByfindByDescripcionGrupo(descripcionGrupo);
	}

	/**
	* Caches the grupo pago in the entity cache if it is enabled.
	*
	* @param grupoPago the grupo pago
	*/
	public static void cacheResult(GrupoPago grupoPago) {
		getPersistence().cacheResult(grupoPago);
	}

	/**
	* Caches the grupo pagos in the entity cache if it is enabled.
	*
	* @param grupoPagos the grupo pagos
	*/
	public static void cacheResult(List<GrupoPago> grupoPagos) {
		getPersistence().cacheResult(grupoPagos);
	}

	/**
	* Creates a new grupo pago with the primary key. Does not add the grupo pago to the database.
	*
	* @param codigoGrupoPagos the primary key for the new grupo pago
	* @return the new grupo pago
	*/
	public static GrupoPago create(int codigoGrupoPagos) {
		return getPersistence().create(codigoGrupoPagos);
	}

	/**
	* Removes the grupo pago with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param codigoGrupoPagos the primary key of the grupo pago
	* @return the grupo pago that was removed
	* @throws NoSuchGrupoPagoException if a grupo pago with the primary key could not be found
	*/
	public static GrupoPago remove(int codigoGrupoPagos)
		throws mx.com.allianz.service.catalogos.exception.NoSuchGrupoPagoException {
		return getPersistence().remove(codigoGrupoPagos);
	}

	public static GrupoPago updateImpl(GrupoPago grupoPago) {
		return getPersistence().updateImpl(grupoPago);
	}

	/**
	* Returns the grupo pago with the primary key or throws a {@link NoSuchGrupoPagoException} if it could not be found.
	*
	* @param codigoGrupoPagos the primary key of the grupo pago
	* @return the grupo pago
	* @throws NoSuchGrupoPagoException if a grupo pago with the primary key could not be found
	*/
	public static GrupoPago findByPrimaryKey(int codigoGrupoPagos)
		throws mx.com.allianz.service.catalogos.exception.NoSuchGrupoPagoException {
		return getPersistence().findByPrimaryKey(codigoGrupoPagos);
	}

	/**
	* Returns the grupo pago with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param codigoGrupoPagos the primary key of the grupo pago
	* @return the grupo pago, or <code>null</code> if a grupo pago with the primary key could not be found
	*/
	public static GrupoPago fetchByPrimaryKey(int codigoGrupoPagos) {
		return getPersistence().fetchByPrimaryKey(codigoGrupoPagos);
	}

	public static java.util.Map<java.io.Serializable, GrupoPago> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the grupo pagos.
	*
	* @return the grupo pagos
	*/
	public static List<GrupoPago> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the grupo pagos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GrupoPagoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of grupo pagos
	* @param end the upper bound of the range of grupo pagos (not inclusive)
	* @return the range of grupo pagos
	*/
	public static List<GrupoPago> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the grupo pagos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GrupoPagoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of grupo pagos
	* @param end the upper bound of the range of grupo pagos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of grupo pagos
	*/
	public static List<GrupoPago> findAll(int start, int end,
		OrderByComparator<GrupoPago> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the grupo pagos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GrupoPagoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of grupo pagos
	* @param end the upper bound of the range of grupo pagos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of grupo pagos
	*/
	public static List<GrupoPago> findAll(int start, int end,
		OrderByComparator<GrupoPago> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the grupo pagos from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of grupo pagos.
	*
	* @return the number of grupo pagos
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static GrupoPagoPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<GrupoPagoPersistence, GrupoPagoPersistence> _serviceTracker =
		ServiceTrackerFactory.open(GrupoPagoPersistence.class);
}