/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the Clasificador service. Represents a row in the &quot;PTL_CAT_CLASIFICADOR&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see ClasificadorModel
 * @see mx.com.allianz.service.catalogos.model.impl.ClasificadorImpl
 * @see mx.com.allianz.service.catalogos.model.impl.ClasificadorModelImpl
 * @generated
 */
@ImplementationClassName("mx.com.allianz.service.catalogos.model.impl.ClasificadorImpl")
@ProviderType
public interface Clasificador extends ClasificadorModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link mx.com.allianz.service.catalogos.model.impl.ClasificadorImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<Clasificador, Integer> ID_CLASIFICADOR_ACCESSOR =
		new Accessor<Clasificador, Integer>() {
			@Override
			public Integer get(Clasificador clasificador) {
				return clasificador.getIdClasificador();
			}

			@Override
			public Class<Integer> getAttributeClass() {
				return Integer.class;
			}

			@Override
			public Class<Clasificador> getTypeClass() {
				return Clasificador.class;
			}
		};
}