/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mx.com.allianz.service.catalogos.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link HospitalLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see HospitalLocalService
 * @generated
 */
@ProviderType
public class HospitalLocalServiceWrapper implements HospitalLocalService,
	ServiceWrapper<HospitalLocalService> {
	public HospitalLocalServiceWrapper(
		HospitalLocalService hospitalLocalService) {
		_hospitalLocalService = hospitalLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _hospitalLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _hospitalLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _hospitalLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _hospitalLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _hospitalLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of hospitals.
	*
	* @return the number of hospitals
	*/
	@Override
	public int getHospitalsCount() {
		return _hospitalLocalService.getHospitalsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _hospitalLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _hospitalLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.catalogos.model.impl.HospitalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _hospitalLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.catalogos.model.impl.HospitalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _hospitalLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the hospitals.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link mx.com.allianz.service.catalogos.model.impl.HospitalModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of hospitals
	* @param end the upper bound of the range of hospitals (not inclusive)
	* @return the range of hospitals
	*/
	@Override
	public java.util.List<mx.com.allianz.service.catalogos.model.Hospital> getHospitals(
		int start, int end) {
		return _hospitalLocalService.getHospitals(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _hospitalLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _hospitalLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Adds the hospital to the database. Also notifies the appropriate model listeners.
	*
	* @param hospital the hospital
	* @return the hospital that was added
	*/
	@Override
	public mx.com.allianz.service.catalogos.model.Hospital addHospital(
		mx.com.allianz.service.catalogos.model.Hospital hospital) {
		return _hospitalLocalService.addHospital(hospital);
	}

	/**
	* Creates a new hospital with the primary key. Does not add the hospital to the database.
	*
	* @param idHospital the primary key for the new hospital
	* @return the new hospital
	*/
	@Override
	public mx.com.allianz.service.catalogos.model.Hospital createHospital(
		int idHospital) {
		return _hospitalLocalService.createHospital(idHospital);
	}

	/**
	* Deletes the hospital with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param idHospital the primary key of the hospital
	* @return the hospital that was removed
	* @throws PortalException if a hospital with the primary key could not be found
	*/
	@Override
	public mx.com.allianz.service.catalogos.model.Hospital deleteHospital(
		int idHospital)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _hospitalLocalService.deleteHospital(idHospital);
	}

	/**
	* Deletes the hospital from the database. Also notifies the appropriate model listeners.
	*
	* @param hospital the hospital
	* @return the hospital that was removed
	*/
	@Override
	public mx.com.allianz.service.catalogos.model.Hospital deleteHospital(
		mx.com.allianz.service.catalogos.model.Hospital hospital) {
		return _hospitalLocalService.deleteHospital(hospital);
	}

	@Override
	public mx.com.allianz.service.catalogos.model.Hospital fetchHospital(
		int idHospital) {
		return _hospitalLocalService.fetchHospital(idHospital);
	}

	/**
	* Returns the hospital with the primary key.
	*
	* @param idHospital the primary key of the hospital
	* @return the hospital
	* @throws PortalException if a hospital with the primary key could not be found
	*/
	@Override
	public mx.com.allianz.service.catalogos.model.Hospital getHospital(
		int idHospital)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _hospitalLocalService.getHospital(idHospital);
	}

	/**
	* Updates the hospital in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param hospital the hospital
	* @return the hospital that was updated
	*/
	@Override
	public mx.com.allianz.service.catalogos.model.Hospital updateHospital(
		mx.com.allianz.service.catalogos.model.Hospital hospital) {
		return _hospitalLocalService.updateHospital(hospital);
	}

	@Override
	public HospitalLocalService getWrappedService() {
		return _hospitalLocalService;
	}

	@Override
	public void setWrappedService(HospitalLocalService hospitalLocalService) {
		_hospitalLocalService = hospitalLocalService;
	}

	private HospitalLocalService _hospitalLocalService;
}