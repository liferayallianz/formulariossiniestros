package mx.com.allianz.tipo.queja.portlet;

import mx.com.allianz.tipo.queja.constants.DatosTipoQuejaPortletKeys;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

/**
 * @author sfrancof
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=Quejas",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=datos-tipo-queja Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + DatosTipoQuejaPortletKeys.DatosTipoQueja,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class DatosTipoQuejaPortlet extends MVCPortlet {
}