package mx.com.allianz.tipo.queja.constants;

/**
 * @author sfrancof
 */
public class DatosTipoQuejaPortletKeys {

	public static final String DatosTipoQueja = "DatosTipoQueja";

}