<%@ include file="/init.jsp" %>

<div id="<portlet:namespace/>div_datos_tipo_queja" style="display:none;">
	<aui:form name="fm-tipo-queja" >
		<div class="formularioRadioCentrado col-md-12 text-center" >
			<aui:field-wrapper name="">
				<aui:input id="radioQueja" checked="<%=true%>" inlineLabel="right" name="tipoContacto" type="radio" value="queja" label="tipocontacto_Tipocontactomvcportlet.queja" onClick="seleccionQueja();"/> &nbsp;
				<aui:input id="radioSugerencia"  inlineLabel="right" name="tipoContacto" type="radio" value="sugerencia" label="tipocontacto_Tipocontactomvcportlet.sugerencia" onClick="seleccionSugerencia();" />
			</aui:field-wrapper>
		</div> <br/>
		<div id="<portlet:namespace />divTipoSolicitante" style="display:none;">
			<p class="formularioTituloEncabezado">Seleccione tipo de solicitante</p>
			
			<div class="formularioRadioCentrado col-md-12 text-center" >
				<aui:field-wrapper name="">
					<aui:input id="solicitante_cliente" checked="<%=true%>" inlineLabel="right" name="solicitante" type="radio" value="Cliente" label="tiposolicitante_Tiposolicitantemvcportlet.cliente" onClick="actualizarTipoSolicitante(this);" /> &nbsp;
					<aui:input id="solicitante_asegurado" inlineLabel="right" name="solicitante" type="radio" value="Asegurado" label="tiposolicitante_Tiposolicitantemvcportlet.asegurado" onClick="actualizarTipoSolicitante(this);" /> &nbsp;
					<aui:input id="solicitante_beneficiario" inlineLabel="right" name="solicitante" type="radio" value="Beneficiario" label="tiposolicitante_Tiposolicitantemvcportlet.beneficiario" onClick="actualizarTipoSolicitante(this);" /> &nbsp;
					<aui:input id="solicitante_agente" inlineLabel="right" name="solicitante" type="radio" value="Agente" label="tiposolicitante_Tiposolicitantemvcportlet.agente" onClick="actualizarTipoSolicitante(this);" /> &nbsp;
					<aui:input id="solicitante_proveedor" inlineLabel="right" name="solicitante" type="radio" value="Proveedor" label="tiposolicitante_Tiposolicitantemvcportlet.proveedor" onClick="actualizarTipoSolicitante(this);" /> &nbsp;
					<aui:input id="solicitante_prospecto" inlineLabel="right" name="solicitante" type="radio" value="Prospecto" label="tiposolicitante_Tiposolicitantemvcportlet.prospecto" onClick="actualizarTipoSolicitante(this);" /> &nbsp;
				</aui:field-wrapper>
				<aui:input name="hidden_solicitante_cliente" type="hidden" value="Cliente" />
				<aui:input name="hidden_solicitante_tipoContacto" type="hidden" value="Queja" />
			</div> 
			<br/>
			<br/>
			<div id="<portlet:namespace />divClaveAgente" style="display:none;">
			    <aui:input cssClass="formularioCampoTexto" id="idClaveAgente" name="claveAgente" maxlength="9" value="" label="" placeHolder="Clave de Agente*">
			    	<aui:validator name="maxLength" errorMessage="La longitud de la clave de agente debe ser m&aacute;ximo a 8">8</aui:validator>
			    	<aui:validator name="digits" errorMessage="Por favor ingrese s&oacute;lo n&uacute;meros"/>
			    	<aui:validator name="required" errorMessage="El n&uacute;mero de p&oacute;liza es requerido cuando se selecciona un producto">
			              function(val) {
			                     return A.one('#<portlet:namespace />hidden_solicitante_cliente').get('value') == "Agente";
			              }
			   		</aui:validator>
			    	
			    </aui:input>
			</div>
		</div> 
		<div id="<portlet:namespace />div_queja_datos_poliza" style="display:none;">
			<aui:select cssClass="formularioCampoTexto" id="queja_emisor_poliza" name="emisorPoliza" label="" required="false" errorMessage="Debe seleccionar una poliza">
			    <aui:option selected="true" value="">
			        <liferay-ui:message key="numero.poliza.select.vacio" />
			    </aui:option>
		    </aui:select>
			<aui:input cssClass="formularioCampoTexto" id="queja_numero_poliza" name="numeroPoliza" type="text" value="" label="" placeholder="numero.poliza.numero.poliza" >
				<aui:validator name="rangeLength" errorMessage="La longitud del n&uacute;mero de p&oacute;liza debe ser mayor a 3 y menor a 10 caracteres">[3,10]</aui:validator>
				<aui:validator name="required" errorMessage="El n&uacute;mero de p&oacute;liza es requerido cuando se selecciona un producto">
		              function(val) {
		                     return A.one('#<portlet:namespace />queja_emisor_poliza').get('selectedIndex') > 0;
		              }
		   		</aui:validator>
		   		<aui:validator name="custom" errorMessage="Por favor ingrese s&oacute;lo n&uacute;meros o guiones">
			        function (val, fieldNode, ruleValue) {
			                var returnValue = true;
			                var iChars = "-0123456789";
			               	var value = fieldNode.get('value');
			                for (var i = 0; i < value.length; i++) {
			                    if (returnValue && iChars.indexOf(value.charAt(i)) == -1) {                  
			                    		returnValue = false;
			                    }
			                }
			                return returnValue;
			        }
			    </aui:validator>
			</aui:input>
		</div>
	
	</aui:form>
</div>

<script type="text/javascript"> 
console.log("datos.tipo.queja v1.0.0");
</script>

<aui:script use="aui-base,liferay-form,aui-form-validator">

/////////////////////////////////////////////////////////
///////////////////Declaracion de funciones//////////////
/////////////////////////////////////////////////////////

Liferay.provide(window, 'mostrarSeccionTipoQueja', function(seccionPoliza,mostrado) {
	A.one('#<portlet:namespace/>' + seccionPoliza)._node.style.display = mostrado;
});

Liferay.provide(window, 'actualizarTipoSolicitante', function(param) {
	var solicitante = param.value;
	A.one('#<portlet:namespace />hidden_solicitante_cliente').set('value',solicitante);
	
	var tipoContacto = A.one('#<portlet:namespace />hidden_solicitante_tipoContacto').get('value');
	console.log("actualizarTipoSolicitante, tipoContacto = " + tipoContacto);
	mostrarSeccionTipoQueja('divClaveAgente',solicitante == 'Agente' ? 'block' : 'none');
	
	if(solicitante == 'Prospecto') {
		mostrarSeccionTipoQueja('div_queja_datos_poliza','none');
	} else if(tipoContacto == 'Queja'){
		mostrarSeccionTipoQueja('div_queja_datos_poliza','block');
	}
	
	var idTipoSolicitante = getIdTipoSolicitante(solicitante);
	console.log('idTipoSolicitante = ' + idTipoSolicitante);
	// Revisar que funcionalidad hay aqui
	//Liferay.fire('actualizarNumerosPoliza', {tipoSolicitante:idTipoSolicitante });
});

function getIdTipoSolicitante(solicitante){
	var idTipoSolicitante = 1;
	switch(solicitante) {
	case 'Cliente':
		idTipoSolicitante = 1;
		break;
	case 'Asegurado':
		idTipoSolicitante = 2;
		break;
	case 'Beneficiario':
		idTipoSolicitante = 3;
		break;
	case 'Agente':
		idTipoSolicitante = 4;
		break;
	case 'Proveedor':
		idTipoSolicitante = 5;
		break;
	case 'Prospecto':
		idTipoSolicitante = 6;
		break;
	}
	return idTipoSolicitante;
}

/////////////////////////////////////////////////////////
///////////////////Declaracion eventos///////////////////
/////////////////////////////////////////////////////////

Liferay.on('validaDatosTipoQueja', function(event){
	var formValidator = Liferay.Form.get('<portlet:namespace />fm-tipo-queja').formValidator;
	formValidator.validate();
	var eventoRespuesta = {};
	eventoRespuesta.valido = !formValidator.hasErrors();
	if(eventoRespuesta.valido){
		eventoRespuesta.producto = A.one('#<portlet:namespace />queja_emisor_poliza').get('value');
		eventoRespuesta.numeroPoliza = A.one('#<portlet:namespace />queja_numero_poliza').get('value');
		eventoRespuesta.claveAgente = A.one('#<portlet:namespace />idClaveAgente').get('value');
		eventoRespuesta.solicitante = A.one('#<portlet:namespace />hidden_solicitante_cliente').get('value');
		eventoRespuesta.tipoContacto = A.one('#<portlet:namespace />radioQueja').get('checked') ? 'Queja' : 'Sugerencia';
	}
	eventoRespuesta.nombreSeccion = "DatosTipoQueja";
	console.log("eventoRespuesta de datosTipoQueja");
	console.log(eventoRespuesta);
	Liferay.fire('validaDatosTipoQuejaRespuesta', eventoRespuesta );
});

Liferay.on("mostrarDatosTipoQueja",function(event){
	mostrarSeccionTipoQueja('divTipoSolicitante', event.cliente ? 'none' : 'block' );
	mostrarSeccionTipoQueja('div_datos_tipo_queja','block');
});

Liferay.on("ocultarDatosTipoQueja",function(event){
	mostrarSeccionTipoQueja('div_datos_tipo_queja','none');
});

</aui:script>