<%@ include file="/init.jsp" %>
<div id="<portlet:namespace/>div_seccion_datos_fiscales" style="display:none;">
	<aui:form action="#" name="fm_datos_fiscales">
			<aui:input name="version-datos.fiscales" type="hidden" value="1.0.1"  />
			<aui:input cssClass="formularioCampoTexto" id="datos_fiscales_razon_social" name="razonSocial" type="text" value="" label="" placeholder="datos.fiscales_DatosFiscales.razonsocial" >
				<aui:validator name="rangeLength" errorMessage="La longitud de la raz&oacute;n social debe ser mayor a 2 y menor a 100 caracteres">[2,100]</aui:validator>
			</aui:input>
			<aui:input cssClass="formularioCampoTexto" name="datos_fiscales_rfc" type="text" value="" label="" placeholder="datos.fiscales_DatosFiscales.rfc" >
			<aui:validator name="rangeLength" errorMessage="La longitud del rfc debe ser mayor a 10 y menor a 13 caracteres">[10,13]</aui:validator>
			<aui:validator name="custom"  errorMessage="Por favor ingrese s&oacute;lo letras o n&uacute;meros">
			        function (val, fieldNode, ruleValue) {
			                var returnValue = true;
			                var iChars = "~`!@#$%^&*()_+=-[]\\\';,./{}|\":<>? ";
			                for (var i = 0; i < val.length; i++) {
			                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
			                     returnValue = false;
			                    }
			                }
			                return returnValue;
			        }
			    </aui:validator>		
			</aui:input>
			<aui:select cssClass="formularioCampoTexto" id="datos_fiscales_estado_contacto" name="estado" label="">
		    <aui:option selected="true" value="">
		        <liferay-ui:message key="estado.contacto.estado.contacto" />
		    </aui:option>
		</aui:select>
	</aui:form>
</div>

<script type="text/javascript">
console.log("datos.fiscales v1.0.0");
</script>

<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">

/////////////////////////////////////////////////////////
///////////////////Declaracion de funciones//////////////
/////////////////////////////////////////////////////////

Liferay.provide(window, 'mostrarSeccionDatosFiscales', function(seccionPoliza,mostrado) {
	A.one('#<portlet:namespace/>' + seccionPoliza)._node.style.display = mostrado;
});

Liferay.provide(window, 'cargarRegistrosComboFiscales', function(urlString, onSuccess) {
    A.io.request (
    	urlString, {
        	method: 'get',
         	dataType: 'json',
         	headers: {
                'Accept': 'application/json'
			},
	        on: {
                failure: function() {
                    console.log("Ajax failed! There was some error at the server");
                },
                success: onSuccess
	        }
	}); 
});

/////////////////////////////////////////////////////////
///////////////////Declaracion eventos///////////////////
/////////////////////////////////////////////////////////

Liferay.on("mostrarDatosFiscales",function(event){
	mostrarSeccionDatosFiscales('div_seccion_datos_fiscales','block');
});

Liferay.on("ocultarDatosFiscales",function(event){
	mostrarSeccionDatosFiscales('div_seccion_datos_fiscales','none');
});

Liferay.on('validaDatosFiscales', function(event){
	var formValidator = Liferay.Form.get('<portlet:namespace />fm_datos_fiscales').formValidator;
	formValidator.validate();
	var eventoRespuesta = {};
	var estadoContactoSelect = A.one('#<portlet:namespace />datos_fiscales_estado_contacto');
	eventoRespuesta.valido = !formValidator.hasErrors();
	console.log('validaDatosFiscales eventoRespuesta.valido = ' + eventoRespuesta.valido);
	if (eventoRespuesta.valido) {
		eventoRespuesta.razonSocial = A.one('#<portlet:namespace />datos_fiscales_razon_social').get('value'),
		eventoRespuesta.rfc = A.one('#<portlet:namespace />datos_fiscales_rfc').get('value')
		eventoRespuesta.estado = estadoContactoSelect.get('value');
		eventoRespuesta.estadoDescripcion = (eventoRespuesta.estado) ? estadoContactoSelect.get('options')._nodes[estadoContactoSelect.get('selectedIndex')].text : '';
	}	
	Liferay.fire('validaDatosFiscalesRespuesta', eventoRespuesta );
});

/////////////////////////////////////////////////////////
///////////////////Cargas Iniciales//////////////////////
/////////////////////////////////////////////////////////

console.log("Cargando estados para contacto" );
cargarRegistrosComboFiscales("<%=request.getAttribute("estadosSourceURL")%>",
	function(event, id, obj) {
		console.log(this.get('responseData'));
		var states = this.get('responseData').rows.estado;
		var estadoAjax = A.one('#<portlet:namespace />datos_fiscales_estado_contacto');
		estadoAjax.html("");
		estadoAjax.append("<option value=''>Seleccione de que estado nos contacta *</option>");
		for (var j=0; j < states.length; j++) {
			estadoAjax.append( '<option value="' + states[j].codestado + '">' + states[j].descestado + '</option>');
		}
		console.log("Carga de estados completa");
	});

</aui:script>