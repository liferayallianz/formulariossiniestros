package mx.com.allianz.datos.fiscales.portlet;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import aQute.bnd.annotation.metatype.Configurable;
import mx.com.allianz.commons.catalogos.dto.SucursalDTO;
import mx.com.allianz.datos.fiscales.config.DatosFiscalesConfiguration;
import mx.com.allianz.datos.fiscales.constants.DatosFiscalesPortletKeys;

/**
 * @author sfrancof
 */
@Component(
	configurationPid = "mx.com.allianz.datos.fiscales.config.DatosFiscalesConfiguration",
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=Quejas",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=datos-fiscales Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + DatosFiscalesPortletKeys.DatosFiscales,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,guest"
	},
	service = Portlet.class
)
public class DatosFiscalesPortlet extends MVCPortlet {
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		try {
			_log.info("Dentro del render de solicitud tramite juntos");
			renderRequest.setAttribute("estadosSourceURL", _configuration.estadosSourceURL());
		} catch (Exception e ) { if (_log.isDebugEnabled()) _log.info(e); else e.printStackTrace(); }

		super.render(renderRequest, renderResponse);
	}
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		_configuration = Configurable.createConfigurable(
				DatosFiscalesConfiguration.class, properties);
		if (_log.isInfoEnabled()) {
			_log.info(new StringBuilder("Configured Datos Fiscales (datos.fiscales -  v1.0.0) : \n{ ").append(
					"estadosSourceURL : " + _configuration.estadosSourceURL() ).append(
					" }").toString());
		}
	}

	private DatosFiscalesConfiguration _configuration ;
	private static Log _log = LogFactoryUtil.getLog(DatosFiscalesPortlet.class);

}