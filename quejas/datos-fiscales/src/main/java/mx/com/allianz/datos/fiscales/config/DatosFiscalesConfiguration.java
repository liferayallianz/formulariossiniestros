package mx.com.allianz.datos.fiscales.config;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(
		category = "Productivity",
		scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE
	)
	@Meta.OCD(
		localization = "content/Language",
		name = "Configuracion Datos Fiscales",
		id = "mx.com.allianz.datos.fiscales.config.DatosFiscalesConfiguration"
	)
public interface DatosFiscalesConfiguration {
	
	@Meta.AD(
			deflt = "https://portalb.allianz.com.mx/az-wso2-services/catalogos_allianz/op_sel_json_cat_estado?codpais=412", 
			description = "URL de origen de datos JSON de los estados",
			required = false
		)
	public String estadosSourceURL();
	
	

}
