package mx.com.allianz.datos.fiscales.constants;

/**
 * @author sfrancof
 */
public class DatosFiscalesPortletKeys {

	public static final String DatosFiscales = "DatosFiscales";

}