package mx.com.allianz.quejas.sender.config;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "Productivity")
@Meta.OCD(
		id = "mx.com.allianz.queja.portlet.configuration.AtencionQuejasSenderPortletConfiguration",
		localization = "content/Language",
		name = "Configuracion Atencion Quejas Gnrl"
	)
public interface AtencionQuejasSenderPortletConfiguration {
	
	@Meta.AD(
			deflt = "false", 
			description = "Activa configuracion de Clientes a modulo de Tramites",
			required = false
		)
	public Boolean enableClientConfiguration();
	
	@Meta.AD(
			deflt = "azctToken", 
			description = "Token con llave de cliente",
			required = false
		)
	public String allianzClientToken();

	@Meta.AD(
			deflt = "5000", 
			description = "Segundos de validacion para repeticion de peticiones",
			required = false
		)
	public Long waitRequest();
	
	@Meta.AD(
			deflt = "false", 
			description = "Activa configuracion de Capcha",
			required = false
		)
	public Boolean enableCapcha();

}
