package mx.com.allianz.quejas.sender.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.Cookie;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.google.gson.Gson;
import com.liferay.portal.kernel.captcha.CaptchaTextException;
import com.liferay.portal.kernel.captcha.CaptchaUtil;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;

import aQute.bnd.annotation.metatype.Configurable;
import it.dontesta.labs.liferay.lrbo16.webservice.crm.api.CRMService;
import mx.com.allianz.commons.dto.cliente.ClienteDTO;
import mx.com.allianz.commons.dto.cliente.ClienteTramiteDTO;
import mx.com.allianz.commons.dto.cliente.ContactoDTO;
import mx.com.allianz.commons.dto.cliente.CorreoDTO;
import mx.com.allianz.commons.dto.cliente.ProductoClienteDTO;
import mx.com.allianz.commons.dto.cliente.TelefonoDTO;
import mx.com.allianz.commons.dto.queja.QuejaFlujoDTO;
import mx.com.allianz.commons.dto.tramite.TramiteDto;
import mx.com.allianz.module.service.facade.AllianzService;
import mx.com.allianz.quejas.sender.config.AtencionQuejasSenderPortletConfiguration;
import mx.com.allianz.quejas.sender.constants.AtencionQuejasSenderPortletKeys;

/**
 * @author sfrancof
 */
@Component(
	configurationPid = "mx.com.allianz.queja.portlet.configuration.AtencionQuejasSenderPortletConfiguration",
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=Quejas",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=atencion-quejas-sender Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + AtencionQuejasSenderPortletKeys.AtencionQuejasSender,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,guest"
	},
	service = Portlet.class
)
public class AtencionQuejasSenderPortlet extends MVCPortlet {
	
	
	public static final String LLAVE_CURRENT_PAGE_URL = "currentPageURL"; 
	public static final String LLAVE_QUEJA = "queja"; 
	public static final String LLAVE_QUEJA_REC = "quejaRec"; 
	public static final String LLAVE_CLIENTE_FIRMADO = "clienteFirmado";
	public static final String LLAVE_ATENCION_QUEJA_EVENT = "atencionQuejaEvent";
	public static final String LLAVE_QUEJA_CLIENTE_HABILITADO = "quejaClienteHabilitado"; 
	public static final String LLAVE_INCONSISTENCIA_EVENT = "inconsistenciaQuejaEvent";
	public static final String LLAVE_INCONSISTENCIA_ERROR = "inconsistenciaQuejaError";
	public static final String PRODUCTO_ACTIVO = "ACTIVA"; 
	public static final String CONSULTA_EXITOSA_CLIENTE = "\"estatus\":\"true\"";
	public static final String LADA_CELULAR = "044"; 
	public static final String TIPO_TELEFONO_PARTICULAR = "P"; 
	public static final String TIPO_TELEFONO_CELULAR = "C"; 
	
	public static final String CMD_UPLOAD = "upload";
	public static final String CMD_DOCUMENTOS = "docs";
	public static final String CMD_CAPCHA = "capcha";
	
	
	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws IOException, PortletException {

		PortletSession session = request.getPortletSession();			
		Gson gson = new Gson();

		_log.info("AtencionQuejasSenderPortlet - render - isClienteEnabled = " + isClientEnabled());
		QuejaFlujoDTO queja = new QuejaFlujoDTO();
		if (isClientEnabled()) {
			if (allianzService != null) {
				
				Arrays.stream(Optional.ofNullable(request.getCookies())
									  			.orElseGet(() -> new Cookie[0]))
								.filter(cookie -> allianzClientToken().equals(cookie.getName()))
								.map(Cookie::getValue)
								.findAny()
								.ifPresent(cookie -> {
									Optional.ofNullable(allianzService.obtenerDatosTestingZone(cookie))
											.filter(c -> c.contains(CONSULTA_EXITOSA_CLIENTE))
											.map(clienteV -> {_log.info("cliente = " + clienteV); return clienteV;})
											.map(json -> gson.fromJson(json, ClienteTramiteDTO.class))
											.ifPresent(cliente -> {
												_log.info("cliente parseado = " + cliente);
												session.setAttribute(LLAVE_CLIENTE_FIRMADO, 
														gson.toJson(cliente.getCliente()), PortletSession.APPLICATION_SCOPE);
												_log.info("AtencionQuejasSenderPortlet -> render -> cliente = " + queja);

												request.setAttribute(LLAVE_CLIENTE_FIRMADO, 
														gson.toJson(cliente.getCliente()));
												fillClient(cliente, queja);
											});
								});
				
				_log.info("AtencionQuejasSenderPortlet -> render -> isClientEnabled = " + isClientEnabled());
				session.setAttribute(LLAVE_QUEJA_CLIENTE_HABILITADO, isClientEnabled(), PortletSession.APPLICATION_SCOPE);
				request.setAttribute(LLAVE_QUEJA_CLIENTE_HABILITADO, isClientEnabled());

			} else {
				System.err.println("Reiniciar modulo mx.com.allianz.module.service.facade ");
				System.err.println("$ telnet localhost 11311 ");
				System.err.println("g! lb | grep mx.com.allianz.module.service.facade ");
				System.err.println("g! stop PID ");
				System.err.println("g! start PID ");
				System.err.println("g! disconnect ");
				System.err.println("YES ");
			}
		}
		
		_log.info("AtencionQuejasSenderPortlet -> render -> queja  = " + queja);
		session.setAttribute(LLAVE_QUEJA, gson.toJson(queja), PortletSession.APPLICATION_SCOPE);
		request.setAttribute(LLAVE_QUEJA, gson.toJson(queja));
		
		request.setAttribute(LLAVE_CURRENT_PAGE_URL, "quejas-junto");
		
		_log.info("AtencionQuejasSenderPortlet -> render -> Saliendo ");

		super.render(request, response);
	}
	
	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		_log.info("DatosTramiteSenderPortlet -> serveResource 5");
		
		Gson gson = new Gson();
		PortletSession session = resourceRequest.getPortletSession();
		String tipoTramite;
		JSONArray jsonArray;
		com.liferay.portal.kernel.json.JSONObject jsonMessageObject;
		String cmd = ParamUtil.getString(resourceRequest, Constants.CMD);
		
        PrintWriter writer = resourceResponse.getWriter();
		
		if (!invalidRequest(session)) {
			resourceRequest.getParameterMap().forEach((llave,valor) -> _log.info("llave = " + llave + ", valor = " + Arrays.asList(valor)));
			switch (cmd){
				case CMD_UPLOAD:
					_log.info("Se enviará la peticion del tramite");
					
					//TramiteDto tramite = buildTramite(resourceRequest);
//					_log.info("tramite subido:"+tramite.getTipoTramite());
//					_log.info("Se subieron "+tramite.getDocumentos().size()+" documentos");
					
//					enviarTramite(tramite, resourceRequest);
					// TODO Revisar que faltaría para m ensajes de error
					
					jsonMessageObject = 
							JSONFactoryUtil.createJSONObject();
					jsonMessageObject.put("success", true); 
					jsonMessageObject.put("message","Ejecucion correcta");

	        		resourceResponse.setContentType("application/json");
	        		writer.write(jsonMessageObject.toString());
					break;
				case CMD_CAPCHA:
					jsonMessageObject = JSONFactoryUtil.createJSONObject();
					_log.info("Entro a la seccion del capcha 2");
					String parametro = ParamUtil.getString(resourceRequest, "captchaActionType");
						if(parametro !=null && "testCaptchaURL".equals((String)parametro)){
							//Boolean testCaptcha = testCaptcha(resourceRequest);
							jsonMessageObject.put("success", true);//testCaptcha);
							System.out.println("Sin testCapcha jsonMessageObject = " + jsonMessageObject.toJSONString());
							resourceResponse.setContentType("text/javascript");
							resourceResponse.getWriter().write(jsonMessageObject.toJSONString());
						  } else {
							  _log.info("Va a pasar por el serveImage");
							  try {
								  CaptchaUtil.serveImage(resourceRequest, resourceResponse);
							  } catch (Exception e) {
								  if (e instanceof CaptchaTextException //|| e instanceof CaptchaMaxChallengesException 
										  ){
					                  SessionErrors.add(resourceRequest, e.getClass(), e);
					                  _log.error("no deberia llegar hasta aca pops");
					                  e.printStackTrace();
					            }else{
					                  System.out.println("Captcha verification success::" + e.getMessage());
					            }
							  }
						  }
					break;
				default:
					_log.error("No se puede ejecutar la peticion, no hay cmd");
			}
		} else {
			_log.error("----------Se repitio la peticion------");
		}
	}
	
	public QuejaFlujoDTO fillClient(ClienteTramiteDTO clienteTramite, QuejaFlujoDTO queja) {
		Optional<ClienteDTO> cliente = Optional.ofNullable(clienteTramite)
									   .filter(c -> new Boolean(c.getEstatus()))
									   .map(ClienteTramiteDTO::getCliente);
		Optional<ContactoDTO> contacto = cliente.map(ClienteDTO::getContacto);
		
		cliente.ifPresent(c -> {
			queja.setNombre(c.getNombre());
			queja.setApellidoPaterno(c.getApellidoPaterno());
			queja.setApellidoMaterno(c.getApellidoMaterno());
		});
		
		Arrays.stream(contacto.map(ContactoDTO::getCorreos)
			  .orElseGet(() -> new CorreoDTO[0]))
			  .findAny()
			  .ifPresent(correo -> queja.setEmail(correo.getCorreoElectronico()));
		
		Arrays.stream(contacto.map(ContactoDTO::getTelefonos)
				  .orElseGet(() -> new TelefonoDTO[0]))
				  .forEach(telefono -> {
					  if(TIPO_TELEFONO_CELULAR.equals(telefono.getTipoTelefono()) || 
							  (telefono.getLada() != null && LADA_CELULAR.equals(telefono.getLada()))){
						  
						  queja.setTelefonoCelular(telefono.getTelefono());
					  } else {
						  queja.setTelefonoParticular(telefono.getTelefono());
					  }
				  });
		
		queja.setProductos(Arrays.stream(cliente.map(ClienteDTO::getProductos)
							 					  .orElseGet(() -> new ProductoClienteDTO[0]))
								   .map(cli -> {
									   cli.setNumeroPoliza(Optional.ofNullable(cli.getNumeroPoliza())
									   		   .filter(numeroPoliza -> !"".equals(numeroPoliza))
									   		   .orElse(""+getRandomPolicy()));
									   return cli;
								   })
								   .filter(producto -> 
									   PRODUCTO_ACTIVO.equals(producto.getEstatusProducto())
								   )
								   .collect(Collectors.toList())
		);
		
		return queja;
	}
	
	public static String getRandomPolicy() {
		return "" + new Random().nextInt(((99_999 - 999) + 1) + 999 );
	}
	
	public String allianzClientToken() {
		return _configuration.allianzClientToken();
	}
	
	public Boolean isClientEnabled() {
		return _configuration.enableClientConfiguration();
	}
	
	private boolean invalidRequest(PortletSession session){
		return Optional.ofNullable(session.getAttribute("invalidRequest", PortletSession.APPLICATION_SCOPE))
					   .map(param -> (Long) param)
					   .filter(lastTime -> 
					   		(lastTime + _configuration.waitRequest()) > new Date().getTime())
					   .isPresent();
	}
	

	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL)
	protected void setCRMService(CRMService crmService) {
		this.crmService = crmService;
	}

	protected void unsetCRMService(CRMService crmService) {
		this.crmService = null;
	}
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		_configuration = Configurable.createConfigurable(
				AtencionQuejasSenderPortletConfiguration.class, properties);
		if (_log.isInfoEnabled()) {
			_log.info(new StringBuilder("Configured General Queja Sender (atencion.quejas.sender -  v1.0.0) : \n{ ").append(
					"enable client : " + _configuration.enableClientConfiguration() ).append(
					", allianz client token : " ) .append( _configuration.allianzClientToken() ).append(
					", waitRequest:" ).append(_configuration.waitRequest()).append(
					", enableCapcha:" ).append(_configuration.enableCapcha()).append( " }").toString());
		}
	}
	
	@Reference
	protected void setAllianzService(AllianzService allianzService) {
		this.allianzService = allianzService;
	}

	protected void unsetAllianzService(AllianzService allianzService) {
		this.allianzService = null;
	}

	private AllianzService allianzService;
	private CRMService crmService;
	private AtencionQuejasSenderPortletConfiguration _configuration ;
	private static Log _log = LogFactoryUtil.getLog(AtencionQuejasSenderPortlet.class);
}