<%@ include file="/init.jsp" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="mx.com.allianz.quejas.sender.portlet.AtencionQuejasSenderPortlet" %>
<% 

		String currentPageURL = "/queja-junto";
        Object object = request.getAttribute(AtencionQuejasSenderPortlet.LLAVE_CURRENT_PAGE_URL);
		if (object != null && object instanceof String){
			currentPageURL = (String) object;
		}
		
		String queja = null;
        object = request.getAttribute(AtencionQuejasSenderPortlet.LLAVE_QUEJA);
		if (object != null && object instanceof String){
			queja = (String) object;
		}
		
		
%>

<portlet:resourceURL var="uploadResourceURL" >
</portlet:resourceURL>

<portlet:resourceURL var="captchaURL" id="captchaURL">
    <portlet:param name="captchaActionType" value="loadCaptchaURLImage"/>
    <portlet:param name="cmd" value="capcha"/>
</portlet:resourceURL>

<div id="<portlet:namespace/>div_datos_general_queja_sender" style="display:block;">
	<aui:form name="fm-queja-grl" method="post" enctype="multipart/form-data">
		<aui:input name="version-datos-tramite-solicitud-sender" type="hidden" value="1.0.0"  />
		<aui:input name="paginaActual" type="hidden" value="1"  />
		<aui:input name="factorNavegacion" type="hidden" value="1"  />
		<aui:input name="cmd" type="hidden" value="upload"  />
		
		<div id="<portlet:namespace/>seccion_datos_queja" style="display:none;">
			<p class="formularioTituloEncabezado">Especificaciones</p>
			<p class="formularioEtiquetaTexto"><liferay-ui:message key="datosqueja_Datosquejamvcportlet.tipoQueja" /></p>    
			<aui:select cssClass="formularioCampoTexto"  label="" name="quejaRelacionada" required="true">
					<aui:option value=""><liferay-ui:message key="Selecciona una opci&oacute;n" /></aui:option>
					<aui:option value="1"><liferay-ui:message key="Problemas en la atenci&oacute;n de un siniestro" /></aui:option>
					<aui:option value="2"><liferay-ui:message key="Problemas con tu p&oacute;liza" /></aui:option>
					<aui:option value="3"><liferay-ui:message key="Problemas con el pago de tu p&oacute;liza / cobranza" /></aui:option>
					<aui:option value="4"><liferay-ui:message key="Problemas con tu recibo" /></aui:option>
					<aui:option value="5"><liferay-ui:message key="Otros" /></aui:option>
			</aui:select>
			
			<p class="formularioEtiquetaTexto"><liferay-ui:message key="datosqueja_Datosquejamvcportlet.especificacionQueja" /></p>    
			<aui:select cssClass="formularioCampoTexto" label="" name="quejaRefiere" required="true">
					<aui:option value=""><liferay-ui:message key="Selecciona una opci&oacute;n" /></aui:option>
					<aui:option value="1"><liferay-ui:message key="No estoy de acuerdo en la respuesta en tiempo y forma de mi tr&aacute;mite" /></aui:option>
					<aui:option value="2"><liferay-ui:message key="Problemas en el flujo de atenci&oacute;n de un ajustador o m&eacute;dico dictaminador, red m&eacute;dica o pago a proveedores" /></aui:option>
					<aui:option value="3"><liferay-ui:message key="Problemas de servicio con el Call Center" /></aui:option>
					<aui:option value="4"><liferay-ui:message key="Otros" /></aui:option>
			</aui:select>
			<br>
			<p class="formularioEtiquetaTexto">Por favor especifica de que se trata tu queja y el producto con el que cuentas para que pueda ser atendida por la persona id&oacute;nea</p>
			<br>
			<aui:input cssClass="formularioCampoTexto" type="textarea" label="" placeholder="datosqueja_Datosquejamvcportlet.comentarios" name="comentarios" />
			
		
		</div>
		
		<div id="<portlet:namespace/>seccion_archivos" style="display:none;">
			
			<div id="<portlet:namespace/>div_archivos" style="display:none;">
			<aui:input cssClass="formularioCampoTexto" label="" name="documento" type="file" disabled="true">
		    	<aui:validator name="required" errorMessage="El archivo es requerido"/>
		    	<aui:validator name="acceptFiles" >'doc,docx,xls,xlsx,ppt,pptx,pdf,jpg,jpeg,png,tif,gif'</aui:validator>
		    </aui:input>
			</div>
		    <p class="formularioEtiquetaTexto">Formatos: Word, Excel, Power Point, PDF, JPG, JPEG, PNG, GIF m&aacute;ximo 10 MB.</p>
			</br>
		</div>
		
		<div id="<portlet:namespace/>div_capcha" style="display:none;">
			<liferay-ui:captcha url="<%=captchaURL.toString()%>" />
		</div>
    	<div id="<portlet:namespace/>div_botones_queja_sender">
    	<aui:button-row>
	        	<aui:button name="saveButton" class="btn btn-primary pull-right" type="button" value="datos.queja.general.sender.next" id="submitBtn" onclick="navegarPagina(1);"/>
	    		<aui:button class="btn pull-right" name="cancelButton" id="cancelButton" type="button" value="datos.queja.general.sender.clean" onclick="navegarPagina(-1);" />
	    </aui:button-row>
    	</div>
    	<div id="<portlet:namespace/>div_boton_regreso_queja_sender" style="display:none">
    		<p class="text-left">Su solicitud ha sido enviada con &eacute;xito 
			</br>En breve recibir&aacute; un correo de confirmaci&oacute;n.
			</br>Gracias.</br>
			</br>En caso de no recibir el e-mail de confirmaci&oacute;n &oacute; si tiene alg&uacute;n comentario acerca del servicio, favor de contactarnos para tr&aacute;mites de GMM al 01 800 1111 400 y 5201 3181 y para tr&aacute;mites de Vida al 5201 3039 &oacute; bien al correo: atencion.quejas@allianz.com.mx 
			</br>
			</br>Atentamente: Allianz M&eacute;xico</br>
			</p>
			<p>
			</p>
	    	<aui:button-row>
	    		<aui:button class="btn btn-primary pull-right" name="resetButton" type="button" value="datos.queja.general.sender.reset" onclick="currentPageURLJSClean();" />
	    	</aui:button-row>
    	</div>
	</aui:form>
</div>

<script type="text/javascript"> 
console.log("atencion.queja.sender v1.0.0");
var queja = <%=queja%>;
var currentPageURLJS = '<%=currentPageURL%>';




</script>

<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">
console.log("Queja cargada");
console.log(queja);

/////////////////////////////////////////////////////////
///////////////////Declaracion de funciones//////////////
/////////////////////////////////////////////////////////

Liferay.provide(window, 'mostrarSeccionQuejaGnrl', function(seccionQueja,mostrado) {
	A.one('#<portlet:namespace/>' +  seccionQueja)._node.style.display = mostrado;
});

Liferay.provide(window, 'navegarPagina', function(factor) {
	var pagina = A.one('#<portlet:namespace/>paginaActual');
	var val_pagina = 1*pagina.get('value');
	
	A.one('#<portlet:namespace/>factorNavegacion').set("value",factor);
	
	////Aqui van validaciones
	formsValidSectionTramite = [];
	
	switch(val_pagina) {
		case 0:
			console.log("Aqui no deber�a estar, que chingados");
			break;
		case 1:
			if (factor == -1){
				currentPageURLJSClean();
			} else {
				console.log("Valida pagina 1");
				Liferay.fire('validaDatosTipoQueja', {} );
				Liferay.fire('validaDatosSolicitante', {} );
				Liferay.fire('validaDatosFiscales', {} );
			}
			break;
		case 2:
			console.log("Valida seccion final");
			validaSeccionFinal();
	        break;
	    case 3:
	    	console.log("Este caso existia, yo cre�a que no");
	    	break;
	    default:
	}
	
	
});


Liferay.provide(window, 'configuraPagina', function() {
	var pagina = A.one('#<portlet:namespace/>paginaActual');
	var val_pagina =  pagina.get("value") * 1;
	var factor = A.one('#<portlet:namespace/>factorNavegacion').get("value") * 1;
	console.log("Estaba en pagina: " + val_pagina);
	A.one('#<portlet:namespace/>paginaActual').set( 'value', val_pagina + factor);
	val_pagina = 1*pagina.get('value') ;
	console.log("Ir a pagina: " + val_pagina);
	
	switch(val_pagina) {
		case 0:
			//Aqui va lo de limpiar
			console.log("Pues segun yo ahora limpiaria");
			currentPageURLJSClean();
			break;
		case 1:
			console.log("Despliego pantalla 1");
			A.one('#<portlet:namespace/>cancelButton span').html("Limpiar");
			Liferay.fire('mostrarDatosTipoQueja',{});
	    	Liferay.fire('mostrarDatosSolicitante',{cliente:queja.cliente});
			Liferay.fire('mostrarDatosFiscales',{});
   		    mostrarSeccionQuejaGnrl('seccion_archivos','none');
   		 	mostrarSeccionQuejaGnrl('div_capcha','none');
   		 	mostrarSeccionQuejaGnrl('seccion_datos_queja','none');
			break;
		case 2:
   		    mostrarSeccionQuejaGnrl('seccion_archivos','block');
   		 	mostrarSeccionQuejaGnrl('div_capcha','block');
   		 	mostrarSeccionQuejaGnrl('seccion_datos_queja','block');
   		 	Liferay.fire('ocultarDatosTipoQueja',{});
	    	Liferay.fire('ocultarDatosSolicitante',{});
			Liferay.fire('ocultarDatosFiscales',{});
			console.log("Despliego pantalla 2");
			A.one('#<portlet:namespace/>cancelButton span').html("Regresar");
	        break;
	    case 3:
	    	
	    	A.io.request("<%= uploadResourceURL %>", {
				form: {
					id:"<portlet:namespace/>fm-queja-grl",
					upload: true
				},
				dataType:'text',
				on: {
					failure: function() {
	                    console.log("Ajax failed! There was some error at the server");
	                },
	                success: function(event, id, obj) {
	                	console.log("LLego la solicitud en el success");
	                	console.log(event);
	                	console.log(id);
	                	console.log(obj);
	        		   	console.log(this.get('responseData'));
	        		},
	        		complete: function(data){
	                    console.log("Objeto respuesta !!!!!!!!!!!!");
	                    console.log(data);
	                    console.log("Objeto respuesta !!!!!!!!!!!!");
		        		console.log("Despliego Confirmacion");
						mostrarSeccionQuejaGnrl('div_boton_regreso_queja_sender','block');
	                }
	            }
			});
   		    mostrarSeccionQuejaGnrl('div_botones_queja_sender','none');
   		    mostrarSeccionQuejaGnrl('seccion_archivos','none');
   		 	mostrarSeccionQuejaGnrl('div_capcha','none');
   		 	mostrarSeccionQuejaGnrl('seccion_datos_queja','none');
   		    console.log("Oculto ultima p�gina");
	    	break;
	    	
	    default:
	    	var pagina = A.one('#<portlet:namespace/>paginaActual');
	    	pagina.set( 'value', val_pagina < 1 ? 1: (val_pagina > 4 ? 4:-1));
	    	
	    	console.log("fuera del rango: se pone el contador en " + pagina.get( 'value'));
	}
});

Liferay.provide(window, 'currentPageURLJSClean', function() {
	console.log('currentPageURLJSClean');
	console.log(currentPageURLJS);
	var redir = currentPageURLJS;
	window.location.href = redir;

});


Liferay.provide(window, 'agregaHidden', function(nombre, valor, formGrl) {
	var hiddenA = A.one("#<portlet:namespace />" + nombre );
		
	if ( hiddenA ) 
		if ( valor )  
			hiddenA.set("value",valor );
		else hiddenA.remove();
	else if ( valor )
		formGrl.append('<input type="hidden" name="<portlet:namespace />' + nombre 
				+ '" id="<portlet:namespace />' + nombre + '" value="' + valor + '"/>');
	else console.log("No existia tag y no tenia valor");
});

function validaSeccionTramiteTriple(respuestaSeccion) { 
	console.log("desde la respesta la seccion es" + respuestaSeccion.valido);
	formsValidSectionTramite.push(respuestaSeccion.valido);
	console.log( formsValidSectionTramite );
	Liferay.fire('cargarDatosAFormulario',respuestaSeccion);
	if(formsValidSectionTramite.length % 3 === 0) {
		var factor = A.one('#<portlet:namespace/>factorNavegacion').get("value") * 1;
		if ( (factor === -1 ) || (formsValidSectionTramite[0]
				&& formsValidSectionTramite[1] && formsValidSectionTramite[2])){
			configuraPagina();
		}
	}else {
		console.log("Comportamiento desafortunado");
	}
}


function validaSeccionFinal() { 
	console.log("desde la respesta la seccion final ");
	var factor = A.one('#<portlet:namespace/>factorNavegacion').get("value") * 1;
	var formValidator = Liferay.Form.get('<portlet:namespace />fm-queja-grl').formValidator;
	formValidator.validate();
	if ( factor === 1 ){
		if ( !formValidator.hasErrors() ){
			
		}
		var captcha = A.one('#<portlet:namespace />captchaText') ;
		var captchaTextValue = captcha ? captcha.get('value'): 'NA';
		console.log('validaCaptcha captchaTextValue = ' + captchaTextValue);
		console.log('(NA != captchaTextValue) = ' + ('NA' != captchaTextValue));

		if('NA' != captchaTextValue) {
			console.log('validaCaptcha captcha = ' + captcha);

			console.log('captchaTextValue captcha = ' + captchaTextValue);

			$.post('<%= captchaURL.toString() %>', {
	        		captchaTextValue: captchaTextValue,
	        		gRecaptchaResponse: responseReCaptcha
        	}).always(function(data){
        	console.log("always func");
	        var stringData = JSON.stringify(data.responseText) + "";
	        console.log('respuesta final captcha = ' + ((stringData.indexOf('true') !== -1))) ;
			});
			
			configuraPagina();

		} else {
			var responseReCaptcha = grecaptcha ? grecaptcha.getResponse() : 'NA';
			console.log(responseReCaptcha);
			console.log("Respuesta de recaptcha -> responseReCaptcha  = " + responseReCaptcha);
			if ( responseReCaptcha != "" ){
				if ( !formValidator.hasErrors() ) configuraPagina();
			} else {
				console.log("Sin seleccionar captcha");
				//Aqui deber�a mostrar mensaje de seleccionar capcha
			}
		}
	} else {
		configuraPagina();
	}
}

/////////////////////////////////////////////////////////
///////////////////Declaracion eventos///////////////////
/////////////////////////////////////////////////////////

Liferay.on('validaDatosTipoQuejaRespuesta', validaSeccionTramiteTriple);
Liferay.on('validaDatosFiscalesRespuesta', validaSeccionTramiteTriple);
Liferay.on('validaDatosSolicitanteRespuesta', validaSeccionTramiteTriple);

Liferay.on("cargarDatosAFormulario",function(event){
	var formGrl = A.one('#<portlet:namespace/>fm-queja-grl');
	switch (event.nombreSeccion){
		case 'DatosTipoQueja':
			agregaHidden("tipoTramite",event.tipoTramite , formGrl);
			agregaHidden("tipoTramiteGMM",event.tipoTramiteGMM , formGrl);
			agregaHidden("estado",event.estado , formGrl);
			agregaHidden("municipio",event.municipio , formGrl);
			agregaHidden("sucursal",event.sucursal , formGrl);
			break;
		case 'DatosSolicitante':
			agregaHidden("nombreSolicitante",event.nombreSolicitante , formGrl);
			agregaHidden("apellidoPaternoSolicitante",event.apellidoPaternoSolicitante , formGrl);
			agregaHidden("apellidoMaternoSolicitante",event.apellidoMaternoSolicitante , formGrl);
			agregaHidden("telefonoParticular",event.telefonoParticular , formGrl);
			agregaHidden("telefonoCelular",event.telefonoCelular , formGrl);
			agregaHidden("email",event.email , formGrl);
			break;
		case 'DatosFiscales':
			agregaHidden("tipoSiniestro",event.tipoSiniestro , formGrl);
			agregaHidden("numeroSiniestro",event.numeroSiniestro , formGrl);
			agregaHidden("tipoReclamacion",event.tipoReclamacion , formGrl);
			if ( event.tipoReclamacion )
				agregaHidden("tipoReclamacionDescripcion",event.tipoReclamacionDescripcion , formGrl);
			agregaHidden("observaciones",event.observaciones , formGrl);
			break;
		default:
			console.log("No se detect� correctamente la seccion de procedencia");
	}
});


Liferay.on("mostrarBotonesSolicitudQuejaGeneral",function(event){
	mostrarSeccionQuejaGnrl('div_datos_general_queja_sender','block');
	mostrarSeccionQuejaGnrl('seccion_datos_queja','block');
});

Liferay.on("ocultarBotonesSolicitudQuejaGeneral",function(event){
	mostrarSeccionQuejaGnrl('div_datos_general_queja_sender','none');
});

/////////////////////////////////////////////////////////
///////////////////Cargas Iniciales//////////////////////
/////////////////////////////////////////////////////////
Liferay.fire('mostrarDatosTipoQueja',{});
Liferay.fire('mostrarDatosSolicitante',{cliente:queja.cliente});
Liferay.fire('mostrarDatosFiscales',{});

console.log("Termin� de cargar el sender de queja");

</aui:script>