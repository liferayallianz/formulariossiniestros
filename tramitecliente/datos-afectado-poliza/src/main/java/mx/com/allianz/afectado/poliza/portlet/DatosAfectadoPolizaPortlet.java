package mx.com.allianz.afectado.poliza.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.google.gson.Gson;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import mx.com.allianz.afectado.poliza.constants.DatosAfectadoPolizaPortletKeys;
import mx.com.allianz.commons.catalogos.dto.ProductoDTO;
import mx.com.allianz.service.catalogos.model.ProductoACX;
import mx.com.allianz.service.catalogos.service.ProductoACXLocalServiceUtil;

/**
 * @author sfrancof
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=Tramite",
		"com.liferay.portlet.instanceable=true",
		"com.liferay.portlet.requires-namespaced-parameters=false",
		"javax.portlet.display-name=datos-afectado-poliza Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + DatosAfectadoPolizaPortletKeys.DatosAfectadoPoliza,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,guest"
	},
	service = Portlet.class
)
public class DatosAfectadoPolizaPortlet extends MVCPortlet {
	
	public static final String PRODUCTO_ACTIVO = "ACT";
	
	@Override
    public void serveResource(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse) throws IOException,
            PortletException {
		_log.info("DatosAfectadoPolizaPortlet -> serveResource");
		
		Gson gson = new Gson();

		try {
			
	        JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
	        
	        Optional.ofNullable(ProductoACXLocalServiceUtil.getProductoACXs(-1,-1))
	        .orElseGet(ArrayList<ProductoACX>::new).stream()
	        .map(producto -> 
			new ProductoDTO(producto.getCodigoProducto(), 
							producto.getDescripcionProducto(),
							producto.getTipoProducto())
			)
	        .filter(producto -> PRODUCTO_ACTIVO.equals(producto.getTipoProducto()))
//	.filter(producto -> filter.isEmpty() || filter.contains(producto.getCodigoProducto()))
//	.filter(producto -> {
//		return (tramiteClienteHabilitado && !productosCliente.isEmpty()) ? 
//				productosCliente.stream()
//								.map(ProductoClienteDTO::getCodigoProducto)
//								.filter(producto.getCodigoProducto()::equals)
//								.findAny()
//								.isPresent() : true;
//	})
	        .peek(_log::debug)
	        .sorted((producto1, producto2) -> producto1.getDescripcionProducto().compareTo(producto2.getDescripcionProducto()))
	        .map(gson::toJson).map(DatosAfectadoPolizaPortlet::parse)
	        .filter(Optional::isPresent).map(Optional::get)
	        .forEach(jsonArray::put); 
	        resourceResponse.setContentType("text/javascript");
	        PrintWriter writer = resourceResponse.getWriter();
	        writer.write(jsonArray.toString());
		} catch (Exception e ) { e.printStackTrace(); }
	}
	
	private static Optional<JSONObject> parse(String gsonString) {
		JSONParser parser = new JSONParser();
		Optional<JSONObject> objectJson = null;
		try {
			objectJson = Optional.ofNullable((JSONObject)parser.parse(gsonString));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return objectJson;
	}
	
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		if (_log.isInfoEnabled()) {
			_log.info("Cargado : datos.afectado.poliza -  v1.0.0  ");
		}
	}
	
	private static Log _log = LogFactoryUtil.getLog(DatosAfectadoPolizaPortlet.class);
}