<%@ include file="/init.jsp" %>

<portlet:resourceURL var="productosResourceURL" />

<div id="<portlet:namespace/>div_datos_afectado_poliza" style="display:none" >
<p class="formularioTituloEncabezado">Datos del afectado</p>
<aui:form action="#" name="fm_datos_afectado_poliza">
	<div id="<portlet:namespace />div_afectado_datos_afectado">
		<aui:input cssClass="formularioCampoTexto" name="nombre" type="text" value="" label="" placeholder="datos.afectado.nombre" >
			<aui:validator name="required" errorMessage="El nombre del afectado es requerido"/>
			<aui:validator name="rangeLength" errorMessage="La longitud del nombre del afectado debe ser menor a 75 caracteres">[1,75]</aui:validator>
			<aui:validator name="custom"  errorMessage="Por favor ingrese s&oacute;lo letras, guiones o espacios">
		        function (val, fieldNode, ruleValue) {
		                var returnValue = true;
		                var iChars = "~!@#$%&*()+=[]\\;,./{}|\":<>?0123456789";
		                for (var i = 0; i < val.length; i++) {
		                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
		                    	returnValue = false;
		                    }
		                }
		                return returnValue;
		        }
		    </aui:validator>
		</aui:input>
		<aui:input cssClass="formularioCampoTexto" name="apellidoPaterno" type="text" value="" label="" placeholder="datos.afectado.apellido.paterno" >
			<aui:validator name="required" errorMessage="El apellido paterno del afectado es requerido" />
			<aui:validator name="rangeLength" errorMessage="La longitud del apellido paterno del afectado debe ser menor a 50 caracteres">[1,50]</aui:validator>
			<aui:validator name="custom"  errorMessage="Por favor ingrese s&oacute;lo letras, guiones o espacios">
		        function (val, fieldNode, ruleValue) {
		                var returnValue = true;
		                var iChars = "~!@#$%&*()+=[]\\;,./{}|\":<>?0123456789";
		                for (var i = 0; i < val.length; i++) {
		                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
		                     returnValue = false;
		                    }
		                }
		                return returnValue;
		        }
		    </aui:validator>
		</aui:input>
		<aui:input cssClass="formularioCampoTexto" name="apellidoMaterno" type="text" value="" label="" placeholder="datos.afectado.apellido.materno" >
			<aui:validator name="rangeLength" errorMessage="La longitud del apellido materno del afectado debe ser menor a 50 caracteres">[1,50]</aui:validator>
			<aui:validator name="custom"  errorMessage="Por favor ingrese s&oacute;lo letras, guiones o espacios">
		        function (val, fieldNode, ruleValue) {
		                var returnValue = true;
		                var iChars = "~!@#$%&*()+=[]\\;,./{}|\":<>?0123456789";
		                for (var i = 0; i < val.length; i++) {
		                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
		                     returnValue = false;
		                    }
		                }
		                return returnValue;
		        }
		    </aui:validator>
		</aui:input>
	</div>
		
	<div id="<portlet:namespace />div_afectado_datos_poliza">
		<aui:select cssClass="formularioCampoTexto" id="afectado_emisor_poliza" name="emisorPoliza" label="" required="false" errorMessage="Debe seleccionar una poliza">
		    <aui:option selected="true" value="">
		        <liferay-ui:message key="numero.poliza.select.vacio" />
		    </aui:option>
	    </aui:select>
		<aui:input cssClass="formularioCampoTexto" id="afectado_numero_poliza" name="numeroPoliza" type="text" value="" label="" placeholder="numero.poliza.numero.poliza" >
			<aui:validator name="rangeLength" errorMessage="La longitud del n&uacute;mero de p&oacute;liza debe ser mayor a 3 y menor a 10 caracteres">[3,10]</aui:validator>
			<aui:validator name="required" errorMessage="El n&uacute;mero de p&oacute;liza es requerido cuando se selecciona un producto">
	              function(val) {
	                     return A.one('#<portlet:namespace />afectado_emisor_poliza').get('selectedIndex') > 0;
	              }
	   		</aui:validator>
	   		<aui:validator name="custom" errorMessage="Por favor ingrese s&oacute;lo n&uacute;meros o guiones">
		        function (val, fieldNode, ruleValue) {
		                var returnValue = true;
		                var iChars = "-0123456789";
		               	var value = fieldNode.get('value');
		                for (var i = 0; i < value.length; i++) {
		                    if (returnValue && iChars.indexOf(value.charAt(i)) == -1) {                  
		                    		returnValue = false;
		                    }
		                }
		                return returnValue;
		        }
		    </aui:validator>
		</aui:input>
	
	</div>
	
	<div id="<portlet:namespace />div_afectado_combo_afectado">
		<aui:select cssClass="formularioCampoTexto" id="asegurado_poliza_afectado" name="aseguradoCliente" label="" required="false" errorMessage="Debe seleccionar un asegurado" >
		    <aui:option selected="true" value="">
		        <liferay-ui:message key="Seleccione un Asegurado" />
		    </aui:option>
		    
    	</aui:select>
	</div>
</aui:form>
</div>

<script type="text/javascript"> 
console.log("datos.afectado.poliza v1.0.0");
</script>

<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">
Liferay.provide(window, 'mostrarSeccionAfectadoPoliza', function(seccionPoliza,mostrado) {
	A.one('#<portlet:namespace/>'+seccionPoliza)._node.style.display = mostrado;
});

Liferay.on("mostrarDatosAfectadoPoliza",function(event){
	var optionsPoliza = A.all("#<portlet:namespace/>afectado_emisor_poliza option");
	
	mostrarSeccionAfectadoPoliza('div_datos_afectado_poliza','block');
	
	if ( event.cliente ){
		mostrarSeccionAfectadoPoliza("div_afectado_combo_afectado", "none" );
		mostrarSeccionAfectadoPoliza("div_afectado_datos_poliza", "block");
		mostrarSeccionAfectadoPoliza("div_afectado_datos_afectado",  "none" );
		var productos = event.cliente.cliente ? event.cliente.cliente.productos : null ;
		console.log(productos)
		if ( productos ){
			if ( (optionsPoliza._nodes ? optionsPoliza._nodes.length : -1) < 2 ){
				console.log("ya se hab�an cargado registros");
			} else {
				for (var j=0; j < productos.length; j++) {
		        	productosSelect.append("<option value='" + productos[j].codigoProducto + "'>" 
		        			+ productos[j].codigoProducto + " - " + productos[j].descripcionProducto + "</option>");
		        }
				
				productosSelect.on("change",function(param){
		
					var selectedIndex = param.selectedIndex;
					console.log("productos saveNumeroPolizaSelected, selectedIndex = " +  param.selectedIndex);
					console.log("productos saveNumeroPolizaSelected, value = !!" +  param.value + "!!");
					
					for (prod in productos) {
						console.log("codigo producto en productosCliente = !!" + productosCliente[prod].codigoProducto +"!!");
			
						console.log("existe producto en productosCliente? " +  (param.value == productosCliente[prod].codigoProducto));
			
						if (param.value == productosCliente[prod].codigoProducto){
							var productoPoliza = productosCliente[prod];
							var numeroPoliza = productoPoliza.numeroPoliza; 
							var asegurados = productoPoliza.asegurados;
							var contratante = productoPoliza.contratante;
							console.log(productoPoliza);
							console.log("contratante en productosCliente = " + contratante);
							console.log("productoPoliza en productosCliente = " + productoPoliza);
							console.log("numeroPoliza en productosCliente = " + numeroPoliza);
			
							A.one('#<portlet:namespace />numero_poliza_numero_poliza').set('value',numeroPoliza); 
							Liferay.fire('presentarContratantePoliza', {contratante:contratante});
							mostrarSeccionAfectadoPoliza("div_afectado_combo_afectado",  "block");
						    asegurados = asegurados[0];
							if (asegurados) {
							   	console.log("Porcargar los  aseguradops:"+asegurados.length);
								var aseguradosSelect = A.one('#<portlet:namespace />asegurado_poliza_afectado');
						   		aseguradosSelect.html("");	
						   		aseguradosSelect.append("<option value=''>Seleccione un asegurado </option>");
						        for (var j = 0; j < asegurados.length; j++) {
						        	var asegurado = asegurados[j];
						        	//console.log(asegurado);
						        	aseguradosSelect.append("<option value='" + asegurado.idAsegurado + "'>" + 
						        			( asegurado.nombre ? asegurado.nombre : asegurado[0].contratante) + "</option>");
						        }
							}
						}
					}
				});
			}
		}
	} else {
		if ( (optionsPoliza._nodes ? optionsPoliza._nodes.length : -1) < 2 )
			cargarOptionsComboPoliza();
		else console.log("Se previno la carga");
		mostrarSeccionAfectadoPoliza("div_afectado_combo_afectado", "none");
		mostrarSeccionAfectadoPoliza("div_afectado_datos_poliza",  "block");
		mostrarSeccionAfectadoPoliza("div_afectado_datos_afectado",  "block");
	}
	
});

Liferay.on("ocultarDatosAfectadoPoliza",function(event){
	mostrarSeccionAfectadoPoliza('div_datos_afectado_poliza','none');
});

Liferay.on('validaDatosAfectadoPoliza', function(event){
	console.log('Datos Afectado Poliza Liferay on');
	var formValidator = Liferay.Form.get('<portlet:namespace />fm_datos_afectado_poliza').formValidator;
	var eventoRespuesta = {};
	if(formValidator) {
		formValidator.validate();
		eventoRespuesta.valido = !formValidator.hasErrors();
		console.log('validaDatosAfectadoPoliza eventoRespuesta.valido = ' + eventoRespuesta.valido);
		if (eventoRespuesta.valido) {
			eventoRespuesta.idAfectado =  A.one('#<portlet:namespace />asegurado_poliza_afectado').get('value');
			eventoRespuesta.nombreAfectado = A.one('#<portlet:namespace />nombre').get('value');
			eventoRespuesta.apellidoPaternoAfectado = A.one('#<portlet:namespace />apellidoPaterno').get('value');
			eventoRespuesta.apellidoMaternoAfectado = A.one('#<portlet:namespace />apellidoMaterno').get('value');
			eventoRespuesta.producto = A.one('#<portlet:namespace />afectado_emisor_poliza').get('value');
			eventoRespuesta.numeroPoliza = A.one('#<portlet:namespace />afectado_numero_poliza').get('value');
		}	
	} else {
		eventoRespuesta.valido = false;
	}
	eventoRespuesta.nombreSeccion = "DatosAfectadoPoliza";
	Liferay.fire('validaDatosAfectadoPolizaRespuesta', eventoRespuesta );
});

Liferay.provide(window, 'cargarOptionsComboPoliza', function() {
    A.io.request (
    		'<%= productosResourceURL %>', {
        	method: 'get',
         	dataType: 'json',
	        on: {
                failure: function() {
                    console.log("Ajax failed! There was some error at the server");
                },
                success: function(event, id, obj) {
                    var productos = this.get('responseData');
                    console.log(productos);
                    var productosAjax = A.one("#<portlet:namespace/>afectado_emisor_poliza");
                    productosAjax.html("");
                    productosAjax.append("<option value=''>Seleccione un Producto *</option>");
                    for (var j=0; j < productos.length; j++) 
                    	productosAjax.append("<option value='" + productos[j].codigoProducto + "'>" 
                    			+ productos[j].codigoProducto + " - " + productos[j].descripcionProducto + "</option>");
                    
                }
	        }
	}); 
});


</aui:script>