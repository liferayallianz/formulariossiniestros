package mx.com.allianz.complemento.tramite.constants;

/**
 * @author sfrancof
 */
public class DatosComplementoTramitePortletKeys {

	public static final String DatosComplementoTramite = "DatosComplementoTramite";

}