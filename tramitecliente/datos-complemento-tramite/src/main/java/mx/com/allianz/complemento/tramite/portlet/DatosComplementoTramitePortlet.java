package mx.com.allianz.complemento.tramite.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.google.gson.Gson;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.ParamUtil;

import mx.com.allianz.commons.catalogos.dto.MonedaDTO;
import mx.com.allianz.commons.catalogos.dto.SubgrupoDTO;
import mx.com.allianz.complemento.tramite.constants.DatosComplementoTramitePortletKeys;
import mx.com.allianz.service.catalogos.model.GrupoPago;
import mx.com.allianz.service.catalogos.model.Moneda;
import mx.com.allianz.service.catalogos.model.SubGrupo;
import mx.com.allianz.service.catalogos.service.GrupoPagoLocalServiceUtil;
import mx.com.allianz.service.catalogos.service.MonedaLocalServiceUtil;
import mx.com.allianz.service.catalogos.service.SubGrupoLocalServiceUtil;

/**
 * @author sfrancof
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=Tramite",
		"com.liferay.portlet.instanceable=true",
		"com.liferay.portlet.requires-namespaced-parameters=false",
		"javax.portlet.display-name=datos-complemento-tramite Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + DatosComplementoTramitePortletKeys.DatosComplementoTramite,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,guest"
	},
	service = Portlet.class
)
public class DatosComplementoTramitePortlet extends MVCPortlet {
	
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		_log.info("DatosComplementoTramitePortlet -> render -> inicio");
		
		try {

			List<MonedaDTO> monedas = Optional.ofNullable(MonedaLocalServiceUtil.getMonedas(-1,-1))
											  .orElseGet(ArrayList<Moneda>::new)
											  .stream()
											  .map(moneda -> 
											  		new MonedaDTO(moneda.getIdMoneda(),
											  					  moneda.getCodigoMoneda(), 
											  					  moneda.getDescripcion())
											  )
											  .collect(Collectors.toList());
			renderRequest.setAttribute("monedas", monedas);
		} catch (Exception e ) { 
			e.printStackTrace(); 
		}
		
		super.render(renderRequest, renderResponse);
	}
	
	@Override
    public void serveResource(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse) throws IOException,
            PortletException {
		if(_log.isDebugEnabled()) {
			_log.info("DatosComplementoTramitePortlet -> serveResource");
		}
		Gson gson = new Gson();
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		String parametro = ParamUtil.getString(resourceRequest, "estado");
		String valorEstado = getClaveEstadoSubGrupoPago(parametro);
		try {
			if(_log.isDebugEnabled()) {
				_log.info("parametro = " + parametro);
			}
	        List<GrupoPago> gruposPago = Optional.ofNullable(GrupoPagoLocalServiceUtil.getGrupoPagos(-1, -1))
	        									 .orElseGet(ArrayList<GrupoPago>::new);
	        Optional.ofNullable(SubGrupoLocalServiceUtil.getSubGrupos(-1,-1))
	        		.orElseGet(ArrayList<SubGrupo>::new)
	        		.stream()
	        		.filter(subgrupo -> subgrupo.getCodigoEstado().equals(valorEstado))
	        		.map(subgrupo -> 
        				new SubgrupoDTO(subgrupo.getIdSubGrupo(), 
				        				subgrupo.getCodigoSubGrupo(), subgrupo.getCodigoGrupoPago(),
						    			subgrupo.getDescripcionSubGrupo(), subgrupo.getCodigoPais(),
						    			subgrupo.getCodigoEstado(), subgrupo.getCodigoCiudad(),
						    			subgrupo.getCodigoMunicipio(), subgrupo.getDireccion(),
						    			subgrupo.getNombreContacto(), subgrupo.getTelefono(),
						    			subgrupo.getZip(), subgrupo.getColonia(), 
						    			subgrupo.getStsCodigoSubGrupo(), subgrupo.getTipoId(),
						    			subgrupo.getNumeroId(), subgrupo.getDvId())
	        		)
	        		.map(subgrupo -> {
			        	subgrupo.setDescripcionGrupo(
			        			gruposPago.stream()
			        					  .filter(grupopago -> 
			        						  	subgrupo.getCodigoGrupoPago().equals(String.valueOf(grupopago.getCodigoGrupoPagos()))
			        					  )
			        					  .map(GrupoPago::getDescripcionGrupo)
			        					  .findAny()
			        					  .orElse("")
			        	);
	        		return subgrupo;
	        		})
	        		.sorted((subgrupo1, subgrupo2) -> 
	        			subgrupo1.getDescripcionSubGrupo().compareTo(subgrupo2.getDescripcionSubGrupo())
	        		)
	        		.map(gson::toJson)
	        		.map(DatosComplementoTramitePortlet::parse)
	        		.filter(Optional::isPresent)
	        		.map(Optional::get)
	        		.forEach(jsonArray::put); 
	        
	        resourceResponse.setContentType("text/javascript");
	        PrintWriter writer = resourceResponse.getWriter();
	        writer.write(jsonArray.toString());
		} catch (Exception e ) { e.printStackTrace(); 
			_log.info("Tronó esta mamada al buscar los subgrupos:" + e.getMessage());
		}
    }
	
	private String getClaveEstadoSubGrupoPago(String claveEstado) {
		String valorEstado = "009";
		if(claveEstado != null) {
			int longIdEstado = claveEstado.length();
			valorEstado = (longIdEstado == 1 ? "00": longIdEstado == 2 ? "0" : "") + claveEstado;
		}
		return valorEstado;
	}
	
	private static Optional<JSONObject> parse(String gsonString) {
		JSONParser parser = new JSONParser();
		Optional<JSONObject> objectJson = null;
		try {
			objectJson = Optional.ofNullable((JSONObject)parser.parse(gsonString));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return objectJson;
	}
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		if (_log.isInfoEnabled()) {
			_log.info("Portlet datos.complemento.tramite - 1.0.0, Cargado");
		}
	}
	
	private static Log _log = LogFactoryUtil.getLog(DatosComplementoTramitePortlet.class);

}