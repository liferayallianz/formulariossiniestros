package mx.com.allianz.datos.tramite.solicitud.config;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(
		category = "Productivity",
		scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE
	)
	@Meta.OCD(
		localization = "content/Language",
		name = "Configuracion Tramite Solicitud Completo",
		id = "mx.com.allianz.datos.tramite.solicitud.config.DatosTramiteSolicitudConfiguration"
	)
public interface DatosTramiteSolicitudConfiguration {
	
	@Meta.AD(
			deflt = "https://portalb.allianz.com.mx/az-wso2-services/catalogos_allianz/op_sel_json_cat_estado?codpais=412", 
			description = "URL de origen de datos JSON de los estados",
			required = false
		)
	public String estadosSourceURL();
	
	@Meta.AD(
			deflt = "https://portalb.allianz.com.mx/az-wso2-services/catalogos_allianz/op_sel_json_cat_municipio?codpais=412&codestado=", 
			description = "URL de origen de datos JSON de los municpios",
			required = false
		)
	public String municipiosSourceURL();
	
	@Meta.AD(
			deflt = "1:Guadalajara,2:Matriz (CDMX),3:Monterrey", 
			description = "Mapeo de valores de sucursales",
			required = false
		)
	public String[] sucursales();
	

}
