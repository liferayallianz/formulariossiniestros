package mx.com.allianz.datos.tramite.solicitud.constants;

/**
 * @author sfrancof
 */
public class DatosTramiteSolicitudPortletKeys {

	public static final String DatosTramiteSolicitud = "DatosTramiteSolicitud";

}