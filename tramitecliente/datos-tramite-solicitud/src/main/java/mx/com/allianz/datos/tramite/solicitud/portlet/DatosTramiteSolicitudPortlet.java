package mx.com.allianz.datos.tramite.solicitud.portlet;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import aQute.bnd.annotation.metatype.Configurable;
import mx.com.allianz.commons.catalogos.dto.SucursalDTO;
import mx.com.allianz.datos.tramite.solicitud.config.DatosTramiteSolicitudConfiguration;
import mx.com.allianz.datos.tramite.solicitud.constants.DatosTramiteSolicitudPortletKeys;

/**
 * @author sfrancof
 */
@Component(
		configurationPid = "mx.com.allianz.datos.tramite.solicitud.config.DatosTramiteSolicitudConfiguration",
		immediate = true,
		property = {
			"com.liferay.portlet.display-category=Tramite",
			"com.liferay.portlet.instanceable=true",
			// Se agregan parametros para recibir de eventos de portlet
	        "com.liferay.portlet.requires-namespaced-parameters=false",
	        "com.liferay.portlet.private-request-attributes=false",
		    "com.liferay.portlet.private-session-attributes=false",
			// fin Se agregan parametros para recibir de eventos de portlet
	        "javax.portlet.name=" + DatosTramiteSolicitudPortletKeys.DatosTramiteSolicitud,
			"javax.portlet.display-name=Datos Tramite Solicitud",
			"javax.portlet.init-param.template-path=/",
			"javax.portlet.init-param.view-template=/view.jsp",
			"javax.portlet.resource-bundle=content.Language",
			// Fin Se agrega nombre de mensaje y cola del portlet para recibir
			"javax.portlet.security-role-ref=power-user,user,guest"
		},
		service = Portlet.class
	)
	public class DatosTramiteSolicitudPortlet extends MVCPortlet {
		
		@Override
		public void render(RenderRequest renderRequest, RenderResponse renderResponse)
				throws IOException, PortletException {
			try {
				_log.info("Dentro del render de solicitud tramite juntos");
				renderRequest.setAttribute("estadosSourceURL", _configuration.estadosSourceURL());
				renderRequest.setAttribute("municipiosSourceURL", _configuration.municipiosSourceURL());
				renderRequest.setAttribute("sucursales", 
					Arrays.asList(_configuration.sucursales()).stream()
						.map(suc -> suc.split(":"))
						.map(suc -> new SucursalDTO(Integer.valueOf(suc[0]), suc[1]))
						.collect(Collectors.toList()));
			} catch (Exception e ) { e.printStackTrace(); }

			super.render(renderRequest, renderResponse);
		}
		
		@Activate
		@Modified
		protected void activate(Map<String, Object> properties) {
			_configuration = Configurable.createConfigurable(
					DatosTramiteSolicitudConfiguration.class, properties);
			
			if (_log.isInfoEnabled()) {
				_log.info(new StringBuilder("Portlet datos.tramite.solicitud - 1.0.0, Cargado :\n{")
						.append("estadosSourceURL:").append(_configuration.estadosSourceURL())
						.append(",municipiosSourceURL:").append(_configuration.municipiosSourceURL())
						.append(",sucursales:").append(Arrays.asList(_configuration.sucursales()))
						.append("}").toString());
			}
		}
		
		private static Log _log = LogFactoryUtil.getLog(DatosTramiteSolicitudPortlet.class);
		private DatosTramiteSolicitudConfiguration _configuration;
		
		
		
	}