<%@ include file="/init.jsp" %>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="mx.com.allianz.commons.catalogos.dto.SucursalDTO"%>

<% 
	List<SucursalDTO> sucursales = new ArrayList<>();
    Object object = request.getAttribute("sucursales");
	if (object != null && object instanceof List) {
		sucursales = (List<SucursalDTO>) object;
	}
%>


<div id="<portlet:namespace/>div_datos_tramite_solicitud" style="display:block;">
	<div id="<portlet:namespace/>titulo_datos_tramite"></div>
	<aui:form action="" method="post" name="fm_datos_tramite_solicitud">
	
		<aui:field-wrapper name="" cssClass="formularioRadioCentrado">
			<aui:input checked="<%=false%>" inlineLabel="right" id="radioTipoTramiteGMM" name="tipoTramite" type="radio" value="gmm" label="tipos.tramite.gmm" onClick="tipoTramiteGMM();"/> &nbsp;
			<aui:input checked="<%=false%>" inlineLabel="right" id="tipoTramiteVida" name="tipoTramite" type="radio" value="vida" label="tipos.tramite.vida" onClick="tipoTramiteVida();">
				<aui:validator name="required" errorMessage="El tipo de Tr&aacute;mite es requerido"/>
			</aui:input>&nbsp;
		</aui:field-wrapper>
		<div id="<portlet:namespace/>div_tipo_tramite_gmm" class="radioSpaceMargin" style="display:none;">
			<aui:field-wrapper name="" cssClass="formularioRadioCentrado">
				<aui:input checked="<%=false%>" inlineLabel="right" id="tipoTramiteGMMReembolso" name="tipoTramiteGMM" type="radio" value="Reembolso" label="tipo.tramite.gmm.reembolso" onClick="showModalIndicacionesTramiteGMM('Solicitud de Reembolso de Gastos M&eacute;dicos Mayores');"/> &nbsp;
				<aui:input checked="<%=false%>" inlineLabel="right" id="tipoTramiteGMMCirugia" name="tipoTramiteGMM" type="radio" value="CirProgramada" label="tipo.tramite.gmm.cirugia" onClick="showModalIndicacionesTramiteGMM('Solicitud de Cirug&iacute;a Programada de Gastos M&eacute;dicos Mayores');"/>
			</aui:field-wrapper>
			<br/>
		</div>
		<div id="<portlet:namespace/>div_datos_solicitud_tramite" style="display:none;">
			<p class="formularioEtiquetaTexto">Lugar donde usted est&aacute; realizando el tr&aacute;mite:</p>
			<aui:select cssClass="formularioCampoTexto" id="estado_tramite" label="" name="estado"  onChange='fetchCities();' required="true" errorMessage="El estado donde usted est� realizando el tr�mite es requerido" >
				<aui:option selected="true" value="">
				        <liferay-ui:message key="datos.solicitud.tramite.estado.obligatorio" />
			    </aui:option>
			</aui:select>
			<aui:select cssClass="formularioCampoTexto" id="municipio_tramite" label="" name="municipio" required="true" errorMessage="El municipio donde usted est� realizando el tr�mite es requerido">
				<aui:option value="">
					<liferay-ui:message key="datos.solicitud.tramite.municipio.obligatorio" />
			    </aui:option>
			</aui:select>
			
			<p class="formularioEtiquetaTexto">Lugar donde quiere que sea atendido el tr&aacute;mite:</p>
			<aui:select cssClass="formularioCampoTexto" id="sucursal_tramite" label="" name="sucursal" required="true" errorMessage="El lugar donde quiere que sea atendido el tr�mite es requerido" >
				<aui:option selected="true" value="">
					<liferay-ui:message key="datos.solicitud.tramite.sucursal.obligatorio" />
			    </aui:option>
		     	<% 
	    		for(SucursalDTO sucursal: sucursales){
			   	%>
		     	<aui:option value="<%=sucursal.getIdSucursal()%>"><%=sucursal.getDescripcion()%></aui:option>
		 	   	<% 	  
				   };
				%>
			</aui:select>
		</div>
	</aui:form>
</div>

<div aria-labelledby="Title" 
class="fade in lex-modal modal" id="<portlet:namespace/>indicaciones_tramite_vida_modal"
role="dialog" tabindex="-1" style="display:none;">
	<div class="modal-dialog modal-full-screen"  style="height:500px;">
		<div class="modal-content">
			<div id="<portlet:namespace/>indicaciones_tramite_vida_modal_div">
					<div class="modal-header">
						<button aria-labelledby="Close" class="btn btn-default close" data-dismiss="modal" role="button" type="button" onClick="closeIndicacionesTramiteVidaModal();">
							<svg aria-hidden="true" class="lexicon-icon lexicon-icon-times">
								<use xlink:href="<%= themeDisplay.getPathThemeImages() + "/lexicon/icons.svg" %>#times" />
							</svg>
						</button>
						<h4 class="modal-title " ><liferay-ui:message key="Tr&aacute;mites @Clientes - Documentos para Siniestros Vida" /></h4>
					</div>
					<div class="modal-body">
						<h4>DOCUMENTOS PARA INDEMNIZACI&Oacute;N VIDA INDIVIDUAL</h4>
						</br>
						<h4>DOCUMENTOS PARA COBERTURA B&Aacute;SICA POR FALLECIMIENTO Y GASTOS FUNERARIOS</h4>
						</br>
						<h4>DOCUMENTOS DEL ASEGURADO</h4>
						</br>
						<p>PRESENTAR ORIGINAL DE:<p>
							<p>&emsp;Acta de defunci&oacute;n.</p>
							<p>&emsp;Acta de nacimiento.</p>
						<p>P&oacute;liza que lo acredita como asegurado de Allianz M&eacute;xico vigente, que incluya:</p>
							<p>&emsp;1.-Contratante, n&uacute;mero de p&oacute;liza y anexos.</p>
							<p>&emsp;2.-Regla de suma asegurada y/o suma asegurada.</p>
							<p>&emsp;3.-Designaci&oacute;n de beneficiarios.</p>
							<p>&emsp;4.-Firma del Asegurado.</p>
							<p>&emsp;5.-Firma del Director de la Aseguradora.</p>
						</br>
						<p>PRESENTAR COPIA SIMPLE DE:</p>
							<p>&emsp;Identificaci&oacute;n Oficial vigente (credencial de elector, pasaporte, c&eacute;dula profesional, licencia de conducir, credencial del IMSS) (No aplica para gastos funerarios).</li>
							<p>&emsp;Comprobante de Domicilio en caso de ser diferente al registrado en el IFE o de no encontrarse en la identificaci&oacute;n presentada (Recibos Agua, Luz, Tel&eacute;fono, impuesto predial o estados de cuenta bancarios, con una vigencia no mayor a tres meses de su fecha de emisi&oacute;n) (No aplica para gastos funerarios).</p>
							<p>&emsp;Acta de matrimonio (En caso de que el beneficiario sea el c&oacute;nyuge).</p>
						<br>
						<p>DOCUMENTOS ADICIONALES PARA OTRAS COBERTURAS VIDA INDIVIDUAL</p>
						<p>PARA LA COBERTURA DE MUERTE ACCIDENTAL</p>
						<p>PRESENTAR COPIA SIMPLE POR EL MINISTERIO P&Uacute;BLICO DE:</p>
							<p>&emsp;Actuaciones de Ministerio P&uacute;blico, cuando la causa del fallecimiento sea a consecuencia de accidente.</p>
						</br>
						<p>PARA LA COBERTURA DE P&Eacute;RDIDAS ORG&Aacute;NICAS E INVALIDEZ TOTAL Y PERMANENTE</p>
						<p>PRESENTAR COPIA CERTIFICADA POR EL IMSS Y/O ISSSTE DE:</p>
							<p>&emsp;Formato ST-3 &oacute; ST-4 de Dictamen de Invalidez emitido por el IMSS, o Formato de Invalidez del ISSSTE. (para invalidez o p&eacute;rdidas org&aacute;nicas).</p>
							<p>&emsp;&Uacute;ltimas Radiograf&iacute;as (en caso necesario por p&eacute;rdida org&aacute;nica).</p>
						</br>
					</div>

					<div class="modal-footer">
						<button class="btn btn-link close-modal" data-dismiss="modal" type="button" onClick="closeIndicacionesTramiteVidaModal();"><liferay-ui:message key="Cerrar" /></button>
					</div>
			</div>
		</div>
	</div>
</div>

<div class="fade in lex-modal modal" id="<portlet:namespace/>indicaciones_tramite_gmm_modal"
role="dialog" tabindex="-1" style="display:none;">
	<div class="modal-dialog modal-full-screen" style="height:500px;" >
		<div class="modal-content">
			<div id="<portlet:namespace/>indicaciones_tramite_gmm_modal_div">
					<div class="modal-header">
						<button aria-labelledby="Close" class="btn btn-default close" data-dismiss="modal" role="button" type="button" onClick="closeIndicacionesTramiteGMMModal();">
							<svg aria-hidden="true" class="lexicon-icon lexicon-icon-times">
								<use xlink:href="<%= themeDisplay.getPathThemeImages() + "/lexicon/icons.svg" %>#times" />
							</svg>
						</button>
						<h4 class="modal-title" ><liferay-ui:message key="Tr&aacute;mite @Clientes - Documentos para GMM" /></h4>
					</div>
					<div class="modal-body" >
						<h4>DOCUMENTOS PARA GMM</h4>
							<p>&emsp;1.-Aviso de accidente y/o enfermedad requisitado por completo y firmado.</p>
							<p>&emsp;2.-Informe M&eacute;dico (Historia cl&iacute;nica completa) requisitados por completo y firmados.</p>
							<p>&emsp;3.-Interpretaci�n de estudios.</p>
							<p>&emsp;4.-Presupuesto de honorarios en caso que el m&eacute;dico no pertenezca a la red de Allianz.</p>
							<p>&emsp;5.-Comprobante de domicilio (con vigencia no mayor a 3 meses). Identificaci&oacute;n oficial con firma del cliente y formato de indentificaci&oacute;n o solicitud de reembolso requisitado en su totalidad.</p>
						<h4>NOTAS:</h4>
						<p>Para tr&aacute;mites de Reembolso es indispensable anexar a lo anterior factura y/o recibos con desglose a nombre del titular de la p&oacute;liza.</p>
						<p>En caso de solicitar transferencia es necesario ingresar el Aviso de Accidente / Enfermedad en original.</p>	
						<p>Allianz puede solicitar los comprobantes originales si as&iacute; lo requiere para continuar con su reclamaci&oacute;n.</p>
					</div>

					<div class="modal-footer">
						<button class="btn btn-link close-modal" data-dismiss="modal" type="button" onClick="closeIndicacionesTramiteGMMModal();"><liferay-ui:message key="Cerrar" /></button>
					</div>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
console.log("datos.tramite.solicitud v1.0.0");

Liferay.provide(window, 'obtenerTipoTramite', function() {
	return {
		tipoTramite : $('#<portlet:namespace />radioTipoTramiteGMM').prop('checked') ? 'GMM' : 'Vida',
		tipoTramiteGMM : $('#<portlet:namespace />tipoTramiteGMMReembolso').prop('checked') ? 'Reembolso' : 'CirProgramada'
	};
});

</script>
<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">

/////////////////////////////////////////////////////////
///////////////////Declaracion de funciones//////////////
/////////////////////////////////////////////////////////

Liferay.provide(window, 'cargarRegistrosCombo', function(urlString, onSuccess) {
    A.io.request (
    	urlString, {
        	method: 'get',
         	dataType: 'json',
         	headers: {
                'Accept': 'application/json'
			},
	        on: {
                failure: function() {
                    console.log("Ajax failed! There was some error at the server");
                },
                success: onSuccess
	        }
	}); 
});

Liferay.provide(window, 'tipoTramiteGMM', function() {
	A.one('#<portlet:namespace />div_datos_solicitud_tramite')._node.style.display = 'none';
	Liferay.fire('showTipoTramiteGMMFormEvent', {hideModal:true} );
	Liferay.fire('ocultarBotonesSolicitudTramiteGeneral',{});
});

Liferay.provide(window, 'tipoTramiteVida', function() {
	A.one('#<portlet:namespace />indicaciones_tramite_vida_modal')._node.style.display = 'block';
	Liferay.fire('hideTipoTramiteGMMFormEvent', {} );
	Liferay.fire('tipoTramiteVidaSucursalMatrizEvent', {} );
	Liferay.fire('cambiaTituloDatosTramiteSolicitud', {titulo:"Solicitud de Tr&aacute;mite de Vida"} );
	Liferay.fire('mostrarBotonesSolicitudTramiteGeneral',{});
});

Liferay.provide(window, 'showModalIndicacionesTramiteGMM', function(titulo) { 
	A.one('#<portlet:namespace />indicaciones_tramite_gmm_modal')._node.style.display = 'block';
	Liferay.fire('cambiaTituloDatosTramiteSolicitud', {titulo:titulo} );
	Liferay.fire('tipoTramiteVidaSucursalNormalEvent', {} );
	Liferay.fire('mostrarBotonesSolicitudTramiteGeneral',{});
});

Liferay.provide(window, 'closeIndicacionesTramiteGMMModal', function() { 
	A.one('#<portlet:namespace />indicaciones_tramite_gmm_modal')._node.style.display = 'none';
});

Liferay.provide(window, 'closeIndicacionesTramiteVidaModal', function() { 
	A.one('#<portlet:namespace />indicaciones_tramite_vida_modal')._node.style.display = 'none';
});

Liferay.provide(window, 'fetchCities', function() {
	var estadoAjax = A.one('#<portlet:namespace />estado_tramite');
	console.log('Estado = ' + estadoAjax.get('value'));
	cargarRegistrosCombo('<%=request.getAttribute("municipiosSourceURL")%>' + estadoAjax.get('value'),
		function(event, id, obj) {
			var ciudadAjax = A.one('#<portlet:namespace />municipio_tramite');
		   	console.log(this.get('responseData'));
		    var cities = this.get('responseData').rows.ciudad;
		   	ciudadAjax.html("");
		   	ciudadAjax.append("<option value=''>Seleccione una Delegacion/Municipio *</option>");
		    for (var j=0; j < cities.length; j++) {
		     	ciudadAjax.append("<option value='" + cities[j].codMunicipio + "'>" + cities[j].descMunicipio + "</option>");
		    }
		    console.log("Carga de ciudades completa");
		});
});


/////////////////////////////////////////////////////////
///////////////////Declaracion eventos///////////////////
/////////////////////////////////////////////////////////

Liferay.on('validaDatosTramiteSolicitud', function(event){
	console.log('Datos Tramite Solicitud Liferay on');
	var formValidator = Liferay.Form.get('<portlet:namespace />fm_datos_tramite_solicitud').formValidator;
	var eventoRespuesta = {};
	if(formValidator) {
		formValidator.validate();
		console.log('Datos Solicitud Tramite  Liferay on');
		eventoRespuesta.valido = !formValidator.hasErrors();
		console.log('validaDatosTramiteSolicitud eventoRespuesta.valido = ' + eventoRespuesta.valido);
		if (eventoRespuesta.valido) {
			var estado = A.one('#<portlet:namespace />estado_tramite').get('value');
			var municipio = A.one('#<portlet:namespace />municipio_tramite').get('value');
			var sucursal = A.one('#<portlet:namespace />sucursal_tramite').get('value');
			var tipoTramiteChecked = A.one('#<portlet:namespace />tipoTramiteGMMReembolso').get('checked') ? 'Reembolso' : 'CirProgramada';
			var tipoTramiteChecked = A.one('#<portlet:namespace />radioTipoTramiteGMM').get('checked') ? 'GMM' : 'Vida';
			eventoRespuesta.tipoTramite = tipoTramiteChecked;
			eventoRespuesta.tipoTramiteGMM = tipoTramiteChecked;
			eventoRespuesta.estado = estado;
			eventoRespuesta.municipio = municipio;
			eventoRespuesta.sucursal = sucursal;
		}	
	} else {
		eventoRespuesta.valido = false;
	}
	eventoRespuesta.nombreSeccion = "DatosTramiteSolicitud";
	Liferay.fire('validaDatosTramiteSolicitudRespuesta', eventoRespuesta );
});

Liferay.on('ocultarDivDatosTramiteSolicitud', function(event){
	A.one('#<portlet:namespace/>div_datos_tramite_solicitud')._node.style.display = 'none';
});

Liferay.on('mostrarDivDatosTramiteSolicitud', function(event){
	A.one('#<portlet:namespace/>div_datos_tramite_solicitud')._node.style.display = 'block';
});

Liferay.on('showTipoTramiteGMMFormEvent',function(event){ 
	A.one('#<portlet:namespace />tipoTramiteGMMReembolso').set('checked', false);
	A.one('#<portlet:namespace />tipoTramiteGMMCirugia').set('checked', false);
	
	A.one('#<portlet:namespace />div_tipo_tramite_gmm')._node.style.display = 'block';
	//A.one('#<portlet:namespace />div_tipo_tramite_gmm')._node.className = 'col-md-12 text-center';
	if (!event.hideModal){
		showModalIndicacionesTramiteGMM();
	}
	console.log("Saliendo del evento por tipo de tramite GMM");
});


Liferay.on('tipoTramiteVidaSucursalNormalEvent', function(event){
	A.one('#<portlet:namespace />div_datos_solicitud_tramite')._node.style.display = 'block';
	A.one('#<portlet:namespace />sucursal_tramite').set('selectedIndex',0);
	A.one('#<portlet:namespace />sucursal_tramite').set('disabled', false);
	Liferay.fire('tipoTramiteShowButtonEvent', {} );
});

Liferay.on('tipoTramiteVidaSucursalMatrizEvent', function(event){
	A.one('#<portlet:namespace />div_datos_solicitud_tramite')._node.style.display = 'block';
	A.one('#<portlet:namespace />sucursal_tramite').set('selectedIndex',2);
	A.one('#<portlet:namespace />sucursal_tramite').set('disabled', true);
	Liferay.fire('tipoTramiteShowButtonEvent', {} );

});

Liferay.on('cambiaTituloDatosTramiteSolicitud', function(event){
	var tituloA = A.one('#<portlet:namespace />titulo_datos_tramite')._node;
	tituloA.innerHTML = '';
	tituloA.innerHTML = '<p class="formularioTituloEncabezado titulotop">' + event.titulo + '</p>';
});

Liferay.on('hideTipoTramiteGMMFormEvent',function(event){ 
	console.log("on hideTipoTramiteGMMFormEvent");
	A.one('#<portlet:namespace />div_tipo_tramite_gmm')._node.style.display = 'none';
	console.log("Saliendo de hideTipoTramiteGMMFormEvent");
});


/////////////////////////////////////////////////////////
///////////////////Cargas Iniciales//////////////////////
/////////////////////////////////////////////////////////

console.log("Cargando estados" );
cargarRegistrosCombo("<%=request.getAttribute("estadosSourceURL")%>",
	function(event, id, obj) {
		console.log(this.get('responseData'));
	    var states = this.get('responseData').rows.estado;
	    var estadoAjax = A.one('#<portlet:namespace />estado_tramite');
	    estadoAjax.html("");
	    estadoAjax.append("<option value=''>Seleccione un Estado *</option>");
	    for (var j=0; j < states.length; j++) {
	    	estadoAjax.append( '<option value="' + states[j].codestado + '">' + states[j].descestado + '</option>');
	    }
	    console.log("Carga de estados completa");
 });

var tipoTramiteVida = A.one('#<portlet:namespace />tipoTramiteVida');
var tipoTramiteChecked = '';
if (tipoTramiteVida.get('checked')) {
	tipoTramiteChecked = 'Vida';
	console.log('show modal Vida');
	A.one('#<portlet:namespace />indicaciones_tramite_vida_modal')._node.style.display = 'block';
}
var radioTipoTramiteGMM = A.one('#<portlet:namespace />radioTipoTramiteGMM');
if (radioTipoTramiteGMM.get('checked')) {
	tipoTramiteChecked = 'GMM';
	console.log('show modal GMM');
}

</aui:script>
