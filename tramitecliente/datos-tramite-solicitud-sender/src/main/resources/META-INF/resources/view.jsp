<%@ include file="/init.jsp" %>
<%@page import="mx.com.allianz.tramite.datos.sender.exception.IncosistenciaDatosException"%>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="mx.com.allianz.commons.dto.tramite.TramiteDto" %>
<%@ page import="mx.com.allianz.tramite.datos.sender.portlet.DatosTramiteSenderPortlet" %>
<% 

		String currentPageURL = "/tramite-junto";
        Object object = request.getAttribute("currentPageURL");
		if (object != null && object instanceof String){
			currentPageURL = (String) object;
		}
		
		String tramite = null;
        object = request.getAttribute(DatosTramiteSenderPortlet.LLAVE_TRAMITE);
		if (object != null && object instanceof String){
			tramite = (String) object;
		}
		
		int maxNumArchivos = 10;
		object = request.getAttribute(DatosTramiteSenderPortlet.LLAVE_MAX_DOCS);
		if (object != null && object instanceof Integer){
			maxNumArchivos = (Integer) object;
		}
		
%>

<portlet:resourceURL var="uploadResourceURL" >
</portlet:resourceURL>

<portlet:resourceURL var="captchaURL" id="captchaURL">
    <portlet:param name="captchaActionType" value="loadCaptchaURLImage"/>
    <portlet:param name="cmd" value="capcha"/>
</portlet:resourceURL>

<liferay-ui:error exception="<%= IncosistenciaDatosException.class %>" message="mx.com.allianz.tramites.datos.sender.portlet.inconsistencia.error.message" />
<portlet:actionURL var="tramiteActionURL" name="processAction">
</portlet:actionURL>

<div id="<portlet:namespace/>div_datos_general_tramite_sender" style="display:none;">
	<aui:form name="fm-tramite-grl" method="post" enctype="multipart/form-data">
		<aui:input name="version-datos-tramite-solicitud-sender" type="hidden" value="1.0.0"  />
		<aui:input name="paginaActual" type="hidden" value="1"  />
		<aui:input name="factorNavegacion" type="hidden" value="1"  />
		<aui:input name="cmd" type="hidden" value="upload"  />
		<div id="<portlet:namespace/>seccion_archivos" style="display:none;">
			
			<% for (int i = 0;i < maxNumArchivos; i++){ %>
			<div id="<portlet:namespace/>div_archivos<%= i %>" style="display:<%=i == 0 ? "block" : "none"%>;">
				
				<aui:row>
					<aui:col span="5">
						<aui:input cssClass="formularioCampoTexto" label="" name="documento" type="file" disabled="true">
					    	<aui:validator name="required" errorMessage="El archivo es requerido"/>
					    	<aui:validator name="acceptFiles" >'doc,docx,xls,xlsx,ppt,pptx,pdf,jpg,jpeg,png,tif,gif'</aui:validator>
					    </aui:input>
						        
					</aui:col>
						    
					<aui:col span="5">
						<aui:select cssClass="formularioCampoTexto" label="" name="documentos"  errorMessage="El tipo de documento es requerido" disabled="true">
							<aui:option selected="true" value="">
						        Seleccione un tipo de documento
						    </aui:option>
						</aui:select>
					</aui:col>
					<aui:col span="1">
						<% if (i == 0 ){ ;%> <a href="javascript:muestraSigDoc();"  id="<portlet:namespace/>boton_agregar"><i class="icon-plus"></i>Agregar</a>
						<% } else { %><a href="javascript:ocultarSeccionDoc(<%= i %>);"  id="<portlet:namespace/>boton_eliminar<%=i%>"><i class="icon-minus"></i>Eliminar</a><%} %>
					</aui:col>
				</aui:row>
				
			</div>
			<%} %>
		    <p class="formularioEtiquetaTexto">Formatos: Word, Excel, Power Point, PDF, JPG, JPEG, PNG, GIF m�ximo 10 MB.</p>
			</br>
		</div>
		
		<div id="<portlet:namespace/>div_capcha" style="display:none;">
			<liferay-ui:captcha url="<%=captchaURL.toString()%>" />
		</div>
    	<div id="<portlet:namespace/>div_botones_tramite_sender">
    	<aui:button-row>
	        	<aui:button name="saveButton" class="btn btn-primary pull-right" type="button" value="datos.tramite.general.sender.next" id="submitBtn" onclick="navegarPagina(1);"/>
	    		<aui:button class="btn pull-right" name="cancelButton" id="cancelButton" type="button" value="datos.tramite.general.sender.clean" onclick="navegarPagina(-1);" />
	    	</aui:button-row>
    	</div>
    	<div id="<portlet:namespace/>div_boton_regreso_tramite_sender" style="display:none">
    		<p class="text-left">Su solicitud ha sido enviada con &eacute;xito 
			</br>En breve recibir&aacute; un correo de confirmaci&oacute;n.
			</br>Gracias.</br>
			</br>En caso de no recibir el e-mail de confirmaci&oacute;n &oacute; si tiene alg&uacute;n comentario acerca del servicio, favor de contactarnos para tr&aacute;mites de GMM al 01 800 1111 400 y 5201 3181 y para tr&aacute;mites de Vida al 5201 3039 &oacute; bien al correo: atencion.quejas@allianz.com.mx 
			</br>
			</br>Atentamente: Allianz M&eacute;xico</br>
			</p>
			<p>
			</p>
	    	<aui:button-row>
	    		<aui:button class="btn btn-primary pull-right" name="resetButton" type="button" value="datos.tramite.general.sender.reset" onclick="currentPageURLJSClean();" />
	    	</aui:button-row>
    	</div>
	</aui:form>
</div>

<script type="text/javascript"> 
console.log("datostramitesender v1.0.0");
var tramite = <%=tramite%>;
var currentPageURLJS = '<%=currentPageURL%>';




</script>

<aui:script use="aui-base,aui-io-request,liferay-form,aui-form-validator">
console.log("Tramite cargado");
console.log(tramite);

Liferay.provide(window, 'muestraSigDoc', function() {
	for (var numDoc = 1; numDoc < <%=maxNumArchivos%>; numDoc ++){
		
		if (A.one("#<portlet:namespace/>div_archivos" + numDoc)._node.style.display == "none"){
			mostrarSeccionTramiteSolicitudPoliza('div_archivos'+numDoc,'block');
			var fileA = A.one("#<portlet:namespace/>div_archivos" + numDoc + " input");
			if ( fileA )
				fileA.removeAttribute("disabled");
			
			fileA = A.one("#<portlet:namespace/>div_archivos" + numDoc + " select");
			if ( fileA )
				fileA.removeAttribute("disabled");
			break;
		}
	}
});

Liferay.provide(window, 'ocultarSeccionDoc', function(numDoc) {
	if (numDoc > 0){
		mostrarSeccionTramiteSolicitudPoliza('div_archivos'+numDoc,'none');
		var fileA = A.one("#<portlet:namespace/>div_archivos" + numDoc + " input");
		if ( fileA )
			fileA.set("disabled","" );
		
		fileA = A.one("#<portlet:namespace/>div_archivos" + numDoc + " select");
		if ( fileA )
			fileA.set("disabled","");
	}else console.log("no deber�a de ocultar seccion");
});

Liferay.provide(window, 'mostrarSeccionTramiteSolicitudPoliza', function(seccionPoliza,mostrado) {
	A.one('#<portlet:namespace/>' +  seccionPoliza)._node.style.display = mostrado;
});

Liferay.provide(window, 'navegarPagina', function(factor) {
	var pagina = A.one('#<portlet:namespace/>paginaActual');
	var val_pagina = 1*pagina.get('value');
	
	A.one('#<portlet:namespace/>factorNavegacion').set("value",factor);
	
	////Aqui van validaciones
	formsValidSectionTramite = [];
	
	switch(val_pagina) {
		case 0:
			console.log("Aqui no deber�a estar, que chingados");
			break;
		case 1:
			if (factor == -1){
				currentPageURLJSClean();
			} else {
				console.log("Valida pagina 1");
				Liferay.fire('validaDatosTramiteSolicitud', {} );
				
			}
			break;
		case 2:
			console.log("Valida pagina 2");
			Liferay.fire('validaDatosAfectadoPoliza', {} );
			Liferay.fire('validaDatosContratante', {} );
	        break;
	    case 3:
	    	console.log("Valida pagina 3");
	    	Liferay.fire('validaDatosSolicitante', {} );
	    	Liferay.fire('validaDatosReclamacion', {} );
	        break;
	    case 4:
	    	console.log("Valida pagina 4");
	    	Liferay.fire('validaDatosComplementoTramite', {} );
	    	break;
	    case 5:
	    	break;
	    default:
	}
	
	
});


Liferay.provide(window, 'configuraPagina', function() {
	var pagina = A.one('#<portlet:namespace/>paginaActual');
	var val_pagina =  pagina.get("value") * 1;
	var tipoTramite = obtenerTipoTramite();
	var factor = A.one('#<portlet:namespace/>factorNavegacion').get("value") * 1;
	console.log("Estaba en pagina: " + val_pagina);
	A.one('#<portlet:namespace/>paginaActual').set( 'value', val_pagina + factor);
	val_pagina = 1*pagina.get('value') ;
	console.log("Ir a pagina: " + val_pagina);
	
	switch(val_pagina) {
		case 0:
			//Aqui va lo de limpiar
			console.log("Pues segun yo ahora limpiaria");
			currentPageURLJSClean();
			break;
		case 1:
			console.log("Despliego pantalla 1");
			A.one('#<portlet:namespace/>cancelButton span').html("Limpiar");
			Liferay.fire('mostrarDivDatosTramiteSolicitud',{});
			Liferay.fire('ocultarDatosAfectadoPoliza',{});
			Liferay.fire('ocultarDatosContratantePoliza',{});
			break;
		case 2:
	    	Liferay.fire('ocultarDivDatosTramiteSolicitud',{});
	    	console.log(tramite);
	    	console.log(tramite.cliente)
	    	Liferay.fire('mostrarDatosAfectadoPoliza',{
	    		cliente : tramite.cliente
	    	});
	    	Liferay.fire('mostrarDatosContratantePoliza',{});
	    	Liferay.fire('ocultarDatosSolicitante',{});
	    	Liferay.fire('ocultarTipoReclamacion',{});
			console.log("Despliego pantalla 2");
			A.one('#<portlet:namespace/>cancelButton span').html("Regresar");
	        break;
	    case 3:
	    	Liferay.fire('ocultarDatosAfectadoPoliza',{});
	    	Liferay.fire('ocultarDatosContratantePoliza',{});
	    	Liferay.fire('mostrarDatosSolicitante',{
	    		cliente : tramite.cliente
	    	});
	    	Liferay.fire('mostrarTipoReclamacion',{
	    		tipoTramite : tipoTramite.tipoTramite
	    	});
	    	Liferay.fire('ocultarDatosComplementoTramite',{});
	    	console.log("Despliego pantalla 3");
	    	A.one('#<portlet:namespace/>submitBtn span').html("Siguiente");
	    	mostrarSeccionTramiteSolicitudPoliza('seccion_archivos','none');
	    	mostrarSeccionTramiteSolicitudPoliza('div_capcha','none');
	        break;
	    case 4:
	    	
	    	Liferay.fire('ocultarDatosSolicitante',{});
	    	Liferay.fire('ocultarTipoReclamacion',{});
	    	Liferay.fire('mostrarDatosComplementoTramite',{
	    		tipoTramite : tipoTramite.tipoTramite,
	    		tipoTramiteGMM : tipoTramite.tipoTramiteGMM
	    	});
	    	Liferay.fire('cargaTipoDocumento',{
	    		tipoTramite : tipoTramite.tipoTramite
	    	});
	    	mostrarSeccionTramiteSolicitudPoliza('seccion_archivos','block');
	    	mostrarSeccionTramiteSolicitudPoliza('div_capcha','block');
	    	
	    	console.log("Despliego pantalla 4");
	    	A.one('#<portlet:namespace/>submitBtn span').html("Enviar");
	    	break;
	    case 5:
	    	for ( var j = 0; j < <%=maxNumArchivos%>; j ++){
	    		var fileA = A.one("#<portlet:namespace/>div_archivos" + j + " input");
	    		if ( fileA ){
	    			fileA.set("name","file_documento" + j);
	    			if ( j === 0)
	    				fileA.removeAttribute("disabled");
	    		}
	    		
	    		fileA = A.one("#<portlet:namespace/>div_archivos" + j + " select");
	    		if ( fileA ){
	    			fileA.set("name","<portlet:namespace/>tipo_documento" + j);
	    			if ( j === 0)
	    				fileA.removeAttribute("disabled");
	    		}
	    	}
	    	
	    	A.io.request("<%= uploadResourceURL %>", {
				form: {
					id:"<portlet:namespace/>fm-tramite-grl",
					upload: true
				},
				dataType:'text',
				on: {
					failure: function() {
	                    console.log("Ajax failed! There was some error at the server");
	                },
	                success: function(event, id, obj) {
	                	console.log("LLego la solicitud en el success");
	                	console.log(event);
	                	console.log(id);
	                	console.log(obj);
	        		   	console.log(this.get('responseData'));
	        		},
	        		complete: function(data){
	                    console.log("Objeto respuesta !!!!!!!!!!!!");
	                    console.log(data);
	                    console.log("Objeto respuesta !!!!!!!!!!!!");
		        		console.log("Despliego Confirmacion");
						mostrarSeccionTramiteSolicitudPoliza('div_boton_regreso_tramite_sender','block');
	                }
	            }
			});
   		    mostrarSeccionTramiteSolicitudPoliza('div_botones_tramite_sender','none');
   		    mostrarSeccionTramiteSolicitudPoliza('seccion_archivos','none');
   		 	mostrarSeccionTramiteSolicitudPoliza('div_capcha','none');
   		    Liferay.fire('ocultarDatosComplementoTramite',{});
   		    console.log("Oculto ultima p�gina");
	    	break;
	    	
	    default:
	    	var pagina = A.one('#<portlet:namespace/>paginaActual');
	    	pagina.set( 'value', val_pagina < 1 ? 1: (val_pagina > 4 ? 4:-1));
	    	
	    	console.log("fuera del rango: se pone el contador en " + pagina.get( 'value'));
	}
});

Liferay.provide(window, 'currentPageURLJSClean', function() {
	console.log('currentPageURLJSClean');
	console.log(currentPageURLJS);
	var redir = currentPageURLJS;
	window.location.href = redir;

});


Liferay.provide(window, 'agregaHidden', function(nombre, valor, formGrl) {
	var hiddenA = A.one("#<portlet:namespace />" + nombre );
		
	if ( hiddenA ) 
		if ( valor )  
			hiddenA.set("value",valor );
		else hiddenA.remove();
	else if ( valor )
		formGrl.append('<input type="hidden" name="<portlet:namespace />' + nombre 
				+ '" id="<portlet:namespace />' + nombre + '" value="' + valor + '"/>');
	else console.log("No existia tag y no tenia valor");
});

function validaSeccionTramiteDoble(respuestaSeccion) { 
	console.log("desde la respesta la seccion es" + respuestaSeccion.valido);
	formsValidSectionTramite.push(respuestaSeccion.valido);
	console.log( formsValidSectionTramite );
	Liferay.fire('cargarDatosAFormulario',respuestaSeccion);
	if(formsValidSectionTramite.length % 2 === 0) {
		var factor = A.one('#<portlet:namespace/>factorNavegacion').get("value") * 1;
		if ( (factor === -1 ) || (formsValidSectionTramite[0]
				&& formsValidSectionTramite[1])){
			configuraPagina();
		}
	}else {
		console.log("Comportamiento desafortunado");
	}
}

function validaSeccionSimple(respuestaSeccion) { 
	console.log("desde la respesta la seccion simple es" + respuestaSeccion.valido);
	var factor = A.one('#<portlet:namespace/>factorNavegacion').get("value") * 1;
	if ( (factor === -1 ) || respuestaSeccion.valido ){
		Liferay.fire('cargarDatosAFormulario',respuestaSeccion);
		configuraPagina();
	}
	
}

function validaSeccionFinal(respuestaSeccion) { 
	console.log("desde la respesta la seccion final ");
	var factor = A.one('#<portlet:namespace/>factorNavegacion').get("value") * 1;
	var formValidator = Liferay.Form.get('<portlet:namespace />fm-tramite-grl').formValidator;
	formValidator.validate();
	if ( factor === 1 ){
		Liferay.fire('cargarDatosAFormulario',respuestaSeccion);
		if ( !formValidator.hasErrors() && respuestaSeccion.valido ){
			var captcha = A.one('#<portlet:namespace />captchaText') ;
			var captchaTextValue = captcha ? captcha.get('value'): 'NA';
// 			eventoRespuesta.valido = !formValidator.hasErrors();//('NA' != captchaTextValue) ? (!formValidator.hasErrors()) : true;
// 			console.log('validaCaptcha eventoRespuesta.valido = ' + eventoRespuesta.valido);
			console.log('validaCaptcha captchaTextValue = ' + captchaTextValue);
			console.log('(NA != captchaTextValue) = ' + ('NA' != captchaTextValue));

			if('NA' != captchaTextValue) {
				//eventoRespuesta.catpcha = '2345';
				//Liferay.fire('validaCaptchaRespuesta', eventoRespuesta );
				console.log('validaCaptcha captcha = ' + captcha);

// 				eventoRespuesta.catpcha = ('NA' != captchaTextValue) ? captchaTextValue : responseReCaptcha;
				console.log('captchaTextValue captcha = ' + captchaTextValue);

				configuraPagina();
				$.post('<%= captchaURL.toString() %>', {
		        		captchaTextValue: captchaTextValue,
		        		gRecaptchaResponse: responseReCaptcha
	        	}).always(function(data){
	        	console.log("always func");
		        var stringData = JSON.stringify(data.responseText) + "";
		        console.log('respuesta final captcha = ' + ((stringData.indexOf('true') !== -1))) ;
				});

			} else {
				var responseReCaptcha = grecaptcha ? grecaptcha.getResponse() : 'NA';
				console.log(responseReCaptcha);
				console.log("Respuesta de recaptcha -> responseReCaptcha  = " + responseReCaptcha);
				if ( responseReCaptcha != "" ){
					configuraPagina();
				} else {
					console.log("Sin seleccionar captcha");
				}
			}
		}
	} else {
		configuraPagina();
	}
}

Liferay.on('validaDatosTramiteSolicitudRespuesta', validaSeccionSimple);
Liferay.on('validaDatosAfectadoPolizaRespuesta', validaSeccionTramiteDoble);
Liferay.on('validaDatosContratanteRespuesta', validaSeccionTramiteDoble);
Liferay.on('validaDatosSolicitanteRespuesta', validaSeccionTramiteDoble);
Liferay.on('validaDatosReclamacionRespuesta', validaSeccionTramiteDoble);
Liferay.on('validaDatosComplementoTramiteRespuesta', validaSeccionFinal);

Liferay.on("cargarDatosAFormulario",function(event){
	var formGrl = A.one('#<portlet:namespace/>fm-tramite-grl');
	switch (event.nombreSeccion){
		case 'DatosTramiteSolicitud':
			agregaHidden("tipoTramite",event.tipoTramite , formGrl);
			agregaHidden("tipoTramiteGMM",event.tipoTramiteGMM , formGrl);
			agregaHidden("estado",event.estado , formGrl);
			agregaHidden("municipio",event.municipio , formGrl);
			agregaHidden("sucursal",event.sucursal , formGrl);
			break;
		case 'DatosAfectadoPoliza':
			agregaHidden("idAfectado",event.idAfectado , formGrl);
			agregaHidden("nombreAfectado",event.nombreAfectado , formGrl);
			agregaHidden("apellidoPaternoAfectado",event.apellidoPaternoAfectado , formGrl);
			agregaHidden("apellidoMaternoAfectado",event.apellidoMaternoAfectado , formGrl);
			agregaHidden("producto",event.producto , formGrl);
			agregaHidden("numeroPoliza",event.numeroPoliza , formGrl);
			break;
		case 'DatosContratante':
			agregaHidden("nombreContratante",event.nombreContratante , formGrl);
			agregaHidden("apellidoPaternoContratante",event.apellidoPaternoContratante , formGrl);
			agregaHidden("apellidoMaternoContratante",event.apellidoMaternoContratante , formGrl);
			break;
		case 'DatosSolicitante':
			agregaHidden("nombreSolicitante",event.nombreSolicitante , formGrl);
			agregaHidden("apellidoPaternoSolicitante",event.apellidoPaternoSolicitante , formGrl);
			agregaHidden("apellidoMaternoSolicitante",event.apellidoMaternoSolicitante , formGrl);
			agregaHidden("telefonoParticular",event.telefonoParticular , formGrl);
			agregaHidden("telefonoCelular",event.telefonoCelular , formGrl);
			agregaHidden("email",event.email , formGrl);
			break;
		case 'DatosReclamacion':
			agregaHidden("tipoSiniestro",event.tipoSiniestro , formGrl);
			agregaHidden("numeroSiniestro",event.numeroSiniestro , formGrl);
			agregaHidden("tipoReclamacion",event.tipoReclamacion , formGrl);
			if ( event.tipoReclamacion )
				agregaHidden("tipoReclamacionDescripcion",event.tipoReclamacionDescripcion , formGrl);
			agregaHidden("observaciones",event.observaciones , formGrl);
			break;
		case 'DatosComplementoTramite':
			agregaHidden("estadoRespuesta",event.estadoRespuesta , formGrl);//checar nombre en formulario enum
			agregaHidden("subgrupo",event.subgrupo , formGrl);
			agregaHidden("subgrupoDescripcion",event.subgrupoDescripcion , formGrl);
			agregaHidden("grupoDescripcion",event.grupoDescripcion , formGrl);
			agregaHidden("grupo",event.grupo , formGrl);
			agregaHidden("moneda",event.moneda , formGrl);
			agregaHidden("monto",event.monto , formGrl);
			agregaHidden("cantidadFacturaRecibos",event.cantidadFacturaRecibos , formGrl);
			break;
		default:
			console.log("No se detect� correctamente la seccion de procedencia");
	}
});

Liferay.on("cargaTipoDocumento",function(event){
	A.io.request ("<%= uploadResourceURL %>", {
	        	method: 'get',
	         	dataType: 'json',
	         	headers: {
	                'Accept': 'application/json'
				},
				data:{
					"<portlet:namespace />cmd" : "<%=DatosTramiteSenderPortlet.CMD_DOCUMENTOS%>",
					"<portlet:namespace />tipoTramite" : event.tipoTramite
				},
		        on: {
	                failure: function() {
	                    console.log("Ajax failed! There was some error at the server");
	                },
	                success: function(event, id, obj) {
	        		   	console.log(this.get('responseData'));
	        		    var docs = this.get('responseData');
	        		    var documentoAjax;
	        		    var documentoVal;
	        		    for (var i = 0; i < <%=maxNumArchivos%>; i ++){
	        		    	documentoAjax = A.one("#<portlet:namespace />div_archivos" + i + " select");
	        		    	documentoVal = documentoAjax.get("value");
	        		    	documentoAjax.html("");
	        		    	
	        		    	documentoAjax.append("<option value=''>Seleccione un Documento</option>");
		        		    for (var j=0; j < docs.length; j++) {
		        		    	documentoAjax.append('<option value="' + docs[j].codigo + '"'+ ( documentoVal == docs[j].codigo ? ' selected="true">' : '>' ) + docs[j].descripcion + "</option>");
		        		    }
	        		    	
	        		    }
	        		    console.log("Carga de documentos completa");
	        		}
		        }
		});
});

Liferay.on("mostrarBotonesSolicitudTramiteGeneral",function(event){
	mostrarSeccionTramiteSolicitudPoliza('div_datos_general_tramite_sender','block');
});

Liferay.on("ocultarBotonesSolicitudTramiteGeneral",function(event){
	mostrarSeccionTramiteSolicitudPoliza('div_datos_general_tramite_sender','none');
});

A.one("#<portlet:namespace />div_archivos0 select").removeAttribute("disabled");
A.one("#<portlet:namespace />div_archivos0 input").removeAttribute("disabled");
console.log("Termin� de cargar el sender 3");

</aui:script>