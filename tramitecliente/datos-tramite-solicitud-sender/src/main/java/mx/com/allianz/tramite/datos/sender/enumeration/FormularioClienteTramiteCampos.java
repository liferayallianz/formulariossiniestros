package mx.com.allianz.tramite.datos.sender.enumeration;

import static javax.portlet.PortletSession.APPLICATION_SCOPE;
import static mx.com.allianz.commons.constants.FormularioConstants.EMPTY;

import java.lang.reflect.Field;
import java.util.Optional;
import java.util.function.Predicate;

import javax.portlet.PortletSession;
import javax.portlet.ResourceRequest;

import com.liferay.portal.kernel.util.ParamUtil;

import mx.com.allianz.commons.dto.tramite.TramiteDto;

public enum FormularioClienteTramiteCampos {
	PRODUCTO("producto"), NUMERO_POLIZA("numeroPoliza"), ID_AFECTADO("idAfectado"),
	NOMBRE_AFECTADO("nombreAfectado"), NOMBRE_CONTRATATANTE("nombreContratante"),
	APELLIDO_PATERNO_AFECTADO("apellidoPaternoAfectado"), APELLIDO_MATERNO_AFECTADO("apellidoMaternoAfectado"), 
	APELLIDO_PATERNO_CONTRATANTE("apellidoPaternoContratante"), APELLIDO_MATERNO_CONTRATANTE("apellidoMaternoContratante");
	
	
	FormularioClienteTramiteCampos(String valor){
		 try {
	            Field fieldName = getClass().getSuperclass().getDeclaredField("name");
	            fieldName.setAccessible(true);
	            fieldName.set(this, valor);
	            fieldName.setAccessible(false);
	        } catch (Exception e) {}
	}
	
	public void buildTramite(TramiteDto tramite, String parametro){
		switch (this) {
		case PRODUCTO:
			tramite.setProducto(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case NUMERO_POLIZA:
			Optional.ofNullable(parametro).filter(NOT_EMPTY).ifPresent(value -> tramite.setNumeroPoliza(Optional.ofNullable(parametro).orElse(EMPTY)));
			tramite.setNumeroPoliza(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case ID_AFECTADO:
			Optional.ofNullable(parametro).filter(NOT_EMPTY).filter(string -> !"0".equals(string))
			.ifPresent(value -> tramite.setIdAfectado(Integer.valueOf(parametro)));
			break;
		case NOMBRE_AFECTADO:
			tramite.setNombreAfectado(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case APELLIDO_PATERNO_AFECTADO:
			tramite.setApellidoPaternoAfectado(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case APELLIDO_MATERNO_AFECTADO:
			tramite.setApellidoMaternoAfectado(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case NOMBRE_CONTRATATANTE:
			tramite.setNombreContratante(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case APELLIDO_PATERNO_CONTRATANTE:
			tramite.setApellidoPaternoContratante(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case APELLIDO_MATERNO_CONTRATANTE:
			tramite.setApellidoMaternoContratante(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		default:
			break;
		}
	}
	
	
	public void uploadToSession(PortletSession session, String parametro){
		Optional.ofNullable(parametro).filter(NOT_EMPTY).ifPresent(value -> session.setAttribute(this.name(), value, APPLICATION_SCOPE));
	}
	
	public String getParamFromRequest(ResourceRequest resourceRequest){
		return Optional.ofNullable(ParamUtil.getString(resourceRequest, this.name())).map(this::tryString).orElse(EMPTY);
	}
	
	public String getParamFromSession(PortletSession session){
		return Optional.ofNullable(session.getAttribute(this.name(), APPLICATION_SCOPE)).map(this::tryString).orElse(EMPTY);
	}
	
	public Integer tryParseInt(String text) {
			return (text.matches("\\d+")) ? Integer.parseInt(text) : null;
	}
	
	public String tryString(Object param) {
		 return (param instanceof String) ? (String) param : null;
	}
	
	public static final Predicate<String> NOT_EMPTY = (String it) -> !it.isEmpty();


}
