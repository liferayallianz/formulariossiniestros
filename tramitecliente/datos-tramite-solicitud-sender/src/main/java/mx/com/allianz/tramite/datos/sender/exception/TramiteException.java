package mx.com.allianz.tramite.datos.sender.exception;

public class TramiteException extends Exception {

	private static final long serialVersionUID = 8794371220217226163L;

	public TramiteException() {
		super();
	}

	public TramiteException(String message) {
		super(message);
	}

	public TramiteException(Throwable cause) {
        super(cause);
    }
}