package mx.com.allianz.tramite.datos.sender.config;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(
		category = "Productivity",
		scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE
	)
	@Meta.OCD(
		localization = "content/Language",
		name = "Configuracion Tramite General",
		id = "mx.com.allianz.tramites.datos.sender.portlet.configuration.TramiteGeneralSenderPortletConfiguration"
	)
public interface TramiteGeneralSenderPortletConfiguration {
	
	@Meta.AD(
			deflt = "false", 
			description = "Activa configuracion de Clientes a modulo de Tramites",
			required = false
		)
	public Boolean enableClientConfiguration();
	
	@Meta.AD(
			deflt = "azctToken", 
			description = "Token con llave de cliente",
			required = false
		)
	public String allianzClientToken();
	
	@Meta.AD(
			deflt = "10", 
			description = "M&aacute;ximo n&uacute;mero de documentos",
			required = false
		)
	public Integer maxNumDocumentos();

	@Meta.AD(
			deflt = "5000", 
			description = "Segundos de validacion para repeticion de peticiones",
			required = false
		)
	public Long waitRequest();
	
	@Meta.AD(
			deflt = "false", 
			description = "Activa configuracion de Capcha",
			required = false
		)
	public Boolean enableCapcha();
	
}
