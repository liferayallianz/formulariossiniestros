package mx.com.allianz.tramite.datos.sender.constants;

/**
 * @author sfrancof
 */
public class DatosTramiteSenderPortletKeys {

	public static final String DatosTramiteSender = "DatosTramiteSender";

}