package mx.com.allianz.tramite.datos.sender.enumeration;

import static javax.portlet.PortletSession.APPLICATION_SCOPE;
import static mx.com.allianz.commons.constants.FormularioConstants.EMPTY;

import java.lang.reflect.Field;
import java.util.Optional;
import java.util.function.Predicate;

import javax.portlet.PortletSession;
import javax.portlet.ResourceRequest;

import com.liferay.portal.kernel.util.ParamUtil;

import mx.com.allianz.commons.dto.tramite.TramiteDto;

public enum FormularioSolicitanteTramiteCampos {

	NOMBRE_SOLICITANTE("nombreSolicitante"), APELLIDO_PATERNO_SOLICITANTE("apellidoPaternoSolicitante"), APELLIDO_MATERNO_SOLICITANTE("apellidoMaternoSolicitante"),
	TELEFONO_PARTICULAR("telefonoParticular"),TELEFONO_CELULAR("telefonoCelular"), EMAIL("email"), TIPO_RECLAMACION("tipoReclamacion"),TIPO_RECLAMACION_DESCRIPCION("tipoReclamacionDescripcion"),
	NUMERO_SINIESTRO("numeroSiniestro"),TIPO_SINIESTRO("tipoSiniestro"), OBSERVACIONES("observaciones");
	
	FormularioSolicitanteTramiteCampos(String valor){
		 try {
	            Field fieldName = getClass().getSuperclass().getDeclaredField("name");
	            fieldName.setAccessible(true);
	            fieldName.set(this, valor);
	            fieldName.setAccessible(false);
	        } catch (Exception e) {}
	}
	
	public void buildTramite(TramiteDto tramite, String parametro){
		switch (this) {
		
		case NOMBRE_SOLICITANTE:
			tramite.setNombreSolicitante(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case APELLIDO_PATERNO_SOLICITANTE:
			tramite.setApellidoPaternoSolicitante(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case APELLIDO_MATERNO_SOLICITANTE:
			tramite.setApellidoMaternoSolicitante(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case TELEFONO_PARTICULAR:
			tramite.setTelefonoParticular(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case TELEFONO_CELULAR:
			tramite.setTelefonoCelular(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case EMAIL:
			tramite.setEmail(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case TIPO_RECLAMACION:
			tramite.setTipoDeReclamacion(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case TIPO_RECLAMACION_DESCRIPCION:
			tramite.setTipoDeReclamacionDescripcion(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case NUMERO_SINIESTRO:
			tramite.setNumeroSiniestro(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case TIPO_SINIESTRO: 
			tramite.setTipoDeSiniestro(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case OBSERVACIONES:
			tramite.setObservaciones(Optional.ofNullable(parametro).orElse(EMPTY));
		default:
			break;
		}
	}
	
	
	
	public void uploadToSession(PortletSession session, String parametro){
		Optional.ofNullable(parametro).filter(NOT_EMPTY).ifPresent(value -> session.setAttribute(this.name(), value, APPLICATION_SCOPE));
	}
	
	public String getParamFromRequest(ResourceRequest resourceRequest){
		return Optional.ofNullable(ParamUtil.getString(resourceRequest, this.name())).map(this::tryString).orElse(EMPTY);
	}
	
	public String getParamFromSession(PortletSession session){
		return Optional.ofNullable(session.getAttribute(this.name(), APPLICATION_SCOPE)).map(this::tryString).orElse(EMPTY);
	}
	
	public Integer tryParseInt(String text) {
			return (text.matches("\\d+")) ? Integer.parseInt(text) : null;
	}
	
	public String tryString(Object param) {
		 return (param instanceof String) ? (String) param : null;
	}
	
	public static final Predicate<String> NOT_EMPTY = (String it) -> !it.isEmpty();

	
	
}
