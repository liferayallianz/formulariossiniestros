package mx.com.allianz.tramite.datos.sender.portlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.xml.ws.WebServiceException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.google.gson.Gson;
import com.liferay.portal.kernel.captcha.CaptchaTextException;
import com.liferay.portal.kernel.captcha.CaptchaUtil;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;

import aQute.bnd.annotation.metatype.Configurable;
import mx.com.allianz.client.puc.tramites.api.PucTramitesServiceClient;
import mx.com.allianz.client.puc.tramites.api.exception.PucTramitesServiceClientException;
import mx.com.allianz.commons.catalogos.dto.DocumentoDTO;
import mx.com.allianz.commons.dto.puc.PucTramiteRespuestaRestDTO;
import mx.com.allianz.commons.dto.tramite.TramiteDto;
import mx.com.allianz.module.service.facade.AllianzService;
import mx.com.allianz.service.catalogos.service.DocumentoLocalServiceUtil;
import mx.com.allianz.tramite.datos.sender.config.TramiteGeneralSenderPortletConfiguration;
import mx.com.allianz.tramite.datos.sender.constants.DatosTramiteSenderPortletKeys;
import mx.com.allianz.tramite.datos.sender.enumeration.FormularioClienteTramiteCampos;
import mx.com.allianz.tramite.datos.sender.enumeration.FormularioComplementoTramiteCampos;
import mx.com.allianz.tramite.datos.sender.enumeration.FormularioSolicitanteTramiteCampos;
import mx.com.allianz.tramite.datos.sender.enumeration.FormularioTramiteCampos;
import mx.com.allianz.tramite.datos.sender.exception.TramiteWSException;

/**
 * @author sfrancof
 */
@Component(
	configurationPid = "mx.com.allianz.tramite.datos.sender.config.TramiteGeneralSenderPortletConfiguration",
	immediate = true,
	property = {
		"com.liferay.portlet.header-portlet-css=css/liferayPortletForms.css",
		"com.liferay.portlet.display-category=Tramite",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=datos-tramite-solicitud-sender Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + DatosTramiteSenderPortletKeys.DatosTramiteSender,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,guest"
	},
	service = Portlet.class
)
public class DatosTramiteSenderPortlet extends MVCPortlet {
	
	
	public static final String LLAVE_CURRENT_PAGE_URL = "currentPageURL"; 
	public static final String LLAVE_MAX_DOCS = "maxNumArchivos"; 
	public static final String LLAVE_TRAMITE = "tramite"; 
	public static final String LLAVE_TRAMITE_EVENT = "clienteTramiteEvent"; 
	public static final String LLAVE_CLIENTE_FIRMADO = "clienteFirmado"; 
	public static final String LLAVE_CLIENTE_FIRMADO_EVENT = "clienteFirmadoEvent"; 
	public static final String LLAVE_CLIENTE_HABILITADO = "tramiteClienteHabilitado"; 
	public static final String LLAVE_CLIENTE_HABILITADO_EVENT = "tramiteClienteHabilitadoEvent"; 
	
	public static final String LLAVE_DATOS_TRAMITE_EVENT = "datosTramiteEvent";
	public static final String CMD_UPLOAD = "upload";
	public static final String CMD_DOCUMENTOS = "docs";
	public static final String CMD_CAPCHA = "capcha";
	
	public static final String LLAVE_INCONSISTENCIA_EVENT = "inconsistenciaTramiteEvent";
	public static final String LLAVE_INCONSISTENCIA_ERROR = "inconsistenciaTramiteError";
	public static final String CONSULTA_EXITOSA_CLIENTE = "\"estatus\":\"true\""; 

	
	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws IOException, PortletException {

		PortletSession session = request.getPortletSession();			
		Gson gson = new Gson();

//		Optional.ofNullable(session
//					.getAttribute(LLAVE_INCONSISTENCIA_ERROR, PortletSession.APPLICATION_SCOPE)
//				)
//				.map(error -> {
//					session.setAttribute(LLAVE_INCONSISTENCIA_ERROR, PortletSession.APPLICATION_SCOPE);
//					return error;
//				})
//				.filter(p -> p instanceof String)
//				.map( p -> (String) p)
//				.ifPresent(message -> 
//					SessionErrors.add(request, IncosistenciaDatosException.class, 
//							new IncosistenciaDatosException("Hubo un error al registras su tramite"))
//				);
		_log.info("Tramite - Sender - isClienteEnabled = " + isClientEnabled());

		TramiteDto tramite = new TramiteDto();
		tramite.setTramiteClienteHabilitado(isClientEnabled());
		if (isClientEnabled()) {
			_log.info("allianzService = " + allianzService);
			if (allianzService != null) {
				_log.info("El servicio de Allianz se seteo correctamente");
				
//				Arrays.stream(Optional.ofNullable(request.getCookies())
//									  			.orElseGet(() -> new Cookie[0]))
//								.filter(cookie -> allianzClientToken().equals(cookie.getName()))
//								.map(Cookie::getValue)
//								.peek(clienteToken -> _log.info("El token con id cliente es: " + clienteToken))
//								.findAny()
//								.ifPresent(cookie -> {
//									_log.info("Cookie a pata  = muDrUM0UNj2mGdfjjcobtzgtijjJ++mahBgY/9PC2Ypn+PfPOjUAPoItR1tvBtDmzTipU121Tu/63vZuQGhB7c8suZnFXiCsMBQ8NLoMWD0=");
//									Optional.ofNullable(allianzService.obtenerDatosTestingZone("muDrUM0UNj2mGdfjjcobt8ck6jbxI2paXVTA1hQY+TRroMUOoqEi6NWZEWsH50Hbwdp/PcSEaH9jzQjWxtRQe9M9ONfm5MT37jflcs1DDC0="))
//											.map(clienteBuscado -> {
//												_log.info("DatosTramiteSenderPortlet -> render -> clienteBuscado = " + clienteBuscado);
//												return clienteBuscado;
//											})
//											.filter(c -> c.contains(CONSULTA_EXITOSA_CLIENTE))
//											.map(json -> gson.fromJson(json, ClienteTramiteDTO.class))
//											.ifPresent(cliente -> {
//												session.setAttribute(LLAVE_CLIENTE_FIRMADO, 
//														gson.toJson(cliente.getCliente()), PortletSession.APPLICATION_SCOPE);
//												tramite.setClienteFirmado(cliente);
//												tramite.fillClient();
//											});
//								});

			} else {
				_log.info("Reiniciar modulo mx.com.allianz.module.service.facade ");
				_log.info("$ telnet localhost 11311 ");
				_log.info("g! lb | grep mx.com.allianz.module.service.facade ");
				_log.info("g! stop PID ");
				_log.info("g! start PID ");
				_log.info("g! disconnect ");
				_log.info("YES ");
			}
		}
		
		_log.info("tramite antes de subir sesion, tramite = " + tramite);
		session.setAttribute(LLAVE_TRAMITE, gson.toJson(tramite), PortletSession.APPLICATION_SCOPE);
		request.setAttribute(LLAVE_TRAMITE, gson.toJson(tramite));
				
		_log.info("DatosTramiteSenderPortlet -> render -> Saliendo ");

		request.setAttribute(LLAVE_MAX_DOCS, _configuration.maxNumDocumentos());
		request.setAttribute(LLAVE_CURRENT_PAGE_URL, "tramites-junto");

		super.render(request, response);
	}
	
	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		_log.info("DatosTramiteSenderPortlet -> serveResource 5");
		
		Gson gson = new Gson();
		PortletSession session = resourceRequest.getPortletSession();
		String tipoTramite;
		JSONArray jsonArray;
		com.liferay.portal.kernel.json.JSONObject jsonMessageObject;
		String cmd = ParamUtil.getString(resourceRequest, Constants.CMD);
		
        PrintWriter writer = resourceResponse.getWriter();
		
		if (!invalidRequest(session)) {
			resourceRequest.getParameterMap().forEach((llave,valor) -> _log.info("llave = " + llave + ", valor = " + Arrays.asList(valor)));
			switch (cmd){
				case CMD_UPLOAD:
					_log.info("Se enviará la peticion del tramite");
					
					TramiteDto tramite = buildTramite(resourceRequest);
					_log.info("tramite subido:"+tramite.getTipoTramite());
					_log.info("Se subieron "+tramite.getDocumentos().size()+" documentos");
					
					enviarTramite(tramite, resourceRequest);
					// TODO Revisar que faltaría para m ensajes de error
					
					jsonMessageObject = 
							JSONFactoryUtil.createJSONObject();
					jsonMessageObject.put("success", true); 
					jsonMessageObject.put("message","Ejecucion correcta");

	        		resourceResponse.setContentType("application/json");
	        		writer.write(jsonMessageObject.toString());
					break;
				case CMD_DOCUMENTOS:
					jsonArray = JSONFactoryUtil.createJSONArray();
					tipoTramite = ParamUtil.getString(resourceRequest, "tipoTramite");
					_log.info("Se cargaran documentos para tipoTramite:"+tipoTramite);
					Optional.ofNullable(DocumentoLocalServiceUtil.getDocumentos(-1,-1))
						.orElseGet(ArrayList::new)
						.stream()
						.filter(documento -> documento.getTipo().equalsIgnoreCase(tipoTramite))
						.map(documento -> new DocumentoDTO(documento.getIdDocumentos(),
								documento.getTipo(), documento.getCodigo(),
								documento.getDescripcion()))
						.sorted(Comparator.comparing(DocumentoDTO::getIdDocumentos))
						.map(gson::toJson)
						.map(DatosTramiteSenderPortlet::parse)
						.filter(Optional::isPresent)
						.map(Optional::get)
						.forEach(jsonArray::put); 
					resourceResponse.setContentType("text/javascript");
					writer.write(jsonArray.toString());
					break;
				case CMD_CAPCHA:
					jsonMessageObject = JSONFactoryUtil.createJSONObject();
					_log.info("Entro a la seccion del capcha 2");
					String parametro = ParamUtil.getString(resourceRequest, "captchaActionType");
						if(parametro !=null && "testCaptchaURL".equals((String)parametro)){
							//Boolean testCaptcha = testCaptcha(resourceRequest);
							jsonMessageObject.put("success", true);//testCaptcha);
							System.out.println("Sin testCapcha jsonMessageObject = " + jsonMessageObject.toJSONString());
							resourceResponse.setContentType("text/javascript");
							resourceResponse.getWriter().write(jsonMessageObject.toJSONString());
						  } else {
							  _log.info("Va a pasar por el serveImage");
							  try {
								  CaptchaUtil.serveImage(resourceRequest, resourceResponse);
							  } catch (Exception e) {
								  if (e instanceof CaptchaTextException //|| e instanceof CaptchaMaxChallengesException 
										  ){
					                  SessionErrors.add(resourceRequest, e.getClass(), e);
					                  System.out.println("no deberia llegar hasta aca pops");
					                  e.printStackTrace();
					            }else{
					                  System.out.println("Captcha verification success::" + e.getMessage());
					            }
							  }
						  }
					break;
				default:
					_log.error("No se puede ejecutar la peticion, no hay cmd");
			}
		} else {
			_log.error("----------Se repitio la peticion------");
		}
	}
	
	public TramiteDto buildTramite(ResourceRequest actionRequest){
		File file;
		List<File> documentos = new ArrayList<>();
		PortletSession session = actionRequest.getPortletSession();
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		final TramiteDto tramite = Optional.ofNullable(session)
				  .map(s -> s.getAttribute("tramite", PortletSession.APPLICATION_SCOPE))
				  .filter(obj -> obj.getClass().equals(TramiteDto.class))
				  .map(obj -> (TramiteDto) obj)
				  .orElseGet(TramiteDto::new);
		
		Arrays.stream(FormularioTramiteCampos.values())
		  .forEach(campo -> 
			  campo.buildTramite(tramite, campo.getParamFromRequest(actionRequest))
		  );
		Arrays.stream(FormularioClienteTramiteCampos.values())
		  .forEach(campo -> 
			  campo.buildTramite(tramite, campo.getParamFromRequest(actionRequest))
		  );
		Arrays.stream(FormularioSolicitanteTramiteCampos.values())
		  .forEach(campo -> 
			  campo.buildTramite(tramite, campo.getParamFromRequest(actionRequest))
		  );
		Arrays.stream(FormularioComplementoTramiteCampos.values())
			  .forEach(campo -> 
				  campo.buildTramite(tramite, campo.getParamFromRequest(actionRequest))
			  );
		if (tramite.getTipoDeReclamacion() == null || "".equals(tramite.getTipoDeReclamacion())){
			tramite.setTipoDeReclamacionDescripcion("");
		}
		for (int i = 0 ; i < _configuration.maxNumDocumentos() ; i++){
			
			file = uploadPortletRequest.getFile("file_documento" + i);
			if ( file != null && file.exists() ){
				documentos.add(file);
			}
		}
		tramite.setDocumentos(documentos);
		
		return tramite;
	}
	
	private TramiteDto enviarTramite(TramiteDto tramite, ResourceRequest request) {
		Integer folioPUC = 0;

		String currentURL = PortalUtil.getCurrentURL(request);
		_log.info("DatosTramiteSenderPortlet -> enviarTramite -> currentURL = " + currentURL);
		
		if (camposObligatoriosInvalidos(tramite)){
//			TODO Revisar opcion de mandar error nueva sin evento
			_log.error("Error en validacion de campos obligatorios");
			_log.error("Error en validacion de campos obligatorios, tramite = " + tramite);
			_log.error("\ntramite.getTipoTramite() = " + tramite.getTipoTramite() +
					  ", tramite.getTipoTramiteGMM() = " + tramite.getTipoTramiteGMM() + 
					  ", tramite.getEstado() = " + tramite.getEstado() + 
					  ", tramite.getMunicipio() = " + tramite.getMunicipio() + 
					  ", tramite.getSucursal() = " + tramite.getSucursal() + 
					  ", tramite.getNombreAfectado() = " + tramite.getNombreAfectado() + 
					  ", tramite.getApellidoPaternoAfectado() = " + tramite.getApellidoPaternoAfectado() + 
					  ", tramite.getNombreContratante() = " + tramite.getNombreContratante() + 
					  ", tramite.getApellidoPaternoContratante() = " + tramite.getApellidoPaternoContratante() + 
					  ", tramite.getNombreSolicitante() = " + tramite.getNombreSolicitante() + 
					  ", tramite.getApellidoPaternoSolicitante() = " + tramite.getApellidoPaternoSolicitante() + 
					  ", tramite.getTelefonoParticular() = " + tramite.getTelefonoParticular() + 
					  ", tramite.getEmail() = " + tramite.getEmail() +
					  ", tramite.getGrupoPago() = " + tramite.getGrupoPago() +
					  ", tramite.getLugarDeEnvio() = " + tramite.getLugarDeEnvio());
			
		} else {
			_log.info("Aqui debería mandar a llamar el servicio puc");
			try {
//				
//				folioPUC = Optional.ofNullable(generarOTTramite(tramite))
//							.map(PucTramiteRespuestaRestDTO::getNumeroFolio)
//							.filter(folio -> folio.matches("\\d+"))
//							.map(Integer::parseInt)
//							.orElse(0);
				
				tramite.setFolioPUC(folioPUC);
				
				
				
			} catch ( //TramiteWS
					Exception e) {
				_log.error("Tramite -> catch e " + e.getMessage());
				_log.error("Tramite -> catch e " + Arrays.stream(e.getStackTrace()).map(StackTraceElement::toString).collect(Collectors.joining("\n")));

				SessionErrors.add(request, e.getClass(), e);
				

			}
		}
		return tramite;
	}
	
	private boolean camposObligatoriosInvalidos(TramiteDto tramite) {

		return Arrays.asList(tramite.getTipoTramite(), tramite.getTipoTramiteGMM(), 
					tramite.getEstado(), tramite.getMunicipio(), tramite.getSucursal(), 
					tramite.getNombreAfectado(), tramite.getApellidoPaternoAfectado(), 
					tramite.getNombreContratante(), tramite.getApellidoPaternoContratante(), 
					tramite.getNombreSolicitante(), tramite.getApellidoPaternoSolicitante(), 
					tramite.getTelefonoParticular(), tramite.getEmail()
				)
				.stream()
				.anyMatch(p -> p == null || p.trim().isEmpty());
		
	}
	
	private PucTramiteRespuestaRestDTO generarOTTramite(TramiteDto tramite) throws TramiteWSException {
		PucTramiteRespuestaRestDTO pucRespuesta = null;
		try {
			pucRespuesta = 
					pucTramitesServiceClient.generarOtTramites(tramite);
		} catch ( PucTramitesServiceClientException | WebServiceException e) {
			_log.error("Error enviando tramite a puc ",e);
			throw new TramiteWSException("Hubo un error al enviar su tramite a un sistema remoto, por favor contacte al administrador.");
		}
		return pucRespuesta;
	}
	
	public String allianzClientToken() {
		return _configuration.allianzClientToken();
	}
	
	public Boolean isClientEnabled() {
		return _configuration.enableClientConfiguration();
	}
	
	private boolean invalidRequest(PortletSession session){
		return Optional.ofNullable(session.getAttribute("invalidRequest", PortletSession.APPLICATION_SCOPE))
					   .map(param -> (Long) param)
					   .filter(lastTime -> 
					   		(lastTime + _configuration.waitRequest()) > new Date().getTime())
					   .isPresent();
	}
	
	private static Optional<JSONObject> parse(String gsonString) {
		JSONParser parser = new JSONParser();
		Optional<JSONObject> objectJson = Optional.empty();
		try {
			objectJson = Optional.ofNullable((JSONObject)parser.parse(gsonString));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return objectJson;
	}
	
	@Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL)
	protected void setPucTramitesServiceClient(PucTramitesServiceClient pucTramitesServiceClient) {
		System.out.println("pucTramitesServiceClient = " + pucTramitesServiceClient);
		this.pucTramitesServiceClient = pucTramitesServiceClient;
	}

	protected void unsetPucTramitesServiceClient(PucTramitesServiceClient pucTramitesServiceClient) {
		this.pucTramitesServiceClient = null;
	}
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		_configuration = Configurable.createConfigurable(
				TramiteGeneralSenderPortletConfiguration.class, properties);
		if (_log.isInfoEnabled()) {
			_log.info(new StringBuilder("Configured General Tramite Sender (datos.tramite.solicitud.sender -  v1.0.0) : \n{ ").append(
					"enable client : " + _configuration.enableClientConfiguration() ).append(
					", allianz client token : " ) .append( _configuration.allianzClientToken() ).append(
					", max num documentos: " ) .append( _configuration.maxNumDocumentos() ).append( 
					", waitRequest:" ).append(_configuration.waitRequest()).append(
					", enableCapcha:" ).append(_configuration.enableCapcha()).append( " }").toString());
		}
	}
	
	@Reference
	protected void setAllianzService(AllianzService allianzService) {
		this.allianzService = allianzService;
	}

	protected void unsetAllianzService(AllianzService allianzService) {
		this.allianzService = null;
	}

	private AllianzService allianzService;
	private PucTramitesServiceClient pucTramitesServiceClient;
	private TramiteGeneralSenderPortletConfiguration _configuration ;
	private static Log _log = LogFactoryUtil.getLog(DatosTramiteSenderPortlet.class);

}