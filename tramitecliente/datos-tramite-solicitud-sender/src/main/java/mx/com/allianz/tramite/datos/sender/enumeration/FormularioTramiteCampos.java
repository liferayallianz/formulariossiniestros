package mx.com.allianz.tramite.datos.sender.enumeration;

import static javax.portlet.PortletSession.APPLICATION_SCOPE;
import static mx.com.allianz.commons.constants.FormularioConstants.EMPTY;

import java.lang.reflect.Field;
import java.util.Optional;
import java.util.function.Predicate;

import javax.portlet.PortletSession;
import javax.portlet.ResourceRequest;

import com.liferay.portal.kernel.util.ParamUtil;

import mx.com.allianz.commons.dto.tramite.TramiteDto;

public enum FormularioTramiteCampos {
	TIPO_TRAMITE("tipoTramite"), TIPO_TRAMITE_GMM("tipoTramiteGMM"), ESTADO ("estado"),
	MUNICIPIO("municipio"), SUCURSAL("sucursal");
	
	FormularioTramiteCampos(String valor){
		 try {
	            Field fieldName = getClass().getSuperclass().getDeclaredField("name");
	            fieldName.setAccessible(true);
	            fieldName.set(this, valor);
	            fieldName.setAccessible(false);
	        } catch (Exception e) {}
	}
	
	public void buildTramite(TramiteDto tramite, String parametro){
		switch (this) {
		case TIPO_TRAMITE:
			tramite.setTipoTramite(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case TIPO_TRAMITE_GMM:
			tramite.setTipoTramiteGMM(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case ESTADO:
			tramite.setEstado(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case MUNICIPIO:
			tramite.setMunicipio(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		case SUCURSAL:
			tramite.setSucursal(Optional.ofNullable(parametro).orElse(EMPTY));
			break;
		default:
			break;
		}
	}

	public void uploadToSession(PortletSession session, String parametro){
		Optional.ofNullable(parametro).filter(NOT_EMPTY).ifPresent(value -> session.setAttribute(this.name(), value, APPLICATION_SCOPE));
	}
	
	public String getParamFromRequest(ResourceRequest resourceRequest){
		return Optional.ofNullable(ParamUtil.getString(resourceRequest, this.name())).map(this::tryString).orElse(EMPTY);
	}
	
	public String getParamFromSession(PortletSession session){
		return Optional.ofNullable(session.getAttribute(this.name(), APPLICATION_SCOPE)).map(this::tryString).orElse(EMPTY);
	}
	
	public Integer tryParseInt(String text) {
			return (text.matches("\\d+")) ? Integer.parseInt(text) : null;
	}
	
	public String tryString(Object param) {
		 return (param instanceof String) ? (String) param : null;
	}
	
	public static final Predicate<String> NOT_EMPTY = (String it) -> !it.isEmpty();

	
}
