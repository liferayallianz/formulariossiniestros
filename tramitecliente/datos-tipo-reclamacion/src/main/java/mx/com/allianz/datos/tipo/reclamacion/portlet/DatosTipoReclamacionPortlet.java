package mx.com.allianz.datos.tipo.reclamacion.portlet;

import java.util.Map;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import mx.com.allianz.datos.tipo.reclamacion.constants.DatosTipoReclamacionPortletKeys;


/**
 * @author sfrancof
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=Tramite",
		"com.liferay.portlet.instanceable=true",
		"com.liferay.portlet.requires-namespaced-parameters=false",
		"javax.portlet.display-name=datos-tipo-reclamacion Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + DatosTipoReclamacionPortletKeys.DatosTipoReclamacion,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,guest"
	},
	service = Portlet.class
)
public class DatosTipoReclamacionPortlet extends MVCPortlet {
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		if (_log.isInfoEnabled()) {
			_log.info("Portlet datos.tipo.reclamacion - 1.0.0, Cargado");
		}
	}
	
	private static Log _log = LogFactoryUtil.getLog(DatosTipoReclamacionPortlet.class);

}