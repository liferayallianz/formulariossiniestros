package mx.com.allianz.datos.tipo.reclamacion.constants;

/**
 * @author sfrancof
 */
public class DatosTipoReclamacionPortletKeys {

	public static final String DatosTipoReclamacion = "DatosTipoReclamacion";

}