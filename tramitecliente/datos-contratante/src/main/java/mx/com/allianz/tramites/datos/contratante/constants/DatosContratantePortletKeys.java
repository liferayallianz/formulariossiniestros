package mx.com.allianz.tramites.datos.contratante.constants;

/**
 * @author sfrancof
 */
public class DatosContratantePortletKeys {

	public static final String DatosContratante = "DatosContratante";

}