package mx.com.allianz.tramites.datos.contratante.portlet;

import java.util.Map;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import mx.com.allianz.tramites.datos.contratante.constants.DatosContratantePortletKeys;

/**
 * @author sfrancof
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=Tramite",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=datos-contratante Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + DatosContratantePortletKeys.DatosContratante,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user,guest"
	},
	service = Portlet.class
)
public class DatosContratantePortlet extends MVCPortlet {
	
	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) {
		if (_log.isInfoEnabled()) {
			_log.info("Portlet datos.contratante - 1.0.0, Cargado");
		}
	}
	
	private static Log _log = LogFactoryUtil.getLog(DatosContratantePortlet.class);
	
}